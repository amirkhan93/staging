<?php

$additional_charges = (array) $price_list->additional_charges;
$taxes = (array) $price_list->taxes;

// echo "<pre>";
// print_r($price_list);
// die();
?>

@extends('template_two')
<link
  rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.2/css/intlTelInput.css"
/>
<link
  rel="stylesheet"
  href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
/>
@section('main')

<main id="site-content" role="main" ng-controller="payment">
<div id="main-view" class="main-view page-container-responsive row-space-top-6 row-space-6">
@if($reservation_id!='' || $booking_type == 'instant_book')
  @if(Session::get('get_token')=='')
  <form action="{{ url('payments/stripe_booking') }}" method="post" id="checkout-form" data-stripe-publishable-key="{{ config('constants.TEST_STRIPE_KEY') }}">
  @else
  <form action="{{ url('api_payments/create_booking') }}" method="post" id="checkout-form">
  @endif
@else
  @if(Session::get('get_token')=='')
  <form action="{{ url('payments/pre_accept') }}" method="post" id="checkout-form">
  @else
  <form action="{{ url('api_payments/pre_accept') }}" method="post" id="checkout-form">
  @endif
@endif

    <input name="room_id" type="hidden" value="{{ $room_id }}">
    <input name="checkin" type="hidden" value="{{ $checkin }}">
    <input name="special_offer_id" type="hidden" value="{{ $special_offer_id }}">
    <input name="checkout" type="hidden" value="{{ $checkout }}">
    <input name="number_of_guests" type="hidden" value="{{ $number_of_guests }}">
    <input name="nights" type="hidden" value="{{ $nights }}">
    <input name="cancellation" type="hidden" value="{{ $cancellation }}">
    <input name="currency" type="hidden" value="{{ $result->rooms_price->code }}">
    <input name="session_key" type="hidden" value="{{ $s_key }}">
    {!! Form::token() !!}

    <div class="row">
      <div class="col-md-5 col-md-push-7 col-lg-4 col-lg-push-8 row-space-2 lang-ar-left" >
        <div id="payment-right">
        <div class="panel payments-listing" >
          <div class="media-photo media-photo-block text-center payments-listing-image">
            @for($i = 0; $i < 3; $i++)
              @if($result->photos[0]->id != 0)
                {!! Html::image('images/'.$result->photos[$i]->name, $result->name, ['class' => 'img-responsive-height']) !!}
              @else
                {!! Html::image('images/'.$result->photos[0]->name, $result->name, ['class' => 'img-responsive-height']) !!}
              @endif
            @endfor
          </div>
          <div class="panel-body">
            <section id="your-trip" class="your-trip">
              <div class="hosting-info">
                <div class="payments-listing-name h4 row-space-1" style="word-wrap: break-word;">
                  {{ $result->name }}
                  <p style="font-weight: normal; font-size: 14px; margin: 10px 0px !important;">
                    @if($result->rooms_address->city !=''){{ $result->rooms_address->city }} , @endif
                    @if($result->rooms_address->state !=''){{ $result->rooms_address->state }} @endif
                    @if($result->rooms_address->country_name !='') , {{  $result->rooms_address->country_name }} @endif
                  </p>
                </div>
                <div class="">
                  <hr>
                  <!-- <div class="row-space-1">
                    <strong>
                    {{ $result->room_type_name }}
                    </strong> {{ trans('messages.payments.for') }} <strong>{{ $number_of_guests }} {{ trans_choice('messages.home.guest',$number_of_guests) }}</strong>
                  </div> -->
                  <div>
                    <strong>{{ date('D, M d, Y', strtotime($checkin)) }}</strong> {{ trans('messages.payments.to') }} <strong>{{ date('D, M d, Y', strtotime($checkout)) }}</strong>
                  </div>
                </div>



                <section id="billing-summary" class="billing-summary">
                    <div class="tooltip tooltip-top-middle taxes-breakdown" role="tooltip" data-sticky="true" data-trigger="#tax-tooltip" aria-hidden="true">
    <div class="panel-body">
      <table>
        <tbody><tr>
          <td colspan="2"></td>
        </tr>
      </tbody></table>
    </div>
  </div>
  <div class="tooltip tooltip-top-middle makent-credit-breakdown" role="tooltip" data-sticky="true" data-trigger="#makent-credit-tooltip" aria-hidden="true">
    <div class="panel-body">
      <table class="table makent-credit-breakdown">
      </table>
    </div>
  </div>
  <table id="billing-table" class="reso-info-table billing-table" style="display:none">
    <tbody>
    <tr class="base-price">
      <td class="name pos-rel">
       <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif
        </span>
        {{ $price_list->rooms_price }}  x {{ $nights }} {{ trans_choice('messages.rooms.night',$nights) }}
         <i id="service-fee-tooltip" rel="tooltip" title="{{ trans('messages.rooms.avg_night_rate') }}" style="position:relative;"  >

        </i>
      </td>
      <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif
        </span>
        <span >{{ $price_list->total_night_price }}</span>
      </td>
    </tr>
    @if($price_list->additional_guest)
      @if(@$special_offer_id == '' || @$special_offer_type == 'pre-approval' )
        <tr class="additional_price">
          <td class="name">
            {{ trans('messages.rooms.addtional_guest_fee') }}
          </td>
        <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
              @endif</span><span>{{ $price_list->additional_guest }}</span></td>
        </tr>
        @endif
    @endif

    @if(count($additional_charges) > 0)
      @foreach($additional_charges as $charges)
      <tr>
        <td class="name">
          {{ $charges->name }}
        </td>
        <td class="val text-right">
          <span class="lang-chang-label">
            @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
            @endif
          </span>
          <span>{{ $charges->amount }}</span>
        </td>
      </tr>
      @endforeach
    @endif

    <tr class="base-price">
      <td class="name pos-rel">
        <span class="lang-chang-label">
          Subtotal
        </span>
      </td>
      <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif
        </span>
        <span >{{ $price_list->subtotal }}</span>
      </td>
    </tr>

    @if(count($taxes) > 0)
      @foreach($taxes as $tax)
      <tr>
        <td class="name">
          {{ $tax->name }}
        </td>
        <td class="val text-right">
          <span class="lang-chang-label">
            @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
            @endif
          </span>
          <span>{{ $tax->amount }}</span>
        </td>
      </tr>
      @endforeach
    @endif

    <tr class="editable-fields" id="after_apply">
      <td colspan="2">
        <div class="row-condensed clearfix row-space-1">
          <div class="col-sm-7">
            <input autocomplete="off" class="coupon-code-field" name="coupon_code" type="text" value="" />
          </div>
          <div class="col-sm-5">
            <a href="javascript:void(0);" id="apply-coupon" class="btn btn-block apply-coupon">{{ trans('messages.payments.apply') }}</a>
          </div>
        </div>

        <p id="coupon_disabled_message" class="icon-rausch" style="display:none"></p>
        <a href="javascript:;" class="cancel-coupon">{{ trans('messages.your_reservations.cancel') }}</a>
      </td>
    </tr>
    {{--
    @if($reservation_id!='' || $booking_type == 'instant_book')
      <tr class="coupon">
        <td class="name">
          <span class="without-applied-coupon">
          <span class="coupon-section-link" id="after_apply_coupon" style="{{ (Session::has('coupon_amount')) ? 'display:Block;' : 'display:none;' }}">
          @if($travel_credit !=0 )
            {{ trans('messages.referrals.travel_credit') }}
          @else
            {{ trans('messages.payments.coupon') }}
          @endif
          </span>
          </span>
          <span class="without-applied-coupon" id="restric_apply">
            @if(Session::get('get_token')=='')
            <a href="javascript:;" class="open-coupon-section-link" style="{{ (Session::has('coupon_amount')) ? 'display:none;' : 'display:Block;' }}">{{ trans('messages.payments.coupon_code') }}</a>
            @endif
          </span>
        </td>
        <td class="val text-right">
          <div class="without-applied-coupon label label-success" id="after_apply_amount" style="{{ (Session::has('coupon_amount')) ? 'display:Block;' : 'display:none;' }}">
           -{{ $result->rooms_price->currency->symbol }}<span id="applied_coupen_amount">{{ $price_list->coupon_amount }}</span>
          </div>
        </td>
      </tr>

      <tr id="after_apply_remove" style="{{ (Session::has('coupon_amount')) ? '' : 'display:none;' }}">
        <td>
          <a data-prevent-default="true" href="javascript:void(0);" id="remove_coupon">
            <span>
              @if($travel_credit !=0 )
                {{ trans('messages.referrals.remove_travel_credit') }}
              @else
              {{ trans('messages.payments.remove_coupon') }}
              @endif
            </span>
          </a>
        </td>
        <td>
        </td>
      </tr>
    @endif
    --}}
  </tbody>
</table>

<hr>

  <table id="payment-total-table" class="reso-info-table billing-table">
    <tbody>
      <tr class="total">
        <td class="name"><span class="h3">{{ trans('messages.rooms.total') }}</span></td>
        <td class="text-special icon-dark-gray text-right"><span class="h3">
          @if(Session::get('get_token')!='')
             {{ Session::get('currency_symbol') }}
          @else
           {{ $result->rooms_price->currency->symbol }}
          @endif
        </span> <span class="h3" id="payment_total">{{ $price_list->total }}</span></td>
      </tr>

    </tbody>
  </table>


</section>
</div>
</section>
</div>
</div>
  <div class="panel call_panel">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-2">
          <i class="mdi mdi-phone"></i>
        </div>
        <div class="col-md-10">
          <span>
          For booking assistance, call (888) 640-6970
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div id="content-container" class="col-md-8 col-md-pull-5 col-lg-pull-4 lang-ar-right">
 <!-- Multi step form -->
 <section class="multi_step_form">
      <div id="msform">
        <!-- fieldsets -->
        <fieldset id="f1">
          <h2>Begin your booking</h2>
          <hr />
          <h3>Enter contact information</h3>

          <div class="form-row row">
            <div class="form-group  col-md-6">
              <input
                id="firstName"
                name="first_name"
                type="text"
                placeholder="Enter First Name"
                class="form-control"
                value="{{ old('first_name', $contact_details->firstname) }}"
              />
            </div>
            <div class="form-group  col-md-6">
              <input
                id="lastName"
                name="last_name"
                type="text"
                placeholder="Enter Last Name"
                class="form-control"
                value="{{ old('last_name', $contact_details->lastname) }}"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                id="email"
                name="email"
                type="email"
                placeholder="Enter Email"
                class="form-control"
                value="{{ old('email', $contact_details->email) }}"
              />
            </div>
            <div class="form-group col-md-6 phone_number_col">
              <input
                type="tel"
                id="phone"
                name="phone_code"
                class="form-control"
                placeholder="+880"
                value="{{ old('phone_code', $contact_details->phone_code) }}"
              />
              <input
                id="phoneNumber"
                type="text"
                name="phone_number"
                class="form-control"
                placeholder="01123456789"
                value="{{ old('phone_number', $contact_details->phone_number) }}"
              />
            </div>
          </div>

          <h3>Include a message for the owner</h3>
          <div class="block mb-3 owner_block">
          <div class="media">
            <div class="media-left">
              <a href="#">
                <img class="media-object" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAe1BMVEVVYIDn7O3///9TXn9KVnnq7+/t8vJGU3dNWXtRXH1EUXbe4+ZJVXlPWnzp7u+/xc5faYdZZIP29vhsdZBye5SFjKJocY3DydG1u8aytsN+hp2an7Hi5+nl5uvz9PaTmazR09vU2t+kqbmMk6efprXO1NrZ2+Kssr/h4ujU+9XeAAAMKklEQVR4nN2d6ZKjOgxGHWywTbMkZN/3Tr//E46BLCSBAJII1Hx169bM/KA5LSPJsmwzq3FF6104PyyPo+Gk3x8wxgb9yWhxXM7D3d+m+R/Pmnz4X3gYT5iQQihfa84ZM/+x+P+ca18JISWbjA/hX5Mv0RThOlwOXSmUTpmKxbUS0u1vZ+uG3qQJwr/5gkvhl7E9cfpC8sW8CWNSE26mRy2VrgH3kFYu304j4jciJdzMRkLA6O6UwhvNSP0PIWG4cJF4N0i5COlei4rw9yg8Crw75PiX6M1oCGd9Weo0a8qXkxnJuxEQrreuIMZLxIVcBh0g/F24fgN4KaOSY3QAQRL+jly6ry9PvjtCfpAowt+RbJYvlpYjlB0RhOtFw/a7M7pjREoHJtwcv8QXy3e34FQHSnjxGvMvuVJi/lXCHWskPnyU14e5HAhhNHa/zmfE3e2XCMMvD9CHFN99gTAay5b4GMyMdQl3ui0DplKs7tdYk3DptsrHYjMeGiTcTETbgEZiVCs21iGckkxw8fL9Og6nBuGp9RF6l3tpgnDUhRF6kxyTE2767frQV6lJ1XJVRcI/wiIMjbSuON+oRrhrMcoXictqkbES4byVPLRU7pSK8NAdJ/ok7lYpq1YgXHZwiF7lVpg0lhNuPcp34ox0wFdALCUkAORcKyWEJ6Xr+dr3XCk9IZRKVhQbRywjRANq5fn98Wkenvc/PSdV72d/Duen8cQXwGWqDGJZZbyEEOdkuJJ6fDmvDJRtB3bQu8n82bbNv67Ol7GWCmXKMnfzmfCCAOTKGx72hi14kL0qCAzn/jD0MJAlQeMjYQgH5KJv8Gy7EO6hBLLvgRm5+zH0fyLcgQG5NwqN9Srg3SHDITyzFz0Y4RocB1U/dIqHZsGAdWYDBfx5fAAijAbAccO9U6XR+WbIYAkdqnoEIRwC/bia7B0AX2LGcx9oRlFcgysk3AI/C3n84DtLzdgbA+OvLAyLRYRQN+pdan+Bz2Y8ARHdoiW4AsK1hH0R7gw4Qu9ygDGYF9mq4N+BXkaiAWNEmBX9RR3CI+yDFyc8oEEEugCZn4TnEk5hA8VfUAAaRKAbz/8U8wg3wI9drzBO5qHgB7Y6yftVCRewwqFH8BGmci6wcSqW1QhDWLamR1SAJi72ga48Jwd/J4yAgUKeacZorCCEGTEvQX0nXACbQ4ncTCpnAvs1i1M5IbT4600h2XaR7Bk0aXyrhL8RAvNt3qcE7AU9slnGK+EJmNz7JMH+IecIXAmSrzWNF8IedFrvEfqZWDbQ17znpy9/B4ZCY0NaE5phCv1di8snwl9o4UKPiQlN6gYtMYjNB0LoU5m6kDqamHALrRX722JCYMZtJEhjRSx7Dq1LMTcoJATmSkbenpxwCq4u6mMRITAhZfGmHqJpRYZwD18xeQr7WUK4CRkj5ounUHBC/5hPiFmsH1CbsBesEM0f7iaXEOxIY1EDGiHeRi3zCMGxMBH5dxisMK+johxC4KwpUQOeJliBo4VRpiv8TrhBrYWqH3LCH1SXGX8nhE4qOkood2+EmMd1kFAvXgmnuI6EzhE+Urcb4QjXE9E9QnV5JsT5mS4S3stuV8ILys90kZDdWhevhMDiXZcJb9PElHCN7T7sICHTWcIDcpB2kvAaElNCzLyps4TXYZoQwjtnukzI2INwjn5YJwnl350QGe67SpgG/YQQ3+bcScJ0DSMm3OHbgDtJyNzoSoibOHWYUOyuhNiEprOE6pQSQlsvuk/IJykhxZafjhKKKCFEp2xGogFCgqEVzy8Yrsh2FVcN1NoI9juqeUKINyEXZ+qFmV7PPsOb22+Ky/vMCvD7tgRZN1RWDrQf46G4D4xZO3zuQNpKk0EcoY3oxoTYAgZtN1RWNrIAyJLkm1lHrKPhk2ZMaIyInreK0BCilpxi+cvGCMFr+TepgyFEj3U1o3ekqWz0F+SPLbZBZzQibOYzhLcoPmTyNvZHQNiYDdGEjFkMPznsNKGIGD6sdppQrhk+7+40offLlui8u9OEYsfG/znhjOFzv04TqgubYJ/RbUL/wAb/N6Feovk6Tuhv/3dCPf7fCfnivycc/e+ErP/fE1Koy/PDWPh4qObdnePHfOhiD9Pbxuo00M1PGfUJsjbeb4wQP8AMH7rURr318KEAvuXiLj4kmFuYmNNQzZvgt28iPn5+mByG0QQgcEf3k/SRbSnOtJRTekRnSnESnplbnEhO7fSoEQMnJFi4TeaHBCEnlksaFANndQRumn+RmjOixIjyvIFe8LNVRAfCiinDLx8mIt1T4mzJju0XO4Kq/vVRhEHRpnmlWHJNsDKTyqfL3RCbK98kIoLVtasGZIQU6ehdFqPo+UokqNa6gxXd5Rl8aAgpkppYZEvBNr6h9y59NIQULVGJqPaRQs81yZO6GEKySgHRFIOgA+MhsTOE+Db2q4jOVXAIutDuctdx1xfZWdYk7Xv2nvBsbe4lfW0Es7BUiuIEFwfd35NR3OltCJdk0YfjbQg9ySxfcZOwIaQrSgr8BIM02hvnlxCSuRrG+1gj2j+kJ9zLIO1kp3uiwLZHOWPSazQG1159uqfyAY4wOJOe4Z8cchIT4ltq7hK4mhRFdS37NuGVEL2/MivMRJigK/hJyX7uZN8TQWn5JoXIv4MV7VUv6VmYCSFJRfEqdw82IrGbMbOdOyHB1q674CUpG37GUb7SbbIJYUSZR4g5DBF3XEueVGYfMGU6z8UPKGRQj9HbgVgpIelqMmycOoQz+1RimiGMSL8ASFAM9jQ17ozU05kKlMMUcnpbsILeU1Co26ltV0LCCiWLA1FdEzr4zdavup3Bczu9hfb229pB0aG/1s23ngkpg775/dWtZzjk156p0wsh1fJFqg4QuusXQrLSd6L2CR/nCd8JSdP69glF+EZoUd4A2AFC651wSVXdZx0gVIccQrqCVPuE3NvkEBKWa1onzB4lnCH8oytmtE2YPUk4ewYtXebUMqHO3hacJcSdYJpVy4SF5wjTGbFdwuKzoOm+xHYJ3V4hIZk7lXWrphS9sjd9OpOdLCbWXmYjLHZz+elcfaLEpn6lhnBRLZPO5BFGJITqULeKAb515V3c+khIM8Xwate9gx/chbIPyderZd/umSGYJ/qApgyqaqkevgK9EeIjBheAixLsPY2Tq3BXEN7ZSNAaonOi+D6q3PdkWRo3Tr0jbN3CWRAc51Tpzi7ENcCxJBCwFwRDNGK1e9csawv/SVrBl7mD4IispOSM0YIbHqF32Gsx/kGs4wdO2MdUpqvff2j9gsYpl6Mz6iJZ41HtGeKC7nc/WkhoQS47FZMpki9hdMCMde4hNTPFuvFXsBkBX8JozwYQxnp3yVpRPUKlD6CruAsYIXasex9wrZChveWKdttT/D3W9Dl173S2rHnVLEpL40DJ9zrXtaP7mnCXE1a89Zh7aAdaxFjHjrm3yJYRWsPyb5HTONAPjNXsqAq8TAlhxMueLgYzp6md6nfGCnbUuaG+nNBaf4yKXPmXoFG+hDGYDcoYud58oPhEaHKb4mdrsew1tUv9mdEps2N+LlOJsPhCRO0eG3CgRYwf7cjd3UeGz4RWmIvI5WL/Nb6E8YMdSwDLCK35O2KzDrSIsciOxYGwIqE1e0UU/YYdaBGjM8+xYylgOeGLFZX+ggMtYgzexqo7K33/csKsFbV3Is5AazI+x0debsFKhMbdpA/l3hcdaBFjbMeqTqYy4TVoiElDGWhNRvui/dSj59SdgITWrxkactmKg8mRsxpLzrT+GOhrElrBQIZtfoDPCpyZpwafUrX6hFZEfqUxSs5+EZW/dC1Cy+oUYc+p/N7VCa1N21QZVRyhNQmtqG2uu6qO0LqExuG0jZYoKH9RMGEnRmqNEQog7MBIrTNCIYSW1W5YrO5D4YStmrGuAWGE7ZmxvgGhhFbUhlOt50KRhG041ZouFE347aEKGqBIwm9mqjbiLTGE32IEfoAkhN/I4zD2oyBs+nuEf390hE0yQv1nVhSEJnY0MVgDQAKTIxpCi96Q+OF5FRmhSXToPKtDY75EhIQWUTZnE+JZ1IRGEW64UlovFTlhrA2ww5TCdb6pEcJYUS3/GtDb7qbGCBNFm9KVjsDZNAaXqFnCq6LNxnFsOwgS3MD8wXaczSZqFu2qf0bEBYdvuOiJAAAAAElFTkSuQmCC" alt="...">
              </a>
            </div>
            <div class="media-body">
              <h4 class="media-heading">Name Of Owner</h4>
              <span>Speaks: English</span>
            </div>
          </div>
</div>
          <textarea placeholder="Special Request" class="form-control" rows="2"></textarea>
          <br />
          <p>
            By clicking 'Agree & continue' you are agreeing to our <a data-toggle="modal" data-target="#termsModal" href="">Terms and
            Conditions</a>, <a data-toggle="modal" data-target="#pirvacyModal" href="#">Privacy Policy</a>, and to receive booking-related texts.
            Standard messaging rates may apply.
          </p>
          <br />

          <button type="button" class="btn btn-primary previous_button">
            Back
          </button>
          <button type="button" class="next btn btn-primary">Continue</button>




        </fieldset>
        <fieldset id="f2">
          <h2>Review rules & policies</h2>
          <hr>
          <div class="block mb-3">
            <p>
              <strong> Check-in: <small>Flexible</small> </strong>
              <strong> Check-out: <small>10:30AM</small> </strong>
            </p>
          </div>
          <div class="block mb-3">
            <b>Owner's Cancellation Policy</b>
            <p>
              Cancel for free up to 2 weeks before your trip. Cancel up to 1
              week before your trip for a 50% refund.
            </p>
          </div>
          <div class="block">
            <b>Damage Policy</b>
            <p>
              You will be responsible for any damage to the rental property
              caused by you or your party during your stay.
            </p>
          </div>
          <hr />
          <div class="block">
            <b>House Rules</b>
            <p>
              By booking this space you’re agreeing to follow Sumit's House
              Rules..
            </p>
          </div>
          <div class="card mb-3">
            <div class="card-body">
              <div class="checkbox">
              <label>
                <input type="checkbox" name="rental__checkbox" id="rental__checkbox"> I have read and agree to comply with all rental policies and
                  terms.
              </label>
              <p id="checkbox__err" style="display: none; color: red;">Please accept first</p>
            </div>
            </div>
          </div>
          <button type="button" class="btn btn-primary previous previous_button">
            Back
          </button>
          <button type="button" class="next btn btn-primary">Continue</button>
        </fieldset>
        <fieldset id="f3">
          <h2>Enter payment information</h2>
        <div class="card_payment_main" id="card_payment_main">

<div class="card_container preload">
    <div class="creditcard">
        <div class="front">
            <div id="ccsingle"></div>
            <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                <g id="Front">
                    <g id="CardBackground">
                        <g id="Page-1_1_">
                            <g id="amex_1_">
                                <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                        C0,17.9,17.9,0,40,0z" />
                            </g>
                        </g>
                        <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                    </g>
                    <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">0123 4567 8910 1112</text>
                    <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6">Name on card</text>
                    <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                    <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                    <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">card number</text>
                    <g>
                        <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9">01/23</text>
                        <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                        <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                        <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                    </g>
                    <g id="cchip">
                        <g>
                            <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                    c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                        </g>
                        <g>
                            <g>
                                <rect x="82" y="70" class="st12" width="1.5" height="60" />
                            </g>
                            <g>
                                <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                            </g>
                            <g>
                                <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                        c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                        C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                        c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                        c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                            </g>
                            <g>
                                <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                            </g>
                            <g>
                                <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                            </g>
                            <g>
                                <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                            </g>
                            <g>
                                <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                            </g>
                        </g>
                    </g>
                </g>
                <g id="Back">
                </g>
            </svg>
        </div>
        <div class="back">
            <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                <g id="Front">
                    <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                </g>
                <g id="Back">
                    <g id="Page-1_2_">
                        <g id="amex_2_">
                            <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                    C0,17.9,17.9,0,40,0z" />
                        </g>
                    </g>
                    <rect y="61.6" class="st2" width="750" height="78" />
                    <g>
                        <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                C707.1,246.4,704.4,249.1,701.1,249.1z" />
                        <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                        <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                        <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                    </g>
                    <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7">985</text>
                    <g class="st8">
                        <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                    </g>
                    <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                    <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                    <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13"></text>
                </g>
            </svg>
        </div>
    </div>
</div>
<div class="form-container">
    <div class="field-container">
        <label for="name">Name</label>
        <input id="name" name="cc_name" maxlength="20" type="text">
    </div>
    <div class="field-container">
        <label for="cardnumber">Card Number</label>
        <input id="cardnumber" type="text" name="cardnumber" />
        <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink">
        </svg>
    </div>
    <div class="field-container">
        <label for="expirationdate">Expiration (mm/yy)</label>
        <input id="expirationdate" name="expirationdate" type="text">
    </div>
    <div class="field-container">
        <label for="securitycode">Security Code</label>
        <input id="securitycode" name="securitycode" type="text">
    </div>
    
</div>
</div>
<p id="stripe__err" style="display: none; color: red;"></p>
<!-- start billing form -->
    <div class="billing_form_sec">
    <h2>Billing Information</h2>
    <div class="form-row row">
            <div class="form-group  col-md-6">
              <input
                type="text"
                placeholder="First Name"
                name="b_first_name"
                class="form-control"
              />
            </div>
            <div class="form-group  col-md-6">
              <input
                type="text"
                name="b_last_name"
                placeholder="Last Name"
                class="form-control"
              />
            </div>
            <div class="form-group col-md-6">
              <input
                type="text"
                placeholder="Street"
                class="form-control"
              />
            </div>
            <div class="form-group col-md-6">
            <select id="country" class="form-control" name="country">
              <option disabled selected value="">Select your country</option>
              @foreach($country as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
              @endforeach
            </select>
            </div>

            <div class="form-group col-md-3">
              <input
                type="text"
                placeholder="City"
                class="form-control"
              />
            </div>
            <div class="form-group col-md-3">
              <input
                type="text"
                placeholder="Zip Code"
                name="zipcode"
                class="form-control"
              />
            </div>
            <div class="form-group col-md-3">
            <select name="state" class="form-control">
            <option disabled selected value="">Select your State</option>
  <option value="AL">Alabama</option>
  <option value="AK">Alaska</option>
  <option value="AZ">Arizona</option>
  <option value="AR">Arkansas</option>
  <option value="CA">California</option>
  <option value="CO">Colorado</option>
  <option value="CT">Connecticut</option>
  <option value="DE">Delaware</option>
  <option value="FL">Florida</option>
  <option value="GA">Georgia</option>
  <option value="HI">Hawaii</option>
  <option value="ID">Idaho</option>
  <option value="IL">Illinois</option>
  <option value="IN">Indiana</option>
  <option value="IA">Iowa</option>
  <option value="KS">Kansas</option>
  <option value="KY">Kentucky</option>
  <option value="LA">Louisiana</option>
  <option value="ME">Maine</option>
  <option value="MD">Maryland</option>
  <option value="MA">Massachusetts</option>
  <option value="MI">Michigan</option>
  <option value="MN">Minnesota</option>
  <option value="MS">Mississippi</option>
  <option value="MO">Missouri</option>
  <option value="MT">Montana</option>
  <option value="NE">Nebraska</option>
  <option value="NV">Nevada</option>
  <option value="NH">New Hampshire</option>
  <option value="NJ">New Jersey</option>
  <option value="NM">New Mexico</option>
  <option value="NY">New York</option>
  <option value="NC">North Carolina</option>
  <option value="ND">North Dakota</option>
  <option value="OH">Ohio</option>
  <option value="OK">Oklahoma</option>
  <option value="OR">Oregon</option>
  <option value="PA">Pennsylvania</option>
  <option value="RI">Rhode Island</option>
  <option value="SC">South Carolina</option>
  <option value="SD">South Dakota</option>
  <option value="TN">Tennessee</option>
  <option value="TX">Texas</option>
  <option value="UT">Utah</option>
  <option value="VT">Vermont</option>
  <option value="VA">Virginia</option>
  <option value="WA">Washington</option>
  <option value="WV">West Virginia</option>
  <option value="WI">Wisconsin</option>
  <option value="WY">Wyoming</option>
</select>
            </div>
          </div>
    </div>
    <!-- end billing form -->

          <button type="button" class="btn btn-primary previous previous_button">
            Back
          </button>
          <button type="submit" class="btn btn-primary">Book Now</button>
        </fieldset>
      </div>
    </section>
    <!-- End Multi step form -->
      </div>

    </main>


    <div id="gmap-preload" class="hide"></div>

<div class="ipad-interstitial-wrapper"><span data-reactid=".1"></span></div>


    <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div></div>

<div class="tooltip tooltip-top-middle" role="tooltip" data-trigger="#tooltip-cvv" aria-hidden="true">
      <div class="tooltip-cvv"></div>
    </div><div class="tooltip tooltip-bottom-middle" role="tooltip" aria-hidden="true">  <p class="panel-body">{{ trans('messages.payments.fee_charged_by',['site_name'=>$site_name]) }}</p></div></body></html>
<!-- Modal -->
<div class="modal fade t_p_modal" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
                </div>
                <div class="modal-body">
                  <p>
                  PLEASE READ THESE TERMS OF SERVICE CAREFULLY AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES AND OBLIGATIONS. THESE INCLUDE VARIOUS LIMITATIONS AND EXCLUSIONS, A CLAUSE THAT GOVERNS THE JURISDICTION AND VENUE OF DISPUTES, AND OBLIGATIONS TO COMPLY WITH APPLICABLE LAWS AND REGULATIONS.

IN PARTICULAR, HOSTS SHOULD UNDERSTAND HOW THE LAWS WORK IN THEIR RESPECTIVE CITIES. SOME CITIES HAVE LAWS THAT RESTRICT THEIR ABILITY TO HOST PAYING GUESTS FOR SHORT PERIODS. THESE LAWS ARE OFTEN PART OF A CITY'S ZONING OR ADMINISTRATIVE CODES. IN MANY CITIES, HOSTS MUST REGISTER, GET A PERMIT, OR OBTAIN A LICENSE BEFORE LISTING A PROPERTY OR ACCEPTING GUESTS. CERTAIN TYPES OF SHORT-TERM BOOKINGS MAY BE PROHIBITED ALTOGETHER. LOCAL GOVERNMENTS VARY GREATLY IN HOW THEY ENFORCE THESE LAWS. PENALTIES MAY INCLUDE FINES OR OTHER ENFORCEMENT. HOSTS SHOULD REVIEW LOCAL LAWS BEFORE LISTING A SPACE ON Searchstays.
                  </p>
                  <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim massa ac lectus eleifend tristique. Nullam ultrices ultrices lacus eget euismod. Aliquam lobortis dapibus pellentesque. Suspendisse sagittis dolor ut aliquam convallis. Mauris erat magna, volutpat vel varius at, elementum nec mauris. Praesent non nisi eu orci maximus tristique. Sed mattis nibh nec nisl bibendum scelerisque. Mauris at viverra metus, eu egestas ligula. Fusce nec risus viverra, sodales massa vitae, ornare eros.
                  </p>
                </div>

              </div>
            </div>
          </div>
          <!-- Modal -->
<div class="modal fade t_p_modal" id="pirvacyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Privacy Policy</h4>
                </div>
                <div class="modal-body">
                  <p>
                  PLEASE READ THESE TERMS OF SERVICE CAREFULLY AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES AND OBLIGATIONS. THESE INCLUDE VARIOUS LIMITATIONS AND EXCLUSIONS, A CLAUSE THAT GOVERNS THE JURISDICTION AND VENUE OF DISPUTES, AND OBLIGATIONS TO COMPLY WITH APPLICABLE LAWS AND REGULATIONS.

IN PARTICULAR, HOSTS SHOULD UNDERSTAND HOW THE LAWS WORK IN THEIR RESPECTIVE CITIES. SOME CITIES HAVE LAWS THAT RESTRICT THEIR ABILITY TO HOST PAYING GUESTS FOR SHORT PERIODS. THESE LAWS ARE OFTEN PART OF A CITY'S ZONING OR ADMINISTRATIVE CODES. IN MANY CITIES, HOSTS MUST REGISTER, GET A PERMIT, OR OBTAIN A LICENSE BEFORE LISTING A PROPERTY OR ACCEPTING GUESTS. CERTAIN TYPES OF SHORT-TERM BOOKINGS MAY BE PROHIBITED ALTOGETHER. LOCAL GOVERNMENTS VARY GREATLY IN HOW THEY ENFORCE THESE LAWS. PENALTIES MAY INCLUDE FINES OR OTHER ENFORCEMENT. HOSTS SHOULD REVIEW LOCAL LAWS BEFORE LISTING A SPACE ON Searchstays.
                  </p>
                  <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dignissim massa ac lectus eleifend tristique. Nullam ultrices ultrices lacus eget euismod. Aliquam lobortis dapibus pellentesque. Suspendisse sagittis dolor ut aliquam convallis. Mauris erat magna, volutpat vel varius at, elementum nec mauris. Praesent non nisi eu orci maximus tristique. Sed mattis nibh nec nisl bibendum scelerisque. Mauris at viverra metus, eu egestas ligula. Fusce nec risus viverra, sodales massa vitae, ornare eros.
                  </p>
                </div>

              </div>
            </div>
          </div>
@stop
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.1.2/js/intlTelInput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="{{url()}}/js/card_payment.js"></script>
<script type="text/javascript">
(function($) {
  "use strict";
  var $form = $("#checkout-form");
  //* Form js
  function verificationForm() {
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches
    var firstName=$("#firstName");
    var lastName=$("#lastName");
    var email =$("#email");
    var phoneNumber=$("#phoneNumber");
    $(firstName).keyup(function() {
        $(this).removeClass('err');
    })
    $(lastName).keyup(function(){
      $(this).removeClass('err');
    })
    $(email).keyup(function(){
      $(this).removeClass('err');
    })
    $(phoneNumber).keyup(function(){
      $(this).removeClass('err');
    })

    $('#rental__checkbox').change(function () {
      if($(this)[0].checked) {
        $('#checkbox__err').css({
          display: 'none'
        });
      } else {
        $('#checkbox__err').css({
          display: 'block'
        });
      }
    });

    // $('#msform > fieldset').each((index, ele) => {
      
    // });
   var fieldsetHeight= $('.multi_step_form #msform fieldset#f1');
   console.log(fieldsetHeight.height())
   var main_container=$(".multi_step_form #msform");
   main_container.css({
     height:fieldsetHeight.height()
   })
    var index = 0;
    $('.next').click(function(){
     
      if(index == 0) {
        console.log("f_height",$('.multi_step_form #msform fieldset#f2').height())
        main_container.css({
        height:$('.multi_step_form #msform fieldset#f2').height()
      })
        if(firstName.val() == ""){
          $(firstName).addClass("err");
          $(firstName).focus();
          return false
        }
        if(lastName.val() == ""){
          $(lastName).addClass("err");
          $(lastName).focus();
          return false
        }
        if(email.val() == ""){
          $(email).addClass("err");
          $(email).focus();
          return false
        }
        if(phoneNumber.val() == ""){
          $(phoneNumber).addClass("err");
          $(phoneNumber).focus();
          return false
        }
      }
      console.log(index)
      if(index == 1) {
      if($("#rental__checkbox").prop("checked") == false){
          $('#rental__checkbox').focus();
          $('#checkbox__err').css({
            display: 'block'
          });
          return false;
        }
      }
      // $('input[type="checkbox"]').click(function(){
      //       if($(this).prop("checked") == true){
      //           return true;
      //       }
      //       else if($(this).prop("checked") == false){
      //           return false
      //       }
      //   });
      // if($("#rental__checkbox-1")[0].checked){
      //   alert("hello");
      //     //do something
      // }
      // if(index == 1) {
      //   if(!$('#rental__checkbox')[0].checked) {
      //     $('#rental__checkbox').focus();
      //     $('#checkbox__err').css({
      //       display: 'block'
      //     });
      //     return false;
      //   }
      // }

      if (animating) return false;
      animating = true;

      current_fs = $(this).parent();
      next_fs = $(this)
        .parent()
        .next();

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate(
        {
          opacity: 0
        },
        {
          step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = now * 50 + "%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
              transform: "scale(" + scale + ")",
              position: "absolute"
            });
            next_fs.css({
              left: left,
              opacity: opacity
            });
          },
          duration: 800,
          complete: function() {
            current_fs.hide();
            animating = false;
          },
          //this comes from the custom easing plugin
          easing: "easeInOutBack"
        }
      );
      index++;
    });

    // $(".next").click(function() {
      
    // });

    $(".previous").click(function() {
      main_container.css({
     height:fieldsetHeight.height()
   })
      index--;
      if (animating) return false;
      animating = true;

      current_fs = $(this).parent();
      previous_fs = $(this)
        .parent()
        .prev();

      //show the previous fieldset
      previous_fs.show();
      //hide the current fieldset with style
      current_fs.animate(
        {
          opacity: 0
        },
        {
          step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = (1 - now) * 50 + "%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
              left: left
            });
            previous_fs.css({
              transform: "scale(" + scale + ")",
              opacity: opacity
            });
          },
          duration: 800,
          complete: function() {
            current_fs.hide();
            animating = false;
          },
          //this comes from the custom easing plugin
          easing: "easeInOutBack"
        }
      );
    });

    // $(".submit").click(function(e) {
    //   e.preventDefault()
    // });
    $form.bind('submit', function(e) {
      e.preventDefault();
      var number = $('#cardnumber').val().replace(/\s/g, "");
      var cvc = $('#securitycode').val();
      var expiry = $('#expirationdate').val().split("/");
      var exp_month = expiry[0];
      var exp_year = expiry[1];
      Stripe.setPublishableKey($(this).data('stripe-publishable-key'));
      Stripe.createToken({
        number,
        cvc,
        exp_month,
        exp_year
      }, stripeResponseHandler);
    });
  }

  function stripeResponseHandler(status, response) {
    if (response.error) {
      $('#stripe__err').css({
        display: 'block'
      }).html(response.error.message);
    } else {
      // token contains id, last4, and card type
      var token = response['id'];
      // insert the token into the form so it gets submitted to the server
      $form.find('input[type=text]').empty();
      $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
      // return false;
      $form.get(0).submit();
    }
  }

  //* Add Phone no select
  function phoneNoselect() {
    if ($("#msform").length) {
      $("#phone").intlTelInput();
      $("#phone").intlTelInput("setNumber", "{{$contact_details->phone_code}}");
    }
  }
  verificationForm();
  phoneNoselect();
})(jQuery);
if(typeof $.stickysidebarscroll !== "undefined"){
  if ($(window).width() > 760){
    $.stickysidebarscroll("#payment-right",{offset: {top: 20, bottom: 450}});
  }
}
$(window).resize(function () {
  $(window).scrollTop( 0 );
});
</script>
@endpush
