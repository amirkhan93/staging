<div id="js-manage-listing-content-container" class="manage-listing-content-container">
  <div class="manage-listing-content-wrapper">
    <div id="js-manage-listing-content" class="manage-listing-content col-lg-12 col-md-12">


      <div class="row-space-4">
        <div class="row">
          <!-- <h3 class="col-12">Price and Availibility</h3> -->
        </div>
        <!-- <p class="text-muted">{{ trans('messages.lys.pricing_desc') }}</p> -->
      </div>
      <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Availibility</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Pricing</a></li>
          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Tax & Fees</a></li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
          </div>
          <div role="tabpanel" class="tab-pane" id="profile">
            <div class="panel-body">
              <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <i class="fa fa-files-o margin-right20 " aria-hidden="true"></i> COPY PRICING FROM ANOTHER PROPERTY
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <h5>You can copy pricing from any of your other properties to this property.</h5>
                      <small class="text-warning">Careful! Importing cannot be reversed!</small>
                      <div class="row">
                        <div class="col-md-3">
                          <label>Import everything from:</label>
                        </div>
                        <div class="col-md-6">

                          <select class="js-example-basic-single" name="state">
                            <option value="AL">Main</option>
                            <option value="WY">Property plan</option>
                            <option value="WY">interior</option>
                            <option value="WY">exterior</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <i class="fa fa-cube margin-right20" aria-hidden="true"></i> SEASONAL PRICES
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-4">
                          <label>Number of Guests in your Daily price:</label>
                        </div>
                        <div class="col-md-5">

                          <select class="js-example-basic-single" name="state">
                            <option value="AL">1 people</option>
                            <option value="WY">2 people</option>
                            <option value="WY">3 people</option>
                            <option value="WY">4 people</option>
                          </select>
                        </div>
                        <div class="col-md-3">
                          <button  class="btn btn-primary">Save</button>
                        </div>
                      </div>
                      <div class="add_seasion_table_main">
                        <button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add Seasion</button>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>Season</th>
                              <th>Date From	</th>
                              <th>Date To	</th>
                              <th>Daily Price	</th>
                              <th>Extra Guest</th>
                              <th>Minimum Stay	</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>fall</td>
                              <td>01/09/2019</td>
                              <td>31/12/2019</td>
                              <td>$ 175.00</td>
                              <td>$ 10.00</td>
                              <td>7 nights</td>
                              <td>
                                <div class="seasonal-price-buttons text-right">
                                  <span class="glyphicon glyphicon-edit blue-color"></span>
                                  <span class="glyphicon glyphicon-trash warning-color"></span>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="7">
                                <div class="content gray-dark-solid-border graybg-light">
                                  <div class=" seasonal-price-editor-header">
                                    <div class="pull-left text-capitalize font-bold add-or-edit-seasonal-price-title">
                                      Add / Edit Season
                                    </div>
                                    <div class="pull-right font-bold">
                                      <span class="text-capitalize close-editor parent-close">
                                        <u>Close</u>
                                        <span class="close-editor">
                                          x
                                        </span>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-left0">
                                      <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                        <label for="Name">Season name</label>
                                        <input class="form-control margin-bottom10" data-val="true" data-val-required="The Name field is required." id="Name" name="Name" remove-illegal-characters="" type="text" value="">
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 season-text">
                                        Channels need&nbsp;to display your taxes separately to the guest, we advise you to exclude them from your rental rates
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="display-none periods-to-remove">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 padding-left0 period-list">
                                      <span class="advance-season-spliting-message orange-color font-size-14 font-bold col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group period">
                                          <input value="0" data-val="true" data-val-number="The field Id must be a number." data-val-required="The Id field is required." id="Periods_0__Id" name="Periods[0].Id" type="hidden">
                                          <input value="2534791" data-val="true" data-val-number="The field ApartmentId must be a number." data-val-required="The ApartmentId field is required." id="Periods_0__ApartmentId" name="Periods[0].ApartmentId" type="hidden">
                                          <div class="pull-left inline-block col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                            <label class="font-size-14 text-capitalize" for="Periods_0__From">Date from</label>
                                            <div class="text-box-with-icon calendar margin-bottom10">
                                              <input class="form-control period-from datepicker" id="dateFrom"  type="text" ><span class="icon"></span>
                                            </div>
                                          </div>
                                          <div class="pull-left inline-block col-xs-5 col-sm-5 col-md-5 col-lg-5">
                                            <label class="font-size-14 text-capitalize" for="Periods_0__To">Date to</label>
                                            <div class="text-box-with-icon calendar margin-bottom10">
                                              <input class="form-control period-to datepicker" id="dateTo" type="text"><span class="icon"></span>
                                            </div>
                                          </div>
                                          <div class="btn-plus add-new-period col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                            <div class="glyphicon glyphicon-plus"></div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 ">
                                        <div class="row">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                              <label for="DailyPrice" class="font-size-14">Daily price $</label>
                                              <input class="form-control font-size-14" data-val="true" data-val-number="The field DailyPrice must be a number." data-val-range="Daily price must be greater than 0" data-val-range-max="1.79769313486232E+308" data-val-range-min="0.01" data-val-required="The DailyPrice field is required." id="DailyPrice" name="DailyPrice" type="text" value="0.00">
                                            </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                              <label class=" font-size-14 " for="NumberOfExtraGuests">For N# of Guest</label>
                                              <input class="form-control font-size-14" disabled="disabled" id="NumberOfExtraGuests" name="NumberOfExtraGuests" type="text" value="6">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                              <label for="ExtraGuestPrice" class="font-size-14">Extra Guest Price $</label>
                                              <input class="form-control font-size-14" data-val="true" data-val-number="The field ExtraGuestPrice must be a number." data-val-range="Extra guest price must be greater than or equal 0" data-val-range-max="1.79769313486232E+308" data-val-range-min="0" data-val-required="The ExtraGuestPrice field is required." id="ExtraGuestPrice" name="ExtraGuestPrice" type="text" value="0.00">
                                            </div>
                                          </div>
                                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                              <label class=" font-size-14 " for="MinimumStay">Minimum Stay</label>
                                              <input class="form-control font-size-14" data-val="true" data-val-number="The field MinimumStay must be a number." data-val-range="Minimum stay must be greater than 0" data-val-range-max="2147483647" data-val-range-min="1" data-val-required="The MinimumStay field is required." id="MinimumStay" name="MinimumStay" type="text" value="1">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding-right">
                                          <div class="save-seasonal-price-btn-container margin-bottom20 margin-top10 pull-right col-md-6 nopadding-right text-right">
                                            <div class="btn btn-primary">
                                              save

                                            </div>
                                          </div>


                                        </div>

                                      </div>

                                    </div>
                                    <a href="javascript:void(0)" class="" data-toggle="collapse" data-target="#demo">Show advanced options (changeovers, weekend, rates and fees)</a>
                                    <div id="demo" class="collapse advanace_options_main" >
                                      <div class="row">
                                        <div class="col-md-6">
                                          <h5> <strong> Weekend Pricing:  </strong></h5>
                                          <hr>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <strong>
                                                Applies on:
                                              </strong>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Thursday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Friday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Saturday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Sunday
                                                </label>
                                              </div>

                                            </div>
                                            <div class="col-md-6">
                                              <strong>
                                                Price per night:
                                              </strong>
                                              <div class="form-group" style="margin-top: 15px;">
                                                <input type="text" class="form-control">
                                              </div>
                                            </div>
                                          </div>


                                        </div>
                                        <div class="col-md-6">
                                          <h5> <strong>Changeover: </strong></h5>
                                          <hr>
                                          <div class="row">
                                            <div class="col-md-6">
                                              <strong>
                                                Check-in never on:
                                              </strong>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Monday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Tuesday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Wednesday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Thursday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Friday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Saturday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Sunday
                                                </label>
                                              </div>


                                            </div>
                                            <div class="col-md-6">
                                              <strong>
                                                Check-out never on:
                                              </strong>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Monday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Tuesday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Wednesday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Thursday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Friday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Saturday
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Sunday
                                                </label>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="extra_section">
                                            <hr>
                                            <strong> Occupancy pricing: Extra price per guest </strong>
                                            <p>
                                              If you have X extras guest the price per extra guest per night will be Y </p>

                                              <button  class="btn btn-primary" id="occupancy_btn">
                                                Add Price
                                              </button>
                                              <table class="add_p_table" id="occupancy_box">
                                                <thead>
                                                  <tr>
                                                    <th>
                                                      N# Extra Guest
                                                    </th>
                                                    <th>
                                                      Price
                                                    </th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td colspan="2">
                                                      <div class="content gray-solid-border graybg gray-dark-solid-border" >
                                                        <div class=" extra-person-price-editor-header">
                                                          <div class="pull-left text-capitalize font-bold add-or-edit-extra-person-price-title font-size-16">
                                                            Add / Edit Price
                                                          </div>
                                                          <div class="pull-right font-bold">
                                                            <span class="text-capitalize close-editor" id="close_occu">
                                                              <u> Close</u>
                                                            </span>
                                                            <span class="close-editor">
                                                              x
                                                            </span>
                                                          </div>
                                                        </div>
                                                        <div class="row font-size-12">
                                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-5">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize" for="dummyExtraPerson_NumberOfGuest">N# extra guest</label>
                                                              <input class="form-control " type="text" value="0" >

                                                            </div>
                                                          </div>
                                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize" for="dummyExtraPerson_Price">Price $</label>
                                                              <input class="form-control"  type="text" value="0.00" >
                                                            </div>
                                                          </div>
                                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize" for="dummyExtraPerson_Price">&nbsp;</label>
                                                              <div class="btn btn-primary min_width100">save</div>
                                                            </div>

                                                          </div>
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                </tbody>

                                              </table>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="extra_section">
                                              <hr>
                                              <strong> LOS pricing: Length of stay price </strong>
                                              <p>
                                                If you have X amount of guests staying Y amounts of nights the price per night will be Z.
                                              </p>

                                              <button  class="btn btn-primary" id="los_btn" >
                                                Add Price
                                              </button>
                                              <table class="add_p_table" id="los_box" style="display:none">
                                                <thead>
                                                  <tr>
                                                    <th>
                                                      N# Nights
                                                    </th>
                                                    <th>
                                                      Up To N# Guests
                                                    </th>
                                                    <th>
                                                      Price
                                                    </th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr>
                                                    <td colspan="3">
                                                      <div class="content gray-solid-border graybg gray-dark-solid-border">
                                                        <div class=" extra-person-price-editor-header">
                                                          <div class="pull-left text-capitalize font-bold add-or-edit-extra-person-price-title font-size-16">
                                                            Add / Edit Price
                                                          </div>
                                                          <div class="pull-right font-bold">
                                                            <span class="text-capitalize close-editor" id="close_loss">
                                                              <u> Close</u>
                                                            </span>
                                                            <span class="close-editor">
                                                              x
                                                            </span>
                                                          </div>
                                                        </div>
                                                        <div class="row font-size-12">
                                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize">N# nights</label>
                                                              <input class="form-control" type="text" value="0">
                                                            </div>
                                                          </div>
                                                          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <div class="form-group select2-fill">
                                                              <label class=" text-capitalize">Up to N# Guests</label>
                                                              <select class="form-control">
                                                                <option value="0" selected="selected">unrestricted</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                              </select>

                                                            </div>
                                                          </div>
                                                          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize">Price </label>
                                                              <input class="form-control " type="text" value="0.00" >
                                                            </div>
                                                          </div>

                                                          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                                            <div class="form-group">
                                                              <label class=" text-capitalize">&nbsp; </label>
                                                              <div class="btn btn-primary ">save</div>
                                                            </div>

                                                          </div>
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                </tbody>

                                              </table>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="extra_section">
                                              <hr>
                                              <p>
                                                <strong> Discounts: last minute </strong></p>


                                                <button  class="btn btn-primary" id="last_minute_btn">
                                                  Add Price
                                                </button>
                                                <table class="add_p_table " style="display: none;" id="last_minute_box" >
                                                  <thead>
                                                    <tr>
                                                      <th class="col-md-3 col-lg-3 font-bold text-capitalize ">From days<br><span class="font-size-12 text-lowercase">before arrival</span></th>
                                                      <th class="col-md-3 col-lg-3 font-bold text-capitalize ">To days<br><span class="font-size-12 text-lowercase">before arrival</span></th>
                                                      <th class="col-md-3 col-lg-3 font-bold text-capitalize ">discount</th>
                                                      <th></th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <tr class="last-minute-display add-new" style="display: none;">

                                                      <td class="last-minut-from font-bold">0</td>
                                                      <td class="last-minut-to font-bold">0</td>
                                                      <td class="last-minut-discount font-bold">
                                                        <span class="discount">0</span>&nbsp;<span>%</span>
                                                      </td>
                                                      <td>
                                                        <div class="last-minute-action-buttons text-right">
                                                          <span class="glyphicon glyphicon-edit blue-color"></span>
                                                          <span class="glyphicon glyphicon-trash warning-color" data-id="0-0"></span>
                                                        </div>
                                                      </td>

                                                    </tr><tr class="last-minute-editor add-new" style="display: table-row;">

                                                      <td colspan="5" class="padding-left0 padding-right0">

                                                        <div class="content gray-solid-border graybg gray-dark-solid-border">
                                                          <div class=" last-minute-editor-header">
                                                            <div class="pull-left text-capitalize font-bold add-or-edit-last-minute-title">
                                                              Add / Edit Discount
                                                            </div>
                                                            <div class="pull-right font-bold" id="close_last_minute">
                                                              <span class="text-capitalize close-editor">
                                                                <u> Close</u>
                                                              </span>
                                                              <span class="close-editor">
                                                                x
                                                              </span>
                                                            </div>
                                                          </div>
                                                          <div class="row font-size-12">
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3 ">
                                                              <div class="form-group">
                                                                <label for="dummyLastMinute_FromDays" class="text-capitalize">
                                                                  From days
                                                                  <br>
                                                                  <span class="font-size-12 text-lowercase">before arrival</span>
                                                                </label>
                                                                <input class="form-control " type="text" value="0" >
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                                              <div class="form-group">
                                                                <label for="dummyLastMinute_ToDays" class="text-capitalize">
                                                                  To days
                                                                  <br>
                                                                  <span class="font-size-12 text-lowercase">before arrival</span>
                                                                </label>
                                                                <input class="form-control " id="dummyLastMinute_ToDays" type="text" value="0" >
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="margin-top:18px;">
                                                              <div class="form-group">
                                                                <label class="nowrap text-capitalize" for="dummyLastMinute_Discount">discount (%)</label>
                                                                <input class="form-control " type="text" value="0" >
                                                              </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 text-right" style="margin-top:56px;">
                                                              <div class="save-last-minute-btn-container">
                                                                <div class="btn btn-primary min_width100">save</div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>

                                                      </td>
                                                    </tr></tbody>
                                                  </table>
                                                </div>
                                              </div>
                                              <div class="col-md-6">
                                                <div class="extra_section">
                                                  <hr>
                                                  <p>
                                                    <strong>Discounts: Length of Stay </strong></p>


                                                    <button  class="btn btn-primary" id="l_stay_btn">
                                                      Add Price
                                                    </button>
                                                    <table class="add_p_table"  style="display: none" id="l_stay_box">
                                                      <thead>
                                                        <tr>
                                                          <th class="col-md-3 col-lg-3 font-bold text-capitalize ">stays longer<br><span class="font-size-12 text-lowercase">than (days)</span></th>
                                                          <th class="col-md-3 col-lg-3 font-bold text-capitalize ">shorter<br><span class="font-size-12 text-lowercase">than (days)</span></th>
                                                          <th class="col-md-3 col-lg-3 font-bold text-capitalize ">discount</th>
                                                          <th></th>
                                                        </tr>
                                                      </thead>
                                                      <tbody>
                                                        <tr class="length-of-stay-display add-new" style="display: none;">


                                                          <td class="length-of-stay-longer font-bold">0</td>
                                                          <td class="length-of-stay-shorter font-bold">0</td>
                                                          <td class="length-of-stay-discount font-bold"><span class="discount">0</span>&nbsp;<span>%</span></td>
                                                          <td>
                                                            <div class="length-of-stay-action-buttons text-right">
                                                              <span class="glyphicon glyphicon-edit blue-color"></span>
                                                              <span class="glyphicon glyphicon-trash warning-color" data-id="0-0"></span>
                                                            </div>
                                                          </td>

                                                        </tr><tr class="length-of-stay-editor add-new" style="display: table-row;">

                                                          <td colspan="5" class="padding-left0 padding-right0">
                                                            <input data-val="False" data-val-date="The field DateFrom must be a date." data-val-required="The DateFrom field is required." id="dummyLengthOfStay_DateFrom" name="dummyLengthOfStay.DateFrom" type="hidden" value="0001-01-01 12:00:00 AM">
                                                            <input data-val="False" data-val-date="The field DateTo must be a date." data-val-required="The DateTo field is required." id="dummyLengthOfStay_DateTo" name="dummyLengthOfStay.DateTo" type="hidden" value="0001-01-01 12:00:00 AM">
                                                            <div class="content gray-solid-border graybg gray-dark-solid-border">
                                                              <div class=" length-of-stay-editor-header">
                                                                <div class="pull-left text-capitalize font-bold add-or-edit-length-of-stay-title">
                                                                  Add / Edit Discount
                                                                </div>
                                                                <div class="pull-right font-bold">
                                                                  <span class="text-capitalize close-editor" id="close_l_stay">
                                                                    <u> Close</u>
                                                                  </span>
                                                                  <span class="close-editor">
                                                                    x
                                                                  </span>
                                                                </div>
                                                              </div>
                                                              <div class="row font-size-12">
                                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 ">
                                                                  <div class="form-group">
                                                                    <label class=" text-capitalize " for="dummyLengthOfStay_StayLongerDays">stays longer</label>
                                                                    <input class="form-control validate-on-focus-disabled" data-val="true" data-val-fromtodiscountoverlap="Discount can't overlap" data-val-fromtodiscountoverlap-frominputname="StayLongerDays" data-val-fromtodiscountoverlap-groupselector="tr.length-of-stay-editor" data-val-fromtodiscountoverlap-listcontainerselector=".length-of-stay-list" data-val-fromtodiscountoverlap-periodendinputname=".To" data-val-fromtodiscountoverlap-periodstartinputname=".From" data-val-fromtodiscountoverlap-toinputname="StayShorterDays" data-val-number="The field StayLongerDays must be a number." data-val-range="Stay longer must be greater than 0" data-val-range-max="2147483647" data-val-range-min="1" data-val-regex="Only whole numbers are allowed" data-val-regex-pattern="^[1-9]\d*$" data-val-required="The StayLongerDays field is required." id="dummyLengthOfStay_StayLongerDays" name="dummyLengthOfStay.StayLongerDays" type="text" value="0" data-memento="0" aria-required="true">
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 margin-top20">
                                                                  <div class="form-group">
                                                                    <label class=" text-capitalize " for="dummyLengthOfStay_StayShorterDays">shorter</label>
                                                                    <input class="form-control validate-on-focus-disabled" data-val="true" data-val-fromtodiscountoverlap="Discount can't overlap" data-val-fromtodiscountoverlap-frominputname="StayLongerDays" data-val-fromtodiscountoverlap-groupselector="tr.length-of-stay-editor" data-val-fromtodiscountoverlap-listcontainerselector=".length-of-stay-list" data-val-fromtodiscountoverlap-periodendinputname=".To" data-val-fromtodiscountoverlap-periodstartinputname=".From" data-val-fromtodiscountoverlap-toinputname="StayShorterDays" data-val-isgreaterthan="Shorter must be greater than stays longer" data-val-isgreaterthan-allowequal="True" data-val-isgreaterthan-groupselector=".content" data-val-isgreaterthan-onerror="markRowAsWrong($(element).parents('tr').first().prev('.length-of-stay-display'), 'error-greater-then')" data-val-isgreaterthan-onsuccess="markRowAsOk($(element).parents('tr').first().prev('.length-of-stay-display'), 'error-greater-than')" data-val-isgreaterthan-propertytested="StayLongerDays" data-val-number="The field StayShorterDays must be a number." data-val-range="Shorter must be greater than 0" data-val-range-max="2147483647" data-val-range-min="1" data-val-regex="Only whole numbers are allowed" data-val-regex-pattern="^[1-9]\d*$" data-val-required="The StayShorterDays field is required." id="dummyLengthOfStay_StayShorterDays" name="dummyLengthOfStay.StayShorterDays" type="text" value="0" data-memento="0" aria-required="true">
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3  margin-top20">
                                                                  <div class="form-group">
                                                                    <label class="nowrap text-capitalize " for="dummyLengthOfStay_Discount">discount (%)</label>
                                                                    <input class="form-control validate-on-focus-disabled" data-val="true" data-val-number="The field Discount must be a number." data-val-range="The field Discount must be between 1 and 100." data-val-range-max="100" data-val-range-min="1" data-val-required="The Discount field is required." id="dummyLengthOfStay_Discount" name="dummyLengthOfStay.Discount" type="text" value="0" data-memento="0" aria-required="true">
                                                                  </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                                  <div class="save-length-of-stay-btn-container margin-top40" style="    margin-top: 38px;">
                                                                    <div class="btn btn-primary min_width100">save</div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>

                                                          </td>

                                                        </tr></tbody>
                                                      </table>
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <button class="btn btn-primary save_next_btn">Save & Next</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div role="tabpanel" class="tab-pane" id="messages">
                        <div class="panel-body">
                          <div class="taxes_main">
                            <h3>Taxes</h3>
                            <p>Channels need to display your taxes and fees separately to the guest, we recommend you to specify them here</p>

                            <button class="btn btn-primary"> Add Tax</button>
                            <table id="tblTaxes" class="table">
                              <thead>
                                <tr>
                                  <th class="font-bold text-capitalize graytext">Extra tax</th>
                                  <th class="font-bold text-capitalize graytext">amount</th>
                                  <th class="font-bold text-capitalize graytext">Percentage (%)</th>
                                  <th class="font-bold text-capitalize graytext">Calculation by</th>
                                  <th class="font-bold text-capitalize graytext">Collect Time
                                    <span class="collectTimeId" data-original-title="" title="">
                                      <span class="glyphicon glyphicon-info-sign blue-color"></span>
                                    </span>
                                  </th>
                                  <th class="font-bold text-capitalize graytext">optional</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="tax-display tax-CityTax">

                                  <td class="extra-tax font-bold">City tax</td>
                                  <td class="amount font-bold">-</td>
                                  <td class="percentage font-bold">6%</td>
                                  <td class="calculation-by font-bold">Percentage of total price</td>
                                  <td class="collect-time font-bold">Upon booking</td>
                                  <td class="is-optional"><input data-val="true" data-val-required="The IsOptional field is required." disabled="disabled" id="Taxes_Taxes_0__IsOptional" name="Taxes.Taxes[0].IsOptional" type="checkbox" value="true"><input name="Taxes.Taxes[0].IsOptional" type="hidden" value="false"></td>
                                  <td>
                                    <div class="tax-action-buttons text-right">
                                      <span class="glyphicon glyphicon-edit blue-color margin-right10"></span>
                                      <span class="glyphicon glyphicon-trash warning-color" data-id="65725933" data-temp-id="00000000-0000-0000-0000-000000000000"></span>
                                    </div>
                                  </td>
                                </tr>

                                <tr class="tax-display tax-LocalTax" style="display: table-row;">

                                  <td class="extra-tax font-bold">Local tax</td>
                                  <td class="amount font-bold">-</td>
                                  <td class="percentage font-bold">0.5%</td>
                                  <td class="calculation-by font-bold">Percentage of total price</td>
                                  <td class="collect-time font-bold">Upon booking</td>
                                  <td class="is-optional">
                                    <input disabled="disabled" id="Taxes_Taxes_1__IsOptional" name="Taxes.Taxes[1].IsOptional" type="checkbox" value="true"><input name="Taxes.Taxes[1].IsOptional" type="hidden" value="false"></td>
                                    <td>
                                      <div class="tax-action-buttons text-right">
                                        <span class="glyphicon glyphicon-edit blue-color margin-right10"></span>
                                        <span class="glyphicon glyphicon-trash warning-color" data-id="65725934" data-temp-id="00000000-0000-0000-0000-000000000000"></span>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr class="tax-editor tax-LocalTax" style="display: none;">


                                  </tr>

                                  <tr class="empty-row tax-editor add-new">

                                    <td colspan="7">
                                      <form  method="post">
                                        <div class="content gray-solid-border graybg">
                                          <div class="graytext tax-editor-header">
                                            <div class="pull-left text-capitalize font-bold add-or-edit-tax-title">
                                              Add tax
                                            </div>
                                            <div class="pull-right font-bold">
                                              <span class="text-capitalize close-editor">
                                                <u>Close</u>
                                              </span>
                                              <span class="close-editor">
                                                x
                                              </span>
                                            </div>
                                          </div>
                                          <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 select-height-34">
                                              <label class="graytext text-capitalize" for="Taxes_dummyTax_TaxType">Extra tax</label>
                                              <select data-val="true" style="width:100%;" >
                                                <option value="">Please select</option>
                                                <option value="GoodsAndServicesTax">Goods and services tax</option>
                                                <option value="TouristTax">Tourist tax</option>
                                                <option value="VAT">VAT</option>
                                              </select>                </div>
                                              <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                <label class="graytext text-capitalize" for="Taxes_dummyTax_Value">Value  </label>
                                                <input class="form-control" type="text" value="0.00">
                                              </div>
                                              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 select-height-34">
                                                <label class="graytext text-capitalize" for="Taxes_dummyTax_CalculationById">Calculation by</label>
                                                <select data-val="true" style="width:100%;" class="form-control">
                                                  <option value="">Please select</option>
                                                  <option value="IndependentPercentage">Percentage of total price</option>
                                                  <option value="FlatPerStay">Amount per stay</option>
                                                  <option value="FixedPerDay">Amount per night</option>
                                                  <option value="FixedAmountPerWeek">Amount per week</option>
                                                  <option value="FixedAmountPerPerson">Amount per person &amp; stay</option>
                                                  <option value="FixedAmountPerPersonPerDay">Amount per person &amp; night</option>
                                                  <option value="FixedAmountPerPersonPerWeek">Amount per person &amp; week</option>
                                                  <option value="VariableAmountByConsumption">Amount per usage</option>
                                                  <option value="FixedAmountPerSet">Amount per set</option>
                                                  <option value="VariableAmountByUse">Amount per usage &amp; hour</option>
                                                </select>
                                              </div>
                                              <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 select-height-34">
                                                <label class="graytext text-capitalize" for="Taxes_dummyTax_CollectTimeById">Collect Time</label>

                                                <select style="width:100%;" >
                                                  <option value="">Please select</option>
                                                  <option value="UponBooking">Upon booking</option>
                                                  <option value="UponArrival">Upon arrival</option>
                                                </select>
                                              </div>
                                              <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                <label class="graytext text-capitalize" for="Taxes_dummyTax_IsOptional">optional</label>
                                                <input class="is-optional-checkbox" type="checkbox" value="true">

                                              </div>
                                              <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                <label class="graytext text-capitalize" for="Taxes_dummyTax_IsOptional">&nbsp;</label>
                                                <div class="save-tax-btn-container">
                                                  <button class="btn btn-primary " >
                                                    save
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </form></td>
                                      </tr>
                                    </tbody>
                                  </table>

                                  <h3>Extra charges</h3>


                                  <button class="btn btn-primary"> Add extra charges</button>
                                  <table id="tblExtraCharges" class="table">
                                    <thead>
                                      <tr>
                                        <th class="font-bold text-capitalize graytext">Extra charge</th>
                                        <th class="font-bold text-capitalize graytext">amount</th>
                                        <th class="font-bold text-capitalize graytext">Percentage (%)</th>
                                        <th class="font-bold text-capitalize graytext">Calculation by</th>
                                        <th class="font-bold text-capitalize graytext">Collect Time
                                          <span class="collectTimeId" data-original-title="" title="">
                                            <span class="glyphicon glyphicon-info-sign blue-color"></span>
                                          </span>
                                        </th>
                                        <th class="font-bold text-capitalize graytext">optional</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="extra-charge-display extra-charge-CleaningFee" style="display: none;">

                                        <td class="extra-charge font-bold">Cleaning fee</td>
                                        <td class="amount font-bold">250.00</td>
                                        <td class="percentage font-bold">-</td>
                                        <td class="calculation-by font-bold">Amount per stay</td>
                                        <td class="collect-time font-bold">Upon booking</td>
                                        <td class="is-optional"><input data-val="true" data-val-required="The IsOptional field is required." disabled="disabled" id="ExtraCharges_ExtraCharges_0__IsOptional" name="ExtraCharges.ExtraCharges[0].IsOptional" type="checkbox" value="true"><input name="ExtraCharges.ExtraCharges[0].IsOptional" type="hidden" value="false"></td>
                                        <td>
                                          <div class="extra-charge-action-buttons text-right">
                                            <span class="glyphicon glyphicon-edit blue-color margin-right10"></span>
                                            <span class="glyphicon glyphicon-trash warning-color" data-id="0" data-temp-id="b2316081-4cb3-4ca7-9be8-a7a97f8c1e01"></span>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr class="extra-charge-editor tax-editor  extra-charge-CleaningFee" style="display: table-row;">
                                        <td colspan="7">
                                          <form >
                                            <div class="content gray-solid-border graybg">
                                              <div class="graytext extra-charge-editor-header">
                                                <div class="pull-left text-capitalize font-bold add-or-edit-extra-charge-title">
                                                  Add extra charge
                                                </div>
                                                <div class="pull-right font-bold">
                                                  <span class="text-capitalize close-editor">
                                                    <u>Close</u>
                                                  </span>
                                                  <span class="close-editor">
                                                    x
                                                  </span>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 select-height-34">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__ExtraChargeType">Extra charge</label>
                                                  <select>
                                                    <option value="">Please select</option>
                                                    <option value="AirConditioningFee">Air conditioning fee</option>
                                                    <option value="AirportShuttleFee">Airport shuttle fee</option>
                                                    <option value="BedLinenFee">Bed linen fee</option>
                                                    <option value="BookingFee">Booking fee</option>
                                                    <option value="ChildrenExtraFee">Children extra fee</option>
                                                    <option value="CleaningFee" selected="selected">Cleaning fee</option>
                                                    <option value="ClubCardFee">Club card fee</option>
                                                    <option value="ConservationFee">Conservation fee</option>
                                                    <option value="CreditCardFee">Credit card fee</option>
                                                    <option value="DestinationFee">Destination Fee</option>
                                                    <option value="ElectricityFee">Electricity fee</option>
                                                    <option value="EnvironmentFee">Environment fee</option>
                                                    <option value="ExtraBedFee">Extra beds fee</option>
                                                    <option value="GasFee">Gas fee</option><option value="HeatingFee">Heating fee</option>
                                                    <option value="HousekeepingFee">Housekeeping fee</option>
                                                    <option value="Insurance">Insurance</option>
                                                    <option value="InternetFee">Internet fee</option>
                                                    <option value="KitchenLinenFee">Kitchen linen fee</option>
                                                    <option value="LinenPackageFee">Linen package fee</option>
                                                    <option value="OilFee">Oil fee</option>
                                                    <option value="ParkingFee">Parking fee</option>
                                                    <option value="PetFee">Pet fee</option>
                                                    <option value="ResortFee">Resort Fee</option>
                                                    <option value="SeaPlaneFee">Sea plane fee</option>
                                                    <option value="ServiceFee">Service fee</option>
                                                    <option value="ShuttleBoatFee">Shuttle boat fee</option>
                                                    <option value="SkiPass">Ski pass</option>
                                                    <option value="TourismFee">Tourism Fee</option>
                                                    <option value="TowelsFee">Towels fee</option>
                                                    <option value="TransferFee">Transfer fee</option>
                                                    <option value="VisaSupportFee">Visa support fee</option>
                                                    <option value="WaterParkFee">Water park fee</option>
                                                    <option value="WaterUsageFee">Water usage fee</option>
                                                    <option value="WoodFee">Wood fee</option>
                                                    <option value="WristbandFee">Wristband fee</option>
                                                  </select>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__Value">Value  </label>
                                                  <input class="form-control" type="text" value="250.00" aria-required="true" aria-invalid="false">
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 select-height-34">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__CalculationById">Calculation by</label>
                                                  <select style="width:100%;" ><option value="">Please select</option>
                                                    <option value="IndependentPercentage">Percentage of total price</option>
                                                    <option selected="selected" value="FlatPerStay">Amount per stay</option>
                                                    <option value="FixedPerDay">Amount per night</option>
                                                    <option value="FixedAmountPerWeek">Amount per week</option>
                                                    <option value="FixedAmountPerPerson">Amount per person &amp; stay</option>
                                                    <option value="FixedAmountPerPersonPerDay">Amount per person &amp; night</option>
                                                    <option value="FixedAmountPerPersonPerWeek">Amount per person &amp; week</option>
                                                    <option value="VariableAmountByConsumption">Amount per usage</option>
                                                    <option value="FixedAmountPerSet">Amount per set</option>
                                                    <option value="VariableAmountByUse">Amount per usage &amp; hour</option>
                                                  </select>

                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 select-height-34">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__CollectTimeById">Collect Time</label>

                                                  <select ><option value="">Please select</option>
                                                    <option selected="selected" value="UponBooking">Upon booking</option>
                                                    <option value="UponArrival">Upon arrival</option>
                                                  </select>

                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__IsOptional">optional</label>
                                                  <input class="is-optional-checkbox"  type="checkbox" value="true"><input name="ExtraCharges.ExtraCharges[0].IsOptional" type="hidden" value="false">
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                  <label class="graytext text-capitalize" for="ExtraCharges_ExtraCharges_0__IsOptional">&nbsp;</label>
                                                  <div class="save-extra-charge-btn-container">
                                                    <button class="btn btn-primary">
                                                      save
                                                    </button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </form>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="settings">...</div>
                          </div>

                        </div>
                      </div>

                    </div>
                  </div>

                  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
                  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
                  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

                  <script>
                  $(document).ready(function(){
                    var today = new Date(
                      new Date().getFullYear(),
                      new Date().getMonth(),
                      new Date().getDate()
                    );
                    $("#dateFrom").datepicker({
                      uiLibrary: "bootstrap",
                      iconsLibrary: "fontawesome",
                      minDate: today,
                      maxDate: function() {
                        return $("#dateTo").val();
                      }
                    });
                    $("#dateTo").datepicker({
                      uiLibrary: "bootstrap",
                      iconsLibrary: "fontawesome",
                      minDate: function() {
                        return $("#dateFrom").val();
                      }
                    });
                  })
                </script>
