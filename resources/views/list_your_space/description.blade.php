﻿ <style type="text/css">
       html[lang="ar"] #js-manage-listing-nav{position: fixed !important;}
          @media (min-width: 767px){
   
  #ajax_container{margin-left:25%;}
  html[lang="ar"] #js-manage-listing-nav{position: fixed;}
  html[lang="ar"] #ajax_container{margin-right:25% !important;margin-left: 0px !important;}
}
  @media(min-width: 1100px){
  #ajax_container{margin-left: 16.66667%;}
  html[lang="ar"] #ajax_container{margin-right: 16.66667% !important; margin-left: 0px !important;}
}
    </style>
    <div class="manage-listing-content-container" id="js-manage-listing-content-container" style="position:relative;top: -22px;">
      <div class="manage-listing-content-wrapper">
        <div class="manage-listing-content col-lg-7 col-md-7" id="js-manage-listing-content"><div>
  
<div class="row-space-4">
  <div class="row">
    
      <h3 class="col-12">{{ trans('messages.lys.amenities_title') }}</h3>
    
  </div>
  <p class="text-muted">{{ trans('messages.lys.amenities_desc',['site_name'=>$site_name]) }}</p>
</div>

  <hr>
    <form name="overview">
    <div class="js-section" >
      <div class="js-saving-progress saving-progress" style="display: none;">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>
  
      <div class="row-space-2 clearfix" id="help-panel-name" ng-init="name='{{addslashes($result->name)}}'">
        <div class="row row-space-top-2">
          <div class="col-4">
            <label class="label-large">{{ trans('messages.lys.listing_name') }}</label>
          </div>
          <div class="col-8">
            <div id="js-name-count" class="row-space-top-1 h6 label-large text-right">
              <span ng-bind="35 - text_length_calc(name)">35</span> {{ trans('messages.lys.characters_left') }}
            </div>
          </div>
        </div>

        <input type="text" name="name" id='name' value="{!! $result->name !!}" class="overview-title input-large name_required" placeholder="{{ trans('messages.lys.name_placeholder') }}" maxlength="35" ng-model="name">
        <p class="hide icon-rausch error-too-long row-space-top-1">{{ trans('messages.lys.shorten_to_save_changes') }}</p>

        <p class="hide icon-rausch error-value-required row-space-top-1 name_required_msg">{{ trans('messages.lys.value_is_required') }}</p>

      </div>

      <div id="summary"  ng-init="summary='{{addslashes($result->summary)}}'">
        <div class="row">
          <div class="col-4">
            <label class="label-large">{{ trans('messages.lys.summary') }}</label>
          </div>
          <div class="col-8">
            <div id="js-summary-count" class="row-space-top-1 h6 label-large text-right">
              <span ng-bind="500 - text_length_calc(summary)">500</span> {{ trans('messages.lys.characters_left') }}
            </div>
          </div>
        </div>

        <textarea class="overview-summary input-large summary_required" name="summary" rows="6" placeholder="{{ trans('messages.lys.summary_placeholder') }}" maxlength="500" ng-model="summary">{!! $result->summary !!}</textarea>
      </div>
      <p class="hide icon-rausch error-too-long row-space-top-1">{{ trans('messages.lys.shorten_to_save_changes') }}</p>

      <p class="hide icon-rausch error-value-required row-space-top-1 summary_required_msg">{{ trans('messages.lys.value_is_required') }}</p>

    </div>
    </form>
    
  <p class="row-space-top-6 not-post-listed write_more_p">
    {{ trans('messages.lys.you_can_add_more') }} <a href="javascript:void(0);" id="js-write-more">{{ trans('messages.lys.details') }}</a> {{ trans('messages.lys.tell_travelers_about_your_space') }}
  </p>

    <hr class="more_details_hr" style="display:none;">

<div class="js-section" id="js-section-details" style="display:none;">
  <div class="js-saving-progress saving-progress help-panel-saving" style="display: none;">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>

  <h4>{{ trans('messages.lys.the_trip') }}</h4>

    <div class="row-space-2" id="help-panel-space">
      <label class="label-large">{{ trans('messages.lys.the_space') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="space" rows="4" placeholder="{{ trans('messages.lys.space_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->space : ''}}</textarea>
      @dd($result->rooms_description);
    </div>
    <div class="row-space-2" id="help-panel-access">
      <label class="label-large">{{ trans('messages.lys.guest_access') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="access" rows="4" placeholder="{{ trans('messages.lys.guest_access_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->access : '' }}</textarea>
    </div>
    <div class="row-space-2" id="help-panel-interaction">
      <label class="label-large">{{ trans('messages.lys.interaction_with_guests') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="interaction" rows="4" placeholder="{{ trans('messages.lys.interaction_with_guests_placeholder') }}">{!is_null($result->rooms_description) ? { $result->rooms_description-> : ''interaction }}</textarea>
    </div>
    <div class="row-space-2" id="help-panel-notes">
      <label class="label-large">{{ trans('messages.lys.other_things_note') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="notes" rows="4" placeholder="{{ trans('messages.lys.other_things_note_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->notes : '' }}</textarea>
    </div>
    <div class="row-space-2" id="help-panel-house-rules">
      <label class="label-large">{{ trans('messages.lys.house_rules') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="house_rules" rows="4" placeholder="{{ trans('messages.lys.house_rules_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->house_rules : '' }}</textarea>
    </div>

</div>

  <hr class="row-space-top-6 row-space-5 more_details_hr" style="display:none;">

<div class="js-section" id="js-section-details_2" style="display:none;">
  <div class="js-saving-progress saving-progress help-panel-neigh-saving" style="display: none;">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>

  <h4>{{ trans('messages.lys.the_neighborhood') }}</h4>

    <div class="row-space-2" id="help-panel-neighborhood">
      <label class="label-large">{{ trans('messages.lys.overview') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="neighborhood_overview" rows="4" placeholder="{{ trans('messages.lys.overview_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->neighborhood_overview : '' }}</textarea>
    </div>
    <div id="help-panel-transit">
      <label class="label-large">{{ trans('messages.lys.getting_around') }}</label>
      <textarea class="input-large textarea-resize-vertical" name="transit" rows="4" placeholder="{{ trans('messages.lys.getting_around_placeholder') }}">{{ !is_null($result->rooms_description) ? $result->rooms_description->transit : '' }}</textarea>
    </div>
  
</div>
  

  <div class="not-post-listed row row-space-top-6 progress-buttons">
  <div class="col-12">
    <div class="separator"></div>
  </div>
  <div class="col-2 row-space-top-1 next_step">
    
      <a class="back-section-button" href="{{ url('manage-listing/'.$room_id.'/basics') }}" data-prevent-default="">{{ trans('messages.lys.back') }}</a>
    
  </div>
  <div class="col-10 text-right next_step">
    
    
      <a class="btn btn-large btn-primary next-section-button" href="{{ url('manage-listing/'.$room_id.'/location') }}" data-prevent-default="">
        {{ trans('messages.lys.next') }}
      </a>
    
  </div>
</div>



</div></div>
        <div class="manage-listing-help col-lg-4 pos-fix col-md-4 hide-sm" id="js-manage-listing-help"><div class="manage-listing-help-panel-wrapper">
  <div class="panel manage-listing-help-panel">
    <div class="help-header-icon-container text-center va-container-h">
      {!! Html::image('images/lightbulb2x.png', '', ['class' => 'col-center', 'width' => '50', 'height' => '50']) !!}
    </div>
    <div class="panel-body">
      <h4 class="text-center">{{ trans('messages.lys.listing_name') }}</h4>
      
  <p>{{ trans('messages.lys.listing_name_desc') }}</p>
  <p>{{ trans('messages.lys.example_name') }}</p>

    </div>
  </div>
</div>

</div>
      </div>
      <div class="manage-listing-content-background"></div>
    </div>

