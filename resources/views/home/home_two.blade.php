
@extends('template_two')

@section('main')
<div class="clicked_overlay">

</div>
<main ng-controller="HomePageController" ng-init="initSearch()">
  <div class="home_bg">
    <div class="home_search_box">
      <div class="page-container-responsive new-page-container mini-rel-top">
        <div class="panel">
          <div id="discovery-container"  >
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <div class="no-mar-sm" >
                <div class="textHeaderContainerMarginTop_13o8qr2-o_O-textHeaderContainerWidth_peyti4 row-space-7">
                  <!-- <h1 class="textHeader_8yxs9w"><span class="textHeaderTitle_153t78d-o_O-textHeader_rausch_hp6jb4">{{ $site_name }} </span><br>
                  {{ trans('messages.home.desc') }}
                </h1> -->
                <h1 class="textHeader_8yxs9w">
                  <span
                  class="txt-rotate textHeaderTitle_153t78d-o_O-textHeader_rausch_hp6jb4"
                  data-period="2000"
                  data-rotate='[ "Lorem ipsum dolor sit amet.", "consectetur adipiscing elit.", "Nunc sed nulla posuere leo.", "consectetur vulputate.", "Etiam volutpat a massa ac viverra!" ]'></span>
                </h1>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 hide-sm">
            <div class="container_e4p5a8"><!-- react-empty: 18439 -->
              <form class="simple-search" name="home__search" ng-submit="seach_properties(home__search)">
                <input type="hidden" name="base__url" ng-model="base__url" ng-init="base__url='{{ url() }}'" />
                <div class="container_1tvwao0">
                  <div class="container_mv0xzc" style="width: 100%;">
                    <div class="label_1om3jpt">{{ trans('messages.header.where') }}</div>
                    <div class="largeGeocomplete_1g20x4k">
                      <div class="container_gor68n">
                        <div>
                          <div class="container_e296pg">
                            <div class="container_36rlri-o_O-block_r99te6">
                              <label class="label_hidden_1m8bb6v" >{{ trans('messages.header.where') }}</label>
                              <div class="container_ssgg6h-o_O-container_noMargins_18e9acw-o_O-borderless_mflkgb-o_O-block_r99te6">
                                <div class="inputContainer_178faes">
                                  <input autocomplete="off" class="input_70aky9-o_O-input_book_f17nnd-o_O-input_ellipsis_1bgueul-o_O-input_defaultPlaceholder_jsyynz"
                                  id="header-search-input" name="location" ng-model="location" placeholder="{{ trans('messages.header.anywhere') }}" value="" type="text" ng-keyup="getLocationList(location)" />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="search-box">
                        <ul id="search_google_ul_2"></ul>
                      </div>
                    </div>
                    <div class="focusUnderline_7131v4"></div>
                  </div>
                  <div class="container_mv0xzc-o_O-borderLeft_1ujj4hk-o_O-borderRight_1x9yfnn" style="width: 100%;">
                    <div class="label_1om3jpt">{{ trans('messages.header.when') }}</div>
                    <div class="webcot-lg-datepicker webcot-lg-datepicker--jumbo">
                      <div class="dateRangePicker_e296pg-o_O-hidden_ajz5vs">
                        <div class="DateRangePickerDiv">
                          <div>
                            <div class="DateRangePickerInput">
                              <div class="DateInput">
                                <input aria-label="Check In" class="DateInput__input needsclick" id="checkin" name="checkin" value="" placeholder="{{ trans('messages.header.checkin') }}" autocomplete="off" aria-describedby="DateInput__screen-reader-message-startDate" type="text" />
                                <div class="DateInput__display-text">{{ trans('messages.header.checkin') }}</div>
                              </div>
                              <div class="DateRangePickerInput__arrow" aria-hidden="true" role="presentation">-
                              </div>
                              <div class="DateInput">
                                <input aria-label="Check Out" class="DateInput__input needsclick" id="checkout" name="checkout" value="" placeholder="{{ trans('messages.header.checkout') }}" autocomplete="off" aria-describedby="DateInput__screen-reader-message-endDate" type="text" />
                                <div class="DateInput__display-text">{{ trans('messages.header.checkout') }}</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <button type="button" tabindex="-1" class="button_1b5aaxl-o_O-button_large_c3pob4">
                          <span class="icon_12hl23n"></span>
                          <span class="copy_14aozyc-o_O-copy_fakePlaceholder_10k87om">{{ trans('messages.header.anytime') }}</span>
                        </button>
                      </div>
                    </div>
                    <div class="focusUnderline_7131v4"></div>
                  </div>
                  <div class="container_mv0xzc" style="width: 100%;">
                    <div class="label_1om3jpt col-md-6 padding_left"> {{ trans('messages.header.guest') }}</div>
                    <div>
                      <select id="guests" name="guests" class="col-md-6 ">
                        @for($i=1;$i<=16;$i++)
                        <option value="{{ $i }}"> {{ ($i == '16') ? $i.'+ '.trans_choice('messages.home.guest',$i) : $i.' '.trans_choice('messages.home.guest',$i) }} </option>
                        @endfor
                      </select>
                      <div class="container_mv0xzc" style="width: 0%;"><!-- react-text: 18478 --><!-- /react-text -->
                        <button type="submit" class="searchButton_n8fchz">
                          <span>{{ trans('messages.home.search') }}</span>
                        </button>
                        <div class="focusUnderline_7131v4"></div>
                      </div>
                    </div>
                  </div>
                  <div class="focusUnderline_7131v4">

                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <!-- mobile view header -->

      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 show-sm hide-md">
        <div class="searchBarWrapper_1aq8p3r">
          <div class="container_puzkdo">
            <div>
              <div data-id="SearchBarSmall" class="container_1tvwao0">
                <div class="container_mv0xzc" style="width: 100%;">

                  <button type="button" class="button_1b5aaxl button-sm-search">
                    <span class="icon_12hl23n">
                      <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 18px; width: 18px;"><path fill-rule="nonzero"></path></svg>
                    </span>
                    <span class="copy_14aozyc">{{ trans('messages.header.anywhere') }} · {{ trans('messages.header.anytime') }} · 1 {{ trans('messages.header.guest') }}</span></button>
                    <div class="focusUnderline_7131v4"></div></div></div><!-- react-empty: 29505 -->
                  </div>
                </div></div>


                <div class="panel-drop-down hide-drop-down" style="z-index: 2000;">
                  <div class="panelContent_1jzf86v">
                    <div class="container_gvf938-o_O-container_dropdown_bed46g">
                      <div class="left_egy8rd">
                        <button aria-haspopup="false" aria-expanded="false" class="container_1rp5252" type="button" style="padding: 20px; margin: -20px;">
                          <svg viewBox="0 0 18 18" role="img" aria-label="Close" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 16px; width: 16px;"><path fill-rule="evenodd"></path></svg>
                        </button>
                      </div>
                      <div class="right_8ydhe">
                        <div class="text_5mbkop-o_O-size_small_1gg2mc">
                          <button aria-disabled="false" class="component_9w5i1l-o_O-component_button_r8o91c" type="button"><span>Clear all</span>
                          </button>
                        </div>
                      </div>
                    </div>

                    <div class="body_1sn4o6s-o_O-body_dropdown_7xdft6 arrow-button">
                      <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                        <span class="icon_12hl23n">
                          <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><g fill-rule="evenodd"><path></path></g></svg>
                        </span>
                        <span class="copy_14aozyc">
                          <span>{{ trans('messages.header.anywhere') }}</span></span>
                        </button>
                        <div style="margin-top: 16px; margin-bottom: 16px;">
                          <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                            <span class="icon_12hl23n">
                              <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><path ></path></svg>
                            </span>
                            <span class="copy_14aozyc">{{ trans('messages.header.anytime') }}</span>
                          </button>
                        </div>
                        <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                          <span class="icon_12hl23n">
                            <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><path></path></svg>
                          </span>
                          <span class="copy_14aozyc"><span><span>1 {{ trans('messages.header.guest') }}</span></span></span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="whole-section">
            <div class="page-container-responsive new-page-container mini-rel-top row-space-top-1">


              @if($room_recommented_view != 0 || $room_view_count != 0 || @$res_count != 0)
              <div class="lazy-load-div lazy-load col-md-12 col-lg-12 col-sm-12 col-xs-12  p-0" id="lazy_load_slider" style="display:none">

                @if(@$res_count != 0 )
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="padding:0px;     margin: 45px 0px 15px;">
                    <h3 class="rowHeader pull-left"><!-- react-text: 38121 -->{{ trans('messages.header.justbooked') }}<!-- /react-text --></h3>
                    <div class="seeMoreContainer_11b8zgn pull-right">
                      @if(@$res_count > 3)
                      <a href="{{ url('s') }}">
                        <button class="button_ops1o9-o_O-text_13lu1ne-o_O-button_flushRight_s5eog0">
                          <span class="text_13lu1ne"><span>{{ trans('messages.header.seeall') }}</span></span>
                          <svg viewBox="0 0 18 18" role="presentation" aria-hidden="true" focusable="false" style="fill: currentcolor; height: 10px; width: 10px;"><path fill-rule="evenodd" d="M4.293 1.707A1 1 0 1 1 5.708.293l7.995 8a1 1 0 0 1 0 1.414l-7.995 8a1 1 0 1 1-1.415-1.414L11.583 9l-7.29-7.293z"></path  ></svg>
                        </button></a>
                        @endif

                      </div>
                    </div>

                    <div class="home-bx-slider col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="position:relative;padding:0px;">
                      <ul class="bxslider">
                        @foreach(@$reservation as $fetch_data)
                        @if($fetch_data->total)
                        @if($fetch_data->rooms->photo_name)
                        <li><a href="{{ url('rooms').'/'.$fetch_data->room_id }}"><img src= {{ url().'/images/'.$fetch_data->rooms->photo_name}}  ></a>
                          <div class="panel-body panel-card-section" style="padding:10px 0px;">
                            <div class="media">
                              <a href= " {{ url('rooms').'/'.$fetch_data->room_id }} "   target="listing_10001" class="text-normal" style="text-decoration:none !important;">
                                <h3 title="Castro one-bedroom suite" itemprop="name" class="h5 listing-name text-truncate row-space-top-1 ng-binding" style="white-space:normal;">
                                  <span style="font-size:22px;color:#555;" >{{$fetch_data->currency->symbol }}{{ $fetch_data->total }}<!-- /react-text --></span>

                                  @if($fetch_data->rooms->name != '')
                                  {!! \Illuminate\Support\Str::words($fetch_data->rooms->name,3,'...')  !!}
                                  @endif

                                </h3>
                              </a>
                              <div itemprop="description" class="pull-left text-muted listing-location text-truncate"><a href="{{ url('rooms').'/'.$fetch_data->room_id }}" class="text-normal link-reset pull-left" style="margin-top:5px !important;">
                                <span class="pull-left" > @if($fetch_data->room_category != '')
                                  {{ $fetch_data->room_category }}
                                  @endif
                                </span>
                                <span class="pull-left" >
                                  &nbsp;&nbsp;&nbsp; {!! @$fetch_data->rooms->overall_star_rating !!}
                                </span>
                                <a href="{{ url('rooms').'/'.$fetch_data->room_id }}">
                                  <span class="pull-left mr_mb" style="padding-left: 5px;" >
                                    <span class="pull-left r-count ng-binding " style="font-size:15px;color:#555;">@if($fetch_data->rooms->reviews_count) {{ $fetch_data->rooms->reviews_count }} @endif</span>
                                    <span class="pull-left r-label ng-binding" style="font-size:12px;color:#555;">
                                      @if($fetch_data->rooms->overall_star_rating) {{ trans_choice('messages.header.review',$fetch_data->rooms->reviews_count) }} @endif
                                    </span>
                                  </span>
                                </a>
                              </div>
                            </div>
                          </div>
                        </li>
                        @endif
                        @endif
                        @endforeach
                      </ul>
                    </div>
                  </div>
                  @endif
                  @if(@$room_recommented_view !=0 )
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="padding:0px;     margin: 45px 0px 15px;">
                      <h3 class="rowHeader pull-left"><!-- react-text: 38121 -->{{ trans('messages.header.recommend') }}<!-- /react-text --></h3>
                      <div class="seeMoreContainer_11b8zgn pull-right">

                        @if(@$room_recommented_view > 3)
                        <a href="{{ url('s') }}"><button class="button_ops1o9-o_O-text_13lu1ne-o_O-button_flushRight_s5eog0">

                          <span class="text_13lu1ne"> <span>{{ trans('messages.header.seeall') }}</span></span>


                          <svg viewBox="0 0 18 18" role="presentation" aria-hidden="true" focusable="false" style="fill: currentcolor; height: 10px; width: 10px;"><path fill-rule="evenodd" d="M4.293 1.707A1 1 0 1 1 5.708.293l7.995 8a1 1 0 0 1 0 1.414l-7.995 8a1 1 0 1 1-1.415-1.414L11.583 9l-7.29-7.293z"></path></svg>
                        </button></a>
                        @endif

                      </div>
                    </div>
                    <div class="home-bx-slider col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="position:relative;padding:0px;">

                    </div>
                  </div>
                  @endif
                  @if(@$room_view_count != 0)
                  <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="padding:0px;     margin: 45px 0px 15px;">
                      <h3 class="rowHeader pull-left"><!-- react-text: 38121 -->{{ trans('messages.header.most_viewed') }}<!-- /react-text --></h3>
                      <div class="seeMoreContainer_11b8zgn pull-right">
                        @if(@$room_view_count > 3)
                        <a href="{{ url('s') }}">
                          <button class="button_ops1o9-o_O-text_13lu1ne-o_O-button_flushRight_s5eog0">
                            <span class="text_13lu1ne"><span>{{ trans('messages.header.seeall') }}</span> </span>
                            <svg viewBox="0 0 18 18" role="presentation" aria-hidden="true" focusable="false" style="fill: currentcolor; height: 10px; width: 10px;"><path fill-rule="evenodd" d="M4.293 1.707A1 1 0 1 1 5.708.293l7.995 8a1 1 0 0 1 0 1.414l-7.995 8a1 1 0 1 1-1.415-1.414L11.583 9l-7.29-7.293z"></path></svg>
                          </button>
                        </a>
                        @endif
                      </div>
                    </div>

                    <div class="home-bx-slider col-md-12 col-lg-12 col-sm-12 col-xs-12 " style="position:relative;padding:0px;">
                      <ul class="bxslider">
                        @foreach(@$view_count as $view_count_data)
                        @if(@$view_count_data->rooms_price->night != '')
                        @if(@$view_count_data->photo_name != '')
                        <li><a href="{{ url('rooms').'/'.$view_count_data->id }}"><img src="{{ url().'/images/'.$view_count_data->photo_name}}" /></a>
                          <div class="panel-body panel-card-section" style="padding:10px 0px;">
                            <div class="media">

                              <a href="{{ url('rooms').'/'.$view_count_data->id }}" target="listing_10001" class="text-normal" style="text-decoration:none !important;">

                                <h3 title="Castro one-bedroom suite" itemprop="name" class="h5 listing-name text-truncate row-space-top-1 ng-binding" style="white-space:normal;">
                                  <span style="font-size:22px;color:#555;">
                                    {{ @$view_count_data->rooms_price->currency->symbol }}
                                    {{ @$view_count_data->rooms_price->night }}<!-- /react-text --></span>
                                    {!! \Illuminate\Support\Str::words($view_count_data->name,3 ,'...')  !!}
                                  </h3>
                                </a>
                                <div itemprop="description" class="pull-left text-muted listing-location text-truncate"><a href="{{ url('rooms').'/'.$view_count_data->id }}" class="text-normal link-reset pull-left" style="margin-top:5px !important;">
                                  <span class="pull-left" > @if($view_count_data->room_type_name != '')
                                    {{ $view_count_data->room_type_name }}
                                    @endif
                                  </span>
                                  <span class="pull-left" >

                                    <span class="pull-left ng-binding" >
                                      &nbsp;&nbsp;&nbsp; {!! $view_count_data->overall_star_rating !!}
                                    </span>
                                  </span>
                                  <a href="{{ url('rooms').'/'.$view_count_data->id }}">
                                    <span class="pull-left" style="padding-left: 5px; ">
                                      <span class="pull-left r-count ng-binding" style="font-size:15px;color:#555;">@if($view_count_data->reviews_count) {{ $view_count_data->reviews_count }} @endif</span>
                                      <span class="pull-left r-label ng-binding" style="font-size:12px;color:#555;">
                                        @if($view_count_data->overall_star_rating) {{ trans_choice('messages.header.review',$view_count_data->reviews_count) }} @endif
                                      </span>
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </li>
                          @endif
                          @endif
                          @endforeach
                        </ul>
                      </div>

                    </div>
                    @endif

                  </div>
                  @endif

                  <div class="discovery-section page-container-no-padding row-space-6" id="discover-recommendations" style="    padding: 0px 15px;">
                    <div class="section-intro text-center row-space row-space-top">
                      <h2 class="row-space-1 text-center"><strong>
                      {{ trans('messages.home.explore_world') }}</strong>
                    </h2>
                    <span class="sub_text" style="    font-size: 18px !important;">{{ trans('messages.home.explore_desc') }}</span>
                  </div>

                  <div id="exTab1" >
                    <ul  class="nav nav-pills discovery-nav-pills">
                      <li class="active">
                        <a  href="#1a" data-toggle="tab">Experience Asia</a>
                      </li>
                      <li><a href="#2a" data-toggle="tab">Explore Europe</a>
                      </li>
                      <li><a href="#3a" data-toggle="tab">Discover North America</a>
                      </li>
                      <li><a href="#4a" data-toggle="tab">Explore Oceania</a>
                      </li>
                      <li><a href="#5a" data-toggle="tab">Experience South America</a>
                      </li>
                      <li><a href="#6a" data-toggle="tab">Explore Africa</a>
                      </li>
                    </ul>

                    <div class="tab-content clearfix">
                      <div class="tab-pane active" id="1a">
                        <div class="discovery-tiles">
                          <div class="row">
                            @for($i=0;$i<= $city_count-1;$i++)

                            @if($i == 0)
                            <div class="col-lg-6 col-md-12 rm-padding-sm">
                              @elseif($i > 0 && $i < 5)
                              <div class=" col-lg-3 col-md-6 col-sm-12 rm-padding-sm half">
                                @elseif($i >= 5 && $i < 8)
                                <div class="col-lg-4 col-md-6 col-sm-12 rm-padding-sm half-small">
                                  @else
                                  <div class="col-lg-6 col-md-6 col-sm-12 rm-padding-sm half-big">
                                    @endif
                                    <div class="discovery-card rm-padding-sm row-space-4 darken-on-hover " style="background-image:url({{ $home_city[$i]->image_url }});">
                                      <a href="{{URL::to('/')}}/s?location={{$home_city[$i]->name}}&source=ds" class="link-reset" data-hook="discovery-card">
                                        <div class="va-container va-container-v va-container-h">
                                          <div class="va-middle text-center text-contrast">
                                            <div class="h2">
                                              <strong>
                                                {{$home_city[$i]->name}}
                                              </strong>
                                            </div>
                                          </div>
                                        </div>
                                      </a>
                                      <div class="hover-info">
                                        <h3 class="location-title"> {{$home_city[$i]->name}}</h3>
                                        <h4 class="price">
                                          <span>Starting price</span>
                                        </h4>
                                        <h4 class="search-in-location ng-scope" >Search properties in this location</h4>
                                      </div>
                                    </div>
                                  </div>
                                  @endfor

                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="2a">
                              <div class="discovery-tiles">
                                <div class="row">
                                  @for($i=0;$i<= $city_count-1;$i++)

                                  @if($i == 0)
                                  <div class="col-lg-6 col-md-12 rm-padding-sm">
                                    @elseif($i > 0 && $i < 5)
                                    <div class=" col-lg-3 col-md-6 col-sm-12 rm-padding-sm half">
                                      @elseif($i >= 5 && $i < 8)
                                      <div class="col-lg-4 col-md-6 col-sm-12 rm-padding-sm half-small">
                                        @else
                                        <div class="col-lg-6 col-md-6 col-sm-12 rm-padding-sm half-big">
                                          @endif
                                          <div class="discovery-card rm-padding-sm row-space-4 darken-on-hover " style="background-image:url({{ $home_city[$i]->image_url }});">
                                            <a href="{{URL::to('/')}}/s?location={{$home_city[$i]->name}}&source=ds" class="link-reset" data-hook="discovery-card">
                                              <div class="va-container va-container-v va-container-h">
                                                <div class="va-middle text-center text-contrast">
                                                  <div class="h2">
                                                    <strong>
                                                      {{$home_city[$i]->name}}
                                                    </strong>
                                                  </div>
                                                </div>
                                              </div>
                                            </a>
                                          </div>
                                        </div>
                                        @endfor

                                      </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="3a">
                                    <div class="discovery-tiles">
                                      <div class="row">
                                        @for($i=0;$i<= $city_count-1;$i++)

                                        @if($i == 0)
                                        <div class="col-lg-6 col-md-12 rm-padding-sm">
                                          @elseif($i > 0 && $i < 5)
                                          <div class=" col-lg-3 col-md-6 col-sm-12 rm-padding-sm half">
                                            @elseif($i >= 5 && $i < 8)
                                            <div class="col-lg-4 col-md-6 col-sm-12 rm-padding-sm half-small">
                                              @else
                                              <div class="col-lg-6 col-md-6 col-sm-12 rm-padding-sm half-big">
                                                @endif
                                                <div class="discovery-card rm-padding-sm row-space-4 darken-on-hover " style="background-image:url({{ $home_city[$i]->image_url }});">
                                                  <a href="{{URL::to('/')}}/s?location={{$home_city[$i]->name}}&source=ds" class="link-reset" data-hook="discovery-card">
                                                    <div class="va-container va-container-v va-container-h">
                                                      <div class="va-middle text-center text-contrast">
                                                        <div class="h2">
                                                          <strong>
                                                            {{$home_city[$i]->name}}
                                                          </strong>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </a>
                                                </div>
                                              </div>
                                              @endfor

                                            </div>
                                          </div>
                                        </div>
                                        <div class="tab-pane" id="4a">
                                          <div class="discovery-tiles">
                                            <div class="row">
                                              @for($i=0;$i<= $city_count-1;$i++)

                                              @if($i == 0)
                                              <div class="col-lg-6 col-md-12 rm-padding-sm">
                                                @elseif($i > 0 && $i < 5)
                                                <div class=" col-lg-3 col-md-6 col-sm-12 rm-padding-sm half">
                                                  @elseif($i >= 5 && $i < 8)
                                                  <div class="col-lg-4 col-md-6 col-sm-12 rm-padding-sm half-small">
                                                    @else
                                                    <div class="col-lg-6 col-md-6 col-sm-12 rm-padding-sm half-big">
                                                      @endif
                                                      <div class="discovery-card rm-padding-sm row-space-4 darken-on-hover " style="background-image:url({{ $home_city[$i]->image_url }});">
                                                        <a href="{{URL::to('/')}}/s?location={{$home_city[$i]->name}}&source=ds" class="link-reset" data-hook="discovery-card">
                                                          <div class="va-container va-container-v va-container-h">
                                                            <div class="va-middle text-center text-contrast">
                                                              <div class="h2">
                                                                <strong>
                                                                  {{$home_city[$i]->name}}
                                                                </strong>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </a>
                                                      </div>
                                                    </div>
                                                    @endfor

                                                  </div>
                                                </div>
                                              </div>
                                            </div>





                                          </div>
                                          <div class="recommended_properties_main">
                                            <div class="row">
                                              <div class="col-md-6">
                                                <div class="title_section">
                                                  <h2>
                                                    <strong>

                                                      Recommended Properties
                                                    </strong>
                                                  </h2>
                                                </div>

                                                <div class="properties_slider">
                                                  <ul id="propertiesGallery">
                                                    <li >
                                                      <img src="{{url()}}/images/properties/1.jpg" />
                                                      <div class="content_caption_p">
                                                        <strong>Hollyhock: a Hobbit House in Downtown Manitou Springs</strong>
                                                        <p>
                                                          Each detail of the home is curated to provide the most luxurious experience possible! Located on a tranquil court in South Mission Beach, this home can accommodate each generation of the family with plenty of indoor/outdoor living.
                                                        </p>
                                                        <button class="btn btn-primary">
                                                          View
                                                        </button>
                                                      </div>

                                                    </li>
                                                    <li >
                                                      <img src="{{url()}}/images/properties/1.jpg" />
                                                      <div class="content_caption_p">
                                                        <strong>Hollyhock: a Hobbit House in Downtown Manitou Springs</strong>
                                                        <p>
                                                          Each detail of the home is curated to provide the most luxurious experience possible! Located on a tranquil court in South Mission Beach, this home can accommodate each generation of the family with plenty of indoor/outdoor living.
                                                        </p>
                                                        <button class="btn btn-primary">
                                                          View
                                                        </button>
                                                      </div>
                                                    </li>
                                                    <li >
                                                      <img src="{{url()}}/images/properties/1.jpg" />
                                                      <div class="content_caption_p">
                                                        <strong>Hollyhock: a Hobbit House in Downtown Manitou Springs</strong>
                                                        <p>
                                                          Each detail of the home is curated to provide the most luxurious experience possible! Located on a tranquil court in South Mission Beach, this home can accommodate each generation of the family with plenty of indoor/outdoor living.
                                                        </p>
                                                        <button class="btn btn-primary">
                                                          View
                                                        </button>
                                                      </div>
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                              <div class="col-md-6">
                                                <div class="title_section">
                                                  <h2>
                                                    <strong>
                                                      Insta Pinboard
                                                    </strong>
                                                  </h2>
                                                </div>
                                                <div>
                                                  <!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="https://lightwidget.com/widgets/f6ba192abfdc524c85ecc06ec523bd15.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0px; overflow: hidden; height: 505px;"></iframe>
                                                </div>
                                              </div>
                                            </div>
                                          </div>


                                        </div>
                                      </div>

                                    </main>
                                    @stop
