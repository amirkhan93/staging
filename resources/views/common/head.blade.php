<!DOCTYPE html>

<html  dir="{{ (((Session::get('language')) ? Session::get('language') : $default_language[0]->value) == 'ar') ? 'rtl' : '' }}" lang="{{ (Session::get('language')) ? Session::get('language') : $default_language[0]->value }}"  xmlns:fb="http://ogp.me/ns/fb#"><!--<![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" >
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <meta name = "viewport" content = "user-scalable=no, width=device-width">

  <link rel="dns-prefetch" href="https://maps.googleapis.com/">
  <link rel="dns-prefetch" href="https://maps.gstatic.com/">
  <link rel="dns-prefetch" href="https://mts0.googleapis.com/">
  <link rel="dns-prefetch" href="https://mts1.googleapis.com/">

  <link rel="shortcut icon" href="{{ $favicon }}">

  <!--[if IE]><![endif]-->
  <meta charset="utf-8">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
  <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link
  href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css"
  rel="stylesheet"
  />


    <!--[if IE 8]>
      {!! Html::style('css/common_ie8.css?v='.$version) !!}
    <![endif]-->
    <!--[if !(IE 8)]><!-->
    {!! Html::style('css/common.css?v='.$version) !!}
    {!! Html::style('css/styles.css?v='.$version) !!}
    {!! Html::style('css/nouislider.min.css?v='.$version) !!}
    {!! Html::style('css/home.css?v='.$version) !!}
    {!! Html::style('css/themes.css?v='.$version) !!}
    {!! Html::style('pcss?css=css/dynamic.css') !!}
    {!! Html::style('css/jquery.selectBoxIt.css?v='.$version) !!}
    {!! Html::style('css/daterangepicker.css?v='.$version) !!}
    {!! Html::style('css/header_two.css?v='.$version) !!}
    <!--<![endif]-->

    <!--[if lt IE 9]>
      {!! Html::style('css/airglyphs-ie8.css?v='.$version) !!}
    <![endif]-->

    @if (isset($exception))
    @if ($exception->getStatusCode()  == '404')
    {!! Html::style('css/error_pages_pretzel.css?v='.$version) !!}
    @endif
    @endif

    @if (!isset($exception))

    @if (Route::current()->uri() == 'signup_action')
    {!! Html::style('css/signinup.css?v='.$version) !!}

    @endif

    @if(@$default_home == 'two' )
    {!! Html::style('css/home_two.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == '/')
    {!! Html::style('css/main.css?v='.$version) !!}
    @if(@$default_home == 'two')
    {!! Html::style('css/common_two.css?v='.$version) !!}
    {!! Html::style('css/header_two.css?v='.$version) !!}
    {!! Html::style('css/daterangepicker.css?v='.$version) !!}
    {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
    @endif

    @endif


    @if (Route::current()->uri() == 'dashboard')

    {!! Html::style('css/host_dashboard.css?v='.$version) !!}
    {!! Html::style('css/dashboard.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'trips/current')

    @endif
    @if (Route::current()->uri() == 'trips/previous')

    @endif
    @if (Route::current()->uri() == 'users/transaction_history')

    @endif
    @if (Route::current()->uri() == 'users/security')

    @endif
    @if (Route::current()->uri() == 'rooms/new')

    {!! Html::style('css/new.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'inbox')

    {!! Html::style('css/threads.css?v='.$version) !!}
    @endif
    @if (Route::current()->uri() == 'home_two')
    {!! Html::style('css/common_two.css?v='.$version) !!}
    {!! Html::style('css/home_two.css?v='.$version) !!}

    {!! Html::style('css/daterangepicker.css?v='.$version) !!}

    {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'reservation/change')

    {!! Html::style('css/alterations.css?v='.$version) !!}
    {!! Html::style('css/policy_timeline_v2.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'alterations/{code}')

    {!! Html::style('css/alterations.css?v='.$version) !!}
    @endif


    @if (Route::current()->uri() == 'z/q/{id}')

    {!! Html::style('css/messaging.css?v='.$version) !!}
    {!! Html::style('css/tooltip.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'messaging/qt_with/{id}')
    {!! Html::style('css/messaging.css?v='.$version) !!}

    {!! Html::style('css/tooltip.css?v='.$version) !!}
    {!! Html::style('css/responsive_calendar.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'manage-listing/{id}/{page}')

    {!! Html::style('css/manage_listing.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'wishlists/picks' || Route::current()->uri() == 'wishlists/my' || Route::current()->uri() == 'wishlists/popular' || Route::current()->uri() == 'wishlists/{id}' || Route::current()->uri() == 'users/{id}/wishlists')
    {!! Html::style('css/wishlists.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'rooms/{id}')
    {!! Html::style('css/rooms_detail.css?v='.$version) !!}
    {!! Html::style('css/slider/nivo-lightbox.css?v='.$version) !!}
    {!! Html::style('css/slider/default.css?v='.$version) !!}
    {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
    {!! Html::style('css/p3.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'rooms')
    {!! Html::style('css/index.css?v='.$version) !!}
    {!! Html::style('css/unlist_modal.css?v='.$version) !!}
    {!! Html::style('css/dashboard.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'reservation/itinerary')

    @endif

    @if (Route::current()->uri() == 'reservation/receipt')
    {!! Html::style('css/receipt.css?v='.$version) !!}

    {!! Html::style('css/receipt-print.css?v='.$version,['media'=>'print']) !!}
    @endif

    @if (Route::current()->uri() == 's' || Route::current()->uri() == 'wishlists/popular')
    {!! Html::style('css/map_search.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'payments/book/{id?}' || Route::current()->uri() == 'api_payments/book/{id?}')
    {!! Html::style('css/payments.css?v='.$version) !!}

    {!! Html::style('css/p4.css?v='.$version) !!}
    {!! Html::style('css/StyleSheet.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'reservation/requested')
    {!! Html::style('css/page5.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'users/edit')
    {!! Html::style('css/address_widget.css?v='.$version) !!}

    {!! Html::style('css/phonenumbers.css?v='.$version) !!}
    {!! Html::style('css/edit_profile.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'users/show/{id}')
    {!! Html::style('css/profile.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'users/payout_preferences/{id}')
    {!! Html::style('css/payout_preferences.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'home/cancellation_policies')
    {!! Html::style('css/policy_timeline_v2.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'reviews/edit/{id}')
    {!! Html::style('css/reviews.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'invite' || Route::current()->uri() == 'c/{username}')
    {!! Html::style('css/referrals.css?v='.$version) !!}

    @endif

    @if (Route::current()->uri() == 'help' || Route::current()->uri() == 'help/topic/{id}/{category}' || Route::current()->uri() == 'help/article/{id}/{question}')
    {!! Html::style('css/help.css?v='.$version) !!}
    {!! Html::style('css/jquery-ui.css?v='.$version) !!}

    @endif

    @endif

    <style type="text/css">
    .ui-selecting { background: #FECA40; }
    .ui-selected { background: #F39814; color: white; }
  </style>

  <title>{{ $title or Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'title') }} {{ $additional_title or '' }}</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


  <meta name="description" content="{{ Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'description') }}">
  <meta name="keywords" content="{{ Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'keywords') }}">


  <meta name="twitter:widgets:csp" content="on">


  @if (!isset($exception))
  @if (Route::current()->uri() == 'rooms/{id}')
  <meta property="og:image" content="{{ url('images/'.$result->photo_name) }}">
  <meta itemprop="image" src="{{ url('images/'.$result->photo_name) }}">
  <link rel="image_src" href="#" src="{{ url('images/'.$result->photo_name) }}">
  @endif

  @if (Route::current()->uri() == 'wishlists/{id}')
  <meta property="og:image" content="{{ url('images/'.@$row->rooms->photo_name) }}">
  <meta itemprop="image" src="{{ url('images/'.@$row->rooms->photo_name) }}">
  <link rel="image_src" href="#" src="{{ url('images/'.@$row->rooms->photo_name) }}">
  @endif

  @endif
  <link rel="search" type="application/opensearchdescription+xml" href="#" title="">

  <meta name="mobile-web-app-capable" content="yes">

  <meta name="theme-color" content="#f5f5f5">
</head>
<body class="{{ (!isset($exception)) ? (Route::current()->uri() == '/' ? 'home_view v2 simple-header p1' : '') : '' }}" ng-app="App">
