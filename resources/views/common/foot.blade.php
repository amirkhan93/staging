
 <div id="gmap-preload" class="hide"></div>
   
   <div class="ipad-interstitial-wrapper"><span data-reactid=".1"></span></div>

    <div id="fb-root"></div>
   <!-- remove for console error -   &sensor=false -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $map_key }}&libraries=places&language={{ (Session::get('language')) ? Session::get('language') : $default_language[0]->value }}"></script>

    {!! Html::script('js/jquery-1.11.3.js') !!}
    {!! Html::script('js/jquery-ui.js') !!}
    @if(Session::get('language') != 'en')
      {!! Html::script('js/i18n/datepicker-'.Session::get('language').'.js') !!}
    @endif
    {!! Html::script('js/bootstrap.min.js') !!}

    {!! Html::script('js/angular.js') !!}
    {!! Html::script('js/angular-sanitize.js') !!}
    
    {!! Html::script('js/responsiveslides.min.js') !!}
    {!! Html::script('js/jquery.sticky-sidebar-scroll.min.js') !!}
       {!! Html::script('js/jquery.selectBoxIt.js') !!}

       {!! Html::script('js/moment.js') !!}
    {!! Html::script('js/daterangepicker.js') !!}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>


            <script type="text/javascript">
      $(document).ready(function() {
            $('.login_popup_head, .bck_btn').click(function(e){
              e.preventDefault();
                 $("body").addClass("pos-fix");
                  $(".sidebar").addClass("overflow-control");
                $(".login_popup").show();
                $(".signup_popup").hide();
                $(".signup_popup2").hide(); 
                $(".forgot-passward").hide(); 
            });
             $('.login-close, .rm_lg').click(function(event){ 
                    $("body").removeClass("pos-fix");
                    $(".sidebar").removeClass("overflow-control");
                $(".login_popup, .forgot-passward, .signup_popup, .signup_popup2").hide(); 
            });
            $('.top-home').click(function(event){
    event.stopPropagation();
});
         });
    </script>

          <script type="text/javascript">
      $(document).ready(function() {
            $('.forgot-password-popup').click(function(e){
              e.preventDefault();
                 $("body").addClass("pos-fix");
                $(".login_popup").hide();
                $(".forgot-passward").show();
            });
             $('.login-close').click(function(event){
                    $("body").removeClass("pos-fix");
                $(".forgot-passward").hide(); 
            });
            $('.top-home').click(function(event){
    event.stopPropagation();
});
         });
    </script>
       <script type="text/javascript">
      $(document).ready(function() {
            $('.signup_popup_head').click(function(e){
               e.preventDefault();
                $("body").addClass("pos-fix");
                 $(".sidebar").addClass("overflow-control");
                $(".signup_popup").show();
                 $(".login_popup").hide();  
            });
             $('.login-close').click(function(){
                 $("body").removeClass("pos-fix");
                 $(".sidebar").removeClass("overflow-control");
                $(".signup_popup").hide(); 
            });
             $('.top-home').click(function(event){
    event.stopPropagation();
});
         });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
            $('.signup_popup_head2').click(function(e){
               e.preventDefault();
                   $("body").addClass("pos-fix");
                $(".signup_popup2").show(); 
                 $(".signup_popup").hide(); 
            });
             $('.login-close').click(function(){
                 $("body").removeClass("pos-fix");
                $(".signup_popup2").hide(); 
            });
             $('.top-home').click(function(event){
    event.stopPropagation();
});
              $(function() {

              var selectBox = $("select.footer-select").selectBoxIt();

            });
         });
    </script>
       <script type="text/javascript">
      $(document).ready(function() {
            $('ul.menu-group li a').click(function()
            {
                $('.nav--sm').css('visibility','hidden');
            });
            $('.burger--sm').click(function()
{
    $('.header--sm .nav--sm').css('visibility','visible');
    $('.makent-header .header--sm .nav-content--sm').css('left','0', 'important');
});

$('.nav-mask--sm').click(function()
{
    $('.header--sm .nav--sm').css('visibility','hidden');
    $('.makent-header .header--sm .nav-content--sm').css('left','-285px');
});

// if(document.getElementById('header-search-form'))
//     {
//         header_autocomplete= new google.maps.places.Autocomplete(document.getElementById('header-search-form'));
//         google.maps.event.addListener(header_autocomplete, 'place_changed', function() {
//             $('#header-search-settings').addClass('shown');
//             // $("#header-search-checkin").datepicker("show");
//         });
//     }
    // if(document.getElementById('search-modal--sm'))
    // {
    //     sm_autocomplete= new google.maps.places.Autocomplete(document.getElementById('header-location--sm'));
    //     google.maps.event.addListener(sm_autocomplete, 'place_changed', function() {
    //         $("#modal_checkin").datepicker("show");
    //     });
    // }
//     $("#modal_checkin").datepicker({
//     dateFormat: "dd-mm-yy",
//     minDate: 0,
//     beforeShow: function(input, inst) {
//         setTimeout(function() {
//                 inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
//             }, 100);
//     },
//     onSelect: function (date) 
//     {
//         var modal_checkout = $('#modal_checkin').datepicker('getDate');
//         modal_checkout.setDate(modal_checkout.getDate() + 1);
//         $('#modal_checkout').datepicker('setDate', modal_checkout);
//         $('#modal_checkout').datepicker('option', 'minDate', modal_checkout);
//         setTimeout(function(){
//             $("#modal_checkout").datepicker("show");
//         },20);
//     }
// });

// $('#modal_checkout').datepicker({
//     dateFormat: "dd-mm-yy",
//     minDate: 1,
//     onClose: function () 
//     {
//         var modal_checkin = $('#checkin').datepicker('getDate');
//         var modal_checkout = $('#modal_checkout').datepicker('getDate');
//         if (modal_checkout <= modal_checkin) 
//         {
//             var minDate = $('#modal_checkout').datepicker('option', 'minDate');
//             $('#modal_checkout').datepicker('setDate', minDate);
//         }
//     }
// });
// function trigger_checkin()
// {
//     $("#checkin").datepicker("show");
// }

// $("#checkin").datepicker({
//     dateFormat: "dd-mm-yy",
//     minDate: 0,
//     beforeShow: function(input, inst) {
//         setTimeout(function() {
//                 inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
//             }, 100);
//     },
//     onSelect: function (date) 
//     {
//         var checkout = $('#checkin').datepicker('getDate');
//         checkout.setDate(checkout.getDate() + 1);
//         $('#checkout').datepicker('setDate', checkout);
//         $('#checkout').datepicker('option', 'minDate', checkout);
//         setTimeout(function(){
//             $("#checkout").datepicker("show");
//         },20);
//     }
// });

// $('#checkout').datepicker({ 
//     dateFormat: "dd-mm-yy",
//     minDate: 1,
//     onClose: function () 
//     {
//         var checkin = $('#checkin').datepicker('getDate');
//         var checkout = $('#checkout').datepicker('getDate');
//         if (checkout <= checkin) 
//         {
//             var minDate = $('#checkout').datepicker('option', 'minDate');
//             $('#checkout').datepicker('setDate', minDate);
//         }
//         if($('#checkin').val()=='')
//         {
//          var checkin = $('#checkout').datepicker('getDate');
//         checkin.setDate(checkin.getDate() -1 );
//         $('#checkin').datepicker('setDate',  new Date());
//         $('#checkout').datepicker('option', 'minDate', checkout);
//         setTimeout(function(){
//             $("#checkin").datepicker("show");
//         },20);
//         }
//     }
// });
 $('.nav-mask--sm').click(function()
{
    $('.header--sm .nav--sm').css('visibility','hidden');
    $('.makent-header .header--sm .nav-content--sm').css('left','-285px');
});

// $('.search-modal-trigger, #sm-search-field').click(function()
// {
//     $('#search-modal--sm').removeClass('hide');
//     $('#search-modal--sm').attr('aria-hidden','false');
// });

// $('.search-modal-container .modal-close').click(function()
// {
//     $('#search-modal--sm').addClass('hide');
//     $('#search-modal--sm').attr('aria-hidden','true');
// });
$('#search-form--sm-btn').click(function(event)
{
    var location  = $("#header-search-form-mob").val();
    if(location == '') {
        $('.searchbar__location-error').removeClass('hide');
        return false;
    }
    else
        $('.searchbar__location-error').addClass('hide');

    var sm_checkin = $('#modal_checkin').val();
    var sm_checkout = $('#modal_checkout').val();
    var sm_guests = $('#modal_guests').val();
    var sm_room_type = '';

    $('[id^="room-type-"]').each(function()
    {
        if($(this).is(':checked'))
            sm_room_type += $(this).val()+',';
    });
    sm_room_type = sm_room_type.slice(0,-1);
    if(location){ var locations = location.replace(" ", "+"); }
    window.location.href = APP_URL+'/s?location='+locations+'&checkin='+sm_checkin+'&checkout='+sm_checkout+'&guests='+sm_guests+'&room_type='+sm_room_type;
    event.preventDefault();
});
$(document).on('change','#user_profile_pic', function() {
    $('#ajax_upload_form').submit();
});
          });
    </script>
    <script> 
    var app = angular.module('App', ['ngSanitize']);
    var APP_URL = {!! json_encode(url('/')) !!};
    var LANGUAGE_CODE = "{!! (Session::get('language')) ? Session::get('language') : $default_language[0]->value !!}";
    var USER_ID = {!! @Auth::user()->user()->id !!}
    $.datepicker.setDefaults($.datepicker.regional[ "{{ (Session::get('language')) ? Session::get('language') : $default_language[0]->value }}" ])
    </script>

    {!! $head_code !!}

    {!! Html::script('js/common.js?v='.$version) !!}
     {!! Html::script('js/nouislider.min.js?v='.$version) !!}
       {!! Html::script('js/jquery.textfill.min.js?v='.$version) !!}    
{!! Html::script('js/jquery.bxslider.js') !!}  
    @if (!isset($exception))
    @if (Route::current()->uri() == '/')
        {!! Html::script('js/jquery.bxslider.min.js?v='.$version) !!}
        @if(@$default_home == 'two')
        {!! Html::script('js/home_two.js') !!}
        @endif
      @endif       
      
      @if (Route::current()->uri() == 'rooms/new')
        {!! Html::script('js/rooms_new.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif

      @if (Route::current()->uri() == 'manage-listing/{id}/{page}')
        {!! Html::script('js/manage_listing.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif

      @if (Route::current()->uri() == 's')
        {!! Html::script('js/search.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
		{!! Html::script('js/infobubble.js') !!}
      @endif
 @if (Route::current()->uri() == 'home_two')
     {!! Html::script('js/jquery.bxslider.min.js') !!}  
    {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif
       @if (Route::current()->uri() == 'trips/current')   
    {!! Html::script('js/home_two.js?v='.$version) !!} 
    @endif
         @if (Route::current()->uri() == 'trips/previous')   
    {!! Html::script('js/home_two.js?v='.$version) !!} 
    @endif
          @if (Route::current()->uri() == 'users/transaction_history')   
    {!! Html::script('js/home_two.js?v='.$version) !!} 
    @endif
         @if (Route::current()->uri() == 'users/security')   
    {!! Html::script('js/home_two.js?v='.$version) !!}
    @endif
      @if (Route::current()->uri() == 'rooms/{id}')
        {!! Html::script('js/rooms.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
        {!! Html::script('js/jquery.bxslider.min.js') !!}
      @endif

      @if (Route::current()->uri() == 'reservation/change')
      {!! Html::script('js/home_two.js?v='.$version) !!}
        {!! Html::script('js/reservation.js?v='.$version) !!}
      @endif

      @if (Route::current()->uri() == 'wishlists/popular' || Route::current()->uri() == 'wishlists/my' || Route::current()->uri() == 'wishlists/{id}' || Route::current()->uri() == 'users/{id}/wishlists')
        {!! Html::script('js/wishlists.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif

      @if (Route::current()->uri() == 'inbox' || Route::current()->uri() == 'z/q/{id}' || Route::current()->uri() == 'messaging/qt_with/{id}')
        {!! Html::script('js/inbox.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif
    @if (Route::current()->uri() == 'dashboard')   
          {!! Html::script('js/home_two.js?v='.$version) !!}
    @endif
      @if (Route::current()->uri() == 'reservation/{id}')
        {!! Html::script('js/reservation.js?v='.$version) !!}
        {!! Html::script('js/home_two.js?v='.$version) !!}
      @endif

    @endif

    @stack('scripts')
 @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
<script>
$(function() {
    $('.login_popup').show();
    $('.signup_popup').hide();
    $('.signup_popup2').hide();
    $('.forgot-passward').hide();
});
</script>
@endif
@if(!empty(Session::get('error_code')) && Session::get('error_code') == 1)
<script>
$(function() {  
    $('.login_popup').hide();
    $('.signup_popup2').show();
    $('.signup_popup').hide();
    $('.forgot-passward').hide();
  });
</script>
@endif

 @if(!empty(Session::get('error_code')) && Session::get('error_code') == 4)
<script>
$(function() {
     $('.login_popup').hide();
    $('.signup_popup').hide();
    $('.signup_popup2').hide();
    $('.forgot-passward').show();
});
</script>
@endif
<!-- ver. 87c23752f8dfbd60bf83837d2c8b2dcd0ec660a9 -->
<div class="tooltip tooltip-bottom-middle" role="tooltip" aria-hidden="true">  
	<p class="panel-body">To sign up, you must be 18 or older. Other people won’t see your birthday.</p>
	</div></body></html>