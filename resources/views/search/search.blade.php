@extends('template_two')
@section('main')
<div class="flash-container"></div>
<main id="site-content" role="main" ng-controller="SearchController" ng-init="initialSearch('{{ url() }}')">
  <input type="hidden" name="base__url" ng-model="base__url" />
  <input type="hidden" id="location" value="{{ $location }}">
  <input type="hidden" id="lat" value="{{ $lat }}">
  <input type="hidden" id="long" value="{{ $long }}">
  <!-- Language Translate for inside Search maps -->
  <input type="hidden" id="current_language" value= "{{ trans('messages.search.search_name') }} ">
  <input type="hidden" id="redo_search_value" value= "{{ trans('messages.search.redo_search_name') }} ">
  <!-- Pagination next prev used-->
  <input type="hidden" id="pagin_next" value= "{{ trans('messages.pagination.pagi_next') }} ">
  <input type="hidden" id="pagin_prev" value= "{{ trans('messages.pagination.pagi_prev') }} ">


  <input type="hidden" id="viewport" value='{!! json_encode($viewport) !!}' ng-model="viewport">
  <div class="map hide-sm-view">

    <div id="map_canvas" role="presentation" class="map-canvas" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(164, 221, 245);visibility:hidden">
    </div>

  </div>
  <div class="top_search_area">
    <div class="page-container-responsive new-page-container mini-rel-top row-space-top-1 main_inner">
      <div class="home_search_box">
        <div class="page-container-responsive new-page-container mini-rel-top form-col-12">
          <div class="">
            <div id="discovery-container"  >
              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 hide-sm form-col-12">
                <div class="container_e4p5a8"><!-- react-empty: 18439 -->
                  <form action="{{ url('s') }}" class="simple-search" method="get" id="searchbar-form" name="simple-search">
                    <div class="container_1tvwao0">
                      <div class="container_mv0xzc" style="width: 100%;">
                        <div class="label_1om3jpt">{{ trans('messages.header.where') }}</div>
                        <div class="largeGeocomplete_1g20x4k">
                          <div class="container_gor68n">
                            <div>
                              <div class="container_e296pg">
                                <div class="container_36rlri-o_O-block_r99te6">
                                  <label class="label_hidden_1m8bb6v" >{{ trans('messages.header.where') }}</label>
                                  <div class="container_ssgg6h-o_O-container_noMargins_18e9acw-o_O-borderless_mflkgb-o_O-block_r99te6">
                                    <div class="inputContainer_178faes">
                                      <input autocomplete="off" class="input_70aky9-o_O-input_book_f17nnd-o_O-input_ellipsis_1bgueul-o_O-input_defaultPlaceholder_jsyynz"
                                      id="header-search-input" name="location" placeholder="{{ trans('messages.header.anywhere') }}" value="" type="text">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="search-box">
                            <strong>Destinations </strong>
                            <ul>
                              <li data-value="Dubai, Dubai Emirate, United Arab Emirates">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> Dubai, Dubai Emirate, United Arab Emirates
                              </li>
                              <li data-value="Dubai, Dubai Emirate, United Arab Emirates">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> Dubai, Dubai Emirate, United Arab Emirates
                              </li>
                              <li data-value="Dubai, Dubai Emirate, United Arab Emirates">
                                <i class="fa fa-map-marker" aria-hidden="true"></i> Dubai, Dubai Emirate, United Arab Emirates
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="focusUnderline_7131v4">

                        </div>
                      </div>
                      <div class="container_mv0xzc-o_O-borderLeft_1ujj4hk-o_O-borderRight_1x9yfnn" style="width: 100%;">
                        <div class="label_1om3jpt">{{ trans('messages.header.when') }}</div>
                        <div class="webcot-lg-datepicker webcot-lg-datepicker--jumbo">
                          <div class="dateRangePicker_e296pg-o_O-hidden_ajz5vs">
                            <div class="DateRangePickerDiv">
                              <div>
                                <div class="DateRangePickerInput">
                                  <div class="DateInput">
                                    <input aria-label="Check In" class="DateInput__input needsclick" id="checkin" name="checkin" value="" placeholder="{{ trans('messages.header.checkin') }}" autocomplete="off" aria-describedby="DateInput__screen-reader-message-startDate" type="text">
                                    <div class="DateInput__display-text">{{ trans('messages.header.checkin') }}</div>
                                  </div>
                                  <div class="DateRangePickerInput__arrow" aria-hidden="true" role="presentation">-
                                  </div>
                                  <div class="DateInput">
                                    <input aria-label="Check Out" class="DateInput__input needsclick" id="checkout" name="checkout" value="" placeholder="{{ trans('messages.header.checkout') }}" autocomplete="off" aria-describedby="DateInput__screen-reader-message-endDate" type="text"><div class="DateInput__display-text">{{ trans('messages.header.checkout') }}</div>
                                  </div>

                                </div></div></div></div>
                                <div>
                                  <button type="button" tabindex="-1" class="button_1b5aaxl-o_O-button_large_c3pob4">
                                    <span class="icon_12hl23n"></span>
                                    <span class="copy_14aozyc-o_O-copy_fakePlaceholder_10k87om">{{ trans('messages.header.anytime') }}</span>
                                  </button>
                                </div>
                              </div>
                              <div class="focusUnderline_7131v4">

                              </div>
                            </div>
                            <div class="container_mv0xzc" style="width: 100%;">
                              <div class="label_1om3jpt col-md-6 padding_left"> {{ trans('messages.header.guest') }} </div>
                              <div>


                                <select id="guests" name="guests" class="col-md-6 ">
                                  @for($i=1;$i<=16;$i++)
                                  <option value="{{ $i }}"> {{ ($i == '16') ? $i.'+ '.trans_choice('messages.home.guest',$i) : $i.' '.trans_choice('messages.home.guest',$i) }} </option>
                                  @endfor
                                </select>
                                <div class="container_mv0xzc" style="width: 0%;"><!-- react-text: 18478 --><!-- /react-text --><button type="submit" class="searchButton_n8fchz">
                                  <span>{{ trans('messages.home.search') }}</span>
                                </button>
                                <div class="focusUnderline_7131v4"></div>
                              </div>
                            </div>




                          </button>
                        </div>
                        <div class="focusUnderline_7131v4">

                        </div>
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>

            <!-- mobile view header -->

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 show-sm hide-md">
              <div class="searchBarWrapper_1aq8p3r">
                <div class="container_puzkdo">
                  <div>
                    <div data-id="SearchBarSmall" class="container_1tvwao0">
                      <div class="container_mv0xzc" style="width: 100%;">

                        <button type="button" class="button_1b5aaxl button-sm-search">
                          <span class="icon_12hl23n">
                            <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 18px; width: 18px;"><path fill-rule="nonzero"></path></svg>
                          </span>
                          <span class="copy_14aozyc">{{ trans('messages.header.anywhere') }} · {{ trans('messages.header.anytime') }} · 1 {{ trans('messages.header.guest') }}</span></button>
                          <div class="focusUnderline_7131v4"></div></div></div><!-- react-empty: 29505 -->
                        </div>
                      </div></div>


                      <div class="panel-drop-down hide-drop-down" style="z-index: 2000;">
                        <div class="panelContent_1jzf86v">
                          <div class="container_gvf938-o_O-container_dropdown_bed46g">
                            <div class="left_egy8rd">
                              <button aria-haspopup="false" aria-expanded="false" class="container_1rp5252" type="button" style="padding: 20px; margin: -20px;">
                                <svg viewBox="0 0 18 18" role="img" aria-label="Close" focusable="false" style="display: block; fill: rgb(72, 72, 72); height: 16px; width: 16px;"><path fill-rule="evenodd"></path></svg>
                              </button>
                            </div>
                            <div class="right_8ydhe">
                              <div class="text_5mbkop-o_O-size_small_1gg2mc">
                                <button aria-disabled="false" class="component_9w5i1l-o_O-component_button_r8o91c" type="button"><span>Clear all</span>
                                </button>
                              </div>
                            </div>
                          </div>

                          <div class="body_1sn4o6s-o_O-body_dropdown_7xdft6 arrow-button">
                            <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                              <span class="icon_12hl23n">
                                <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><g fill-rule="evenodd"><path></path></g></svg>
                              </span>
                              <span class="copy_14aozyc">
                                <span>{{ trans('messages.header.anywhere') }}</span></span>
                              </button>
                              <div style="margin-top: 16px; margin-bottom: 16px;">
                                <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                                  <span class="icon_12hl23n">
                                    <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><path ></path></svg>
                                  </span>
                                  <span class="copy_14aozyc">{{ trans('messages.header.anytime') }}</span>
                                </button>
                              </div>
                              <button type="button" class="button_1b5aaxl-o_O-button_border_hu7ym7-o_O-button_large_c3pob4">
                                <span class="icon_12hl23n">
                                  <svg viewBox="0 0 24 24" role="presentation" aria-hidden="true" focusable="false" style="display: block; fill: currentcolor; height: 1em; width: 1em;"><path></path></svg>
                                </span>
                                <span class="copy_14aozyc"><span><span>1 {{ trans('messages.header.guest') }}</span></span></span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="page-container-responsive new-page-container mini-rel-top row-space-top-1 main_inner">
                <div class="top_detail_area">
                  <div class="total_count_properties">
                    <h4>1580  Properties match your search</h4>
                  </div>
                  <div class="view_types">
                    <ul>
                      <li>
                        <a href="javascript:void(0)"><i class="mdi mdi-view-list"></i> List</a>
                      </li>
                      <li>
                        <a href="javascript:void(0)"><i class="mdi mdi-view-grid"></i> Grid</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="filter_main">
                      <div class="price_filter">
                        <div class="filters-section intro-filter panel-body panel-light">
                          <h4>{{ trans('messages.search.price_range') }}</h4>
                          <div class="row">

                            <div class="col-lg-12 col-md-12 slider_main_range">
                              <div class="example">
                                <div id="slider" class="price-range-slider p2-slider-new"></div>
                                <div class="row" >
                                  <div class="col-6 lang-chang-label">
                                    <span class="price-min"> <span>{{ $currency_symbol }}</span> <span class="price" id="min_text">{{ $min_price }}</span> <span></span></span><input type="hidden" id="min_value" value="{{ $min_price }}">
                                  </div>
                                  <div class="col-6 text-right"><span class="price-min">
                                    <span>{{ $currency_symbol }}</span><span class="price" id="max_text">{{ $max_price }}+</span><span></span></span><input type="hidden" id="max_value" value="{{ $max_price }}">
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="rooms_beds">
                        <h4>Room & Beds</h5>
                          <div class="bedrooms_main b1">
                            <h5>Bedrooms</h5>
                            <div class="number">
                              <button class="minus">-</button>
                              <input type="text" value="0" max-length="10"/>
                              <button class="plus">+</button>
                            </div>
                          </div>
                          <div class="bedrooms_main b2">
                            <h5>Beds</h5>
                            <div class="number">
                              <button class="minus">-</button>
                              <input type="text" value="0" max-length="10"/>
                              <button class="plus">+</button>
                            </div>
                          </div>
                          <div class="bedrooms_main b3">
                            <h5>Bathrooms</h5>
                            <div class="number">
                              <button class="minus">-</button>
                              <input type="text" value="0" max-length="10"/>
                              <button class="plus">+</button>
                            </div>
                          </div>
                        </div>
                        <div class="filters-section room-type-group intro-filter panel-body panel-light">
                          <h4 style="position:relative">{{ trans('messages.lys.room_type') }} <span class="icon icon-question" id="room-type-tooltip"></span>
                            <div class="tooltip-room tooltip-left-middle" role="tooltip" data-trigger="#room-type-tooltip" data-sticky="true" aria-hidden="true" style="left: 120px; top: 80.925px;">
                              <dl class="panel-body">
                                @foreach($room_types as $row)
                                <dt>{{ $row->name }}</dt>
                                <dd>{{ $row->description }}</dd>
                                @endforeach

                              </dl>
                            </div>
                          </h4>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="row row-condensed" id="room-options">
                                @foreach($room_type as $row=>$value)
                                <div class="col-middle-alt col-sm-6">
                                  <label class="checkbox panel panel-dark">
                                    <input class="pull-right room-type" type="checkbox" ng-model="room_type_{{ $row }}" value="{{ $row }}" ng-change="search_result();" ng-init="room_type_{{$row}} = {{ (in_array($row, $room_type_selected)) ? $row : 'null' }}" ng-true-value="{{$row}}" ng-false-value="null" >
                                    @if($row == 1)
                                    <i class="icon icon-entire-place h5 needsclick"></i>
                                    @endif
                                    @if($row == 2)
                                    <i class="icon icon-private-room h5 needsclick"></i>
                                    @endif
                                    @if($row == 3)
                                    <i class="icon icon-shared-room h5 needsclick"></i>
                                    @endif
                                    <!--default icons displaying for newly added room type. -->
                                    @if($row >= 4)
                                    <i class="icon icon-home h5 needsclick"></i>
                                    @endif
                                    <span class="room-typs">{{ $value }}</span>
                                  </label>
                                </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>


                        <div class="checkbox-group hosting_amenities filters-section panel-body panel-light toggle-group" data-name="hosting_amenities">
                          <h4>{{ trans('messages.lys.amenities') }}</h4>
                          <div class="row">

                            <div class="col-lg-12 col-md-12 ">
                              <div class="row row-condensed filters-columns">
                                {{--*/ $row_inc = 1 /*--}}
                                @foreach($amenities as $row_amenities)
                                @if($row_inc <= 3)

                                <div class="col-md-6">
                                  <label class="media checkbox text-truncate panel panel-dark" title="{{ $row_amenities->name }}">
                                    <input type="checkbox" name="more_filter[]" value="{{ $row_amenities->id }}" class="pull-left amenities" id="map-search-amenities-{{ $row_amenities->id }}" ng-checked="{{ (in_array($row_amenities->id, $amenities_selected)) ? 'true' : 'false' }}">
                                    {{ $row_amenities->name }}
                                  </label>
                                </div>
                                @endif
                                {{--*/ $row_inc++ /*--}}
                                @endforeach

                              </div>

                              <div class="filters-more collapse">
                                <hr>
                                <div class="row row-condensed filters-columns">
                                  {{--*/ $amen_inc = 1 /*--}}

                                  @foreach($amenities as $row_amenities)
                                  @if($amen_inc > 3)

                                  <div class="col-md-6">
                                    <label class="media checkbox text-truncate panel panel-dark" title="{{ $row_amenities->name }}">
                                      <input type="checkbox" name="more_filter[]" value="{{ $row_amenities->id }}" class="pull-left amenities" id="map-search-amenities-{{ $row_amenities->id }}" ng-checked="{{ (in_array($row_amenities->id, $amenities_selected)) ? 'true' : 'false' }}">
                                      {{ $row_amenities->name }}
                                    </label>
                                  </div>
                                  @endif
                                  {{--*/ $amen_inc++ /*--}}
                                  @endforeach

                                </div>
                              </div>
                            </div>

                            <!-- <div class="col-md-1">
                            <label class="show-more">
                            <span>
                            <i class="icon icon-chevron-down hide-sm"></i>
                            <strong class="text-muted show-sm">+ {{ trans('messages.profile.more') }}</strong>
                          </span>
                          <span class="hide"><i class="icon icon-chevron-up"></i></span>
                        </label>
                      </div> -->
                    </div>
                  </div>


                  <div class="checkbox-group property_type_id filters-section panel-body panel-light toggle-group" data-name="property_type_id">
                    <h4>{{ trans('messages.lys.property_type') }}</h4>
                    <div class="row">
                      <div class="col-lg-12 col-md-12 right-pull">
                        <div class="row row-condensed filters-columns">
                          {{--*/ $pro_inc = 1 /*--}}
                          @foreach($property_type as $row_property_type =>$value_property_type)
                          @if($pro_inc <= 3)

                          <div class="col-md-6">
                            <label class="media checkbox text-truncate panel panel-dark" title="{{ $value_property_type }}">
                              <input type="checkbox" ng-model="property_type_{{ $row_property_type }}" value="{{ $row_property_type }}" class="pull-left property_type" id="map-search-property_type-{{ $row_property_type }}" ng-checked="{{ (in_array($row_property_type, $property_type_selected)) ? 'true' : 'false' }}">
                              {{ $value_property_type}}
                            </label>
                          </div>
                          @endif
                          {{--*/ $pro_inc++ /*--}}
                          @endforeach

                        </div>

                        <div class="filters-more collapse">
                          <hr>
                          <div class="row row-condensed filters-columns">
                            {{--*/ $property_inc = 1 /*--}}
                            @foreach($property_type as $row_property_type =>$value_property_type)
                            @if($property_inc > 3)

                            <div class="col-md-6">
                              <label class="media checkbox text-truncate panel panel-dark" title="{{ $value_property_type }}">
                                <input type="checkbox" ng-model="property_type_{{ $row_property_type }}" value="{{ $row_property_type }}" class="pull-left property_type" id="map-search-property_type-{{ $row_property_type }}" ng-checked="{{ (in_array($row_property_type, $property_type_selected)) ? 'true' : 'false' }}">
                                {{ $value_property_type}}
                              </label>
                            </div>
                            @endif
                            {{--*/ $property_inc++ /*--}}

                            @endforeach
                          </div>
                        </div>
                      </div>

                      <!-- <div class="col-md-1">
                      <label class="show-more">
                      <span>
                      <i class="icon icon-chevron-down hide-sm"></i>
                      <strong class="text-muted show-sm">+ {{ trans('messages.profile.more') }}</strong>
                    </span>
                    <span class="hide"><i class="icon icon-chevron-up"></i></span>
                  </label>
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="property_list outer-listings-container" ng-cloak>
            <div class="ui items">
              <div class="item" ng-repeat="rooms in room_result.data" ng-cloak>
                <div class="image">
                  <a href="{{ url('rooms') }}/@{{ rooms.id }}?checkin=@{{checkin}}&checkout=@{{checkout}}&guests=@{{ guests }}" target="listing_@{{ rooms.id }}" class="media-photo media-cover">
                    <img ng-src="{{ url() }}/images/@{{ rooms.photo_name }}" />
                  </a>
                </div>
                <div class="content">
                  <div class="row">
                    <div class="col-md-9">
                      <a href="{{ url('rooms') }}/@{{ rooms.id }}?checkin=@{{checkin}}&checkout=@{{checkout}}&guests=@{{ guests }}" target="listing_@{{ rooms.id }}" class="header"> @{{ rooms.name }}</a>
                      <div class="meta">
                        <strong>@{{ rooms.rooms_address.address_line_1 }}, @{{ rooms.rooms_address.address_line_2 }}, @{{ rooms.rooms_address.city }}, @{{ rooms.rooms_address.state }}</strong>
                      </div>
                      <div class="description">
                        <p><span>@{{ rooms.bedrooms }} BR</span> | <span>@{{ rooms.bathrooms }} BA</span> | <span>@{{ rooms.accommodates }} Sleeps</span></p>
                        <strong>@{{ rooms.property_type_name }}</strong>
                      </div>  
                    </div>
                    <div class="col-md-3 has_b_left">
                      <div class="right_options">
                        <p>
                          <h4><small>Per Night</small>  @{{ rooms.rooms_price.night }}</h4>
                        </p>
                        <a href="{{ url('rooms') }}/@{{ rooms.id }}?checkin=@{{checkin}}&checkout=@{{checkout}}&guests=@{{ guests }}" target="listing_@{{ rooms.id }}" class="btn btn-primary">Book Now</a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
<script type="text/javascript">
var min_slider_price = {!! $default_min_price !!};
var max_slider_price = {!! $default_max_price !!};
var min_slider_price_value = {!! $min_price !!};
var max_slider_price_value = {!! $max_price !!};
$(document).ready(function() {

  $("#wish_list_text").keyup(function(){
    $('#wish_list_btn').prop('disabled', true);
    var v_value =  $(this).val();
    var len =v_value.trim().length;
    // alert(len);
    if (len == 0)
    {
      $('#wish_list_btn').prop('disabled', true);
    }
    else{
      $('#wish_list_btn').prop('disabled', false);
    }
  });
});
</script>

@stop
