<?php

/**
 * Host Banners Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Host Banners
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HostBanners extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'host_banners';

    public $timestamps = false;

    public $appends = ['image_url', 'link_url'];

    public function getImageUrlAttribute()
    {
    	return url().'/images/host_banners/'.$this->attributes['image'];
    }

    public function getLinkUrlAttribute()
    {
        return url().$this->attributes['link'];
    }
    
}
