<?php

/**
* Rooms Price Model
*
* @package     Makent
* @subpackage  Model
* @category    Rooms Price
* @author      Trioangle Product Team
* @version     1.5.1.1.1
* @link        http://trioangle.com
*/

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use DB;
use Illuminate\Support\Facades\Route;
use JWTAuth;

class SeasonalPrice extends Model
{
  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = 'seasonal_pricing';
  public $timestamps = false;
  protected $appends = ['taxes', 'additional_charges', 'code'];

  // Join with currency table
  public function currency()
  {
    return $this->belongsTo('App\Models\Currency','currency_code','code');
  }

  public function getTaxesAttribute()
  {
    return DB::table('taxes')->where('room_id', $this->attributes['room_id'])->get();
  }

  public function getAdditionalChargesAttribute()
  {
    return DB::table('additional_charges')->where('room_id', $this->attributes['room_id'])->get();
  }

  // Calculation for current currency conversion of given price field
  // public function currency_calc($field)
  // {  //get currenct url
  //   $route=@Route::getCurrentRoute();
  //
  //   if($route)
  //   {
  //     $api_url = @$route->getPath();
  //   }
  //   else
  //   {
  //     $api_url = '';
  //   }
  //       $url_array=explode('/',$api_url);
  //         //Api currency conversion
  //       if(@$url_array['0']=='api')
  //       {
  //         $user_details = JWTAuth::parseToken()->authenticate();
  //
  //         $rate = Currency::whereCode($this->attributes['currency_code'])->first()->rate;
  //
  //         $usd_amount = $this->attributes[$field] / $rate;
  //
  //         $api_currency = $user_details->currency_code;
  //
  //         $default_currency = Currency::where('default_currency',1)->first()->code;
  //
  //         $session_rate = Currency::whereCode($user_details->currency_code!=null?$user_details->currency_code :$default_currency)->first()->rate;
  //
  //            return round($usd_amount * $session_rate);
  //
  //      }
  //      else
  //      {
  //     $rate = Currency::whereCode($this->attributes['currency_code'])->first()->rate;
  //
  //     $usd_amount = $this->attributes[$field] / $rate;
  //
  //     $default_currency = Currency::where('default_currency',1)->first()->code;
  //
  //     $session_rate = Currency::whereCode((Session::get('currency')) ? Session::get('currency') : $default_currency)->first()->rate;
  //
  //     return round($usd_amount * $session_rate);
  //     }
  // }

  // Get default currency code if session is not set
  public function getCodeAttribute()
  {
    //get currenct url
    $route=@Route::getCurrentRoute();
    if($route)
    {
      $api_url = @$route->getPath();
    }
    else
    {
      $api_url = '';
    }
    $url_array=explode('/',$api_url);
    //Check current user login is web or mobile
    if(@$url_array['0']=='api')
    {

      if(JWTAuth::parseToken()->authenticate()->currency_code)
      //set user currency code
      return JWTAuth::parseToken()->authenticate()->currency_code;

      else
      //set default currency  code . for user currency code not given.
      return DB::table('currency')->where('default_currency', 1)->first()->code;
    }
    else
    {
      if(Session::get('currency'))
      return Session::get('currency');
      else
      return DB::table('currency')->where('default_currency', 1)->first()->code;
    }

  }

}
