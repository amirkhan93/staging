<?php

/**
 * Rooms Description Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Rooms Description
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomsDescription extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms_description';

    public $timestamps = false;

    protected $primaryKey = 'room_id';
    protected $fillable = ['space', 'access','interaction','notes','house_rules'];
    public function setSpaceAttribute($input){
         $this->attributes['space'] = strip_tags($input);
    }
    public function setAccessAttribute($input){
         $this->attributes['access'] = strip_tags($input);
    }
    public function setInteractionAttribute($input){
         $this->attributes['interaction'] = strip_tags($input);
    }
    public function setNotesAttribute($input){
         $this->attributes['notes'] = strip_tags($input);
    }
    public function setHouseRulesAttribute($input){
         $this->attributes['house_rules'] = strip_tags($input);
    }
}
