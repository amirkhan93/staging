<?php

/**
 * Join Us Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Join Us
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JoinUs extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'join_us';

    public $timestamps = false;
}
