<?php

/**
 * Rooms Photos Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Rooms Photos
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomsPhotos extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms_photos';

    public $timestamps = false;

    protected $appends = ['steps_count'];

    // Get steps_count using sum of rooms_steps_status
    public function getStepsCountAttribute()
    {
        $result = RoomsStepsStatus::find($this->attributes['room_id']);

        return 6 - ($result->basics + $result->description + $result->location + $result->photos + $result->pricing + $result->calendar);
    }

    // Get Name Attribute
    // public function getNameAttribute(){
    //     $photo_details = pathinfo($this->attributes['name']); 
    //     $name = @$photo_details['filename'].'_450x250.'.@$photo_details['extension'];
    //     return $name;
    // }
    // Get Slider Image Name Attribute
    // public function getSliderImageNameAttribute(){
    //     $photo_details = pathinfo($this->attributes['name']); 
    //     $name = @$photo_details['filename'].'_1440x960.'.@$photo_details['extension'];
    //     return $name;
    // }
    // Get Banner Image Name Attribute
    // public function getBannerImageNameAttribute(){
    //     $photo_details = pathinfo($this->attributes['name']); 
    //     $name = @$photo_details['filename'].'_1349x402.'.@$photo_details['extension'];
    //     return $name;
    // }
}
