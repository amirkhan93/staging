<?php

/**
 * Metas Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Metas
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metas extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'metas';

    public $timestamps = false;
}
