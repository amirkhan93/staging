<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /*
        **
            @Relationship define between City/State
            Relationship type: One to Many
        **
    */

    public function state()
    {
        return $this->hasMany('App\State');
    }
}
