<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /*
        **
            @Relationship define between State/Country
            Relationship type: Belongs to state
        **
    */

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /*
        **
            @Relationship define between state/city
            Relationship type: One to Many
        **
    */

    public function city()
    {
        return $this->hasMany('App\City');
    }
}
