<?php

/**
* Payment Helper
*
* @package     Makent
* @subpackage  Helper
* @category    Helper
* @author      Trioangle Product Team
* @version     1.5.1.1.1
* @link        http://trioangle.com
*/

namespace App\Http\Helper;

use App\Models\RoomsPrice;
use App\Models\Calendar;
use App\Models\Currency;
use App\Models\Rooms;
use App\Models\SpecialOffer;
use App\Models\Reservation;
use App\Models\Fees;
use App\Models\HostPenalty;
use App\Models\CouponCode;
use App\Models\Payouts;
use App\Models\Referrals;
use App\Models\AppliedTravelCredit;
use DateTime;
use Session;

class PaymentHelper
{
	/**
	* Common Function for Price Calculation
	*
	* @param int $room_id   Room Id
	* @param int $checkin   CheckIn Date
	* @param int $checkout   CheckOut Date
	* @param int $guest_count   Guest Count
	* @param int $special_offer_id   Special Offer Id (Optional)
	* @param int $change_reservation   Dummy string to identify Change reservation or not (Optional)
	* @return json   Calculation Result
	*/
	public function price_calculation($room_id, $checkin, $checkout, $guest_count, $special_offer_id = '', $change_reservation='', $reservation_id ='')
	{
		$from                       = new DateTime($checkin);
		$to                         = new DateTime($checkout);
		$total_nights               = $to->diff($from)->format("%a");
		$date1                      = date('Y-m-d', strtotime($checkin));
		$enddate                    = date('Y-m-d', strtotime($checkout));
		$date2                      = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($enddate ) ) ));
		$days                       = $this->get_days($date1, $date2 );
		// $calendar_result            = Calendar::where(['room_id' => $room_id])->whereIn('date', $days)->get();
		$calendar_result			= (object) array();
		$rooms_details              = Rooms::find($room_id);
		$price_details              = $rooms_details->rooms_price;
		$allowed_guests 			= $price_details->guests;
		$price                      = $price_details->daily_price;
		$week_end_price             = $price_details->weekend_price;
		$result['additional_guest'] = 0;
		$result['additional_charges'] = [];
		$result['taxes'] 							= [];

		if($reservation_id != ''){
			$price_details              = Reservation::find($reservation_id);
			$price                      = $price_details->daily_price;
		}
		$reservation_change         = Reservation::where(['code' => $change_reservation])->get();
		foreach ($reservation_change as $key )
		{
			$reservation_checkin  = $key->checkin;
			$reservation_checkout = $key->checkout;
		}
		if($change_reservation)
		{
			$reservation_checkin  = date('Y-m-d', strtotime($reservation_checkin));
			$reserve_date         = date('Y-m-d', strtotime($reservation_checkout));
			$reservation_checkout = date('Y-m-d',(strtotime ( '-1 day' , strtotime ($reserve_date ) ) ));
			$reseved_days         = $this->get_days($reservation_checkin, $reservation_checkout );
		}

		foreach ($calendar_result as $key )
		{
			$status = $key->status;
			if($status == "Not available")
			{
				if($change_reservation)
				{
					$result['status'] = "Not available";
					$datecheck = in_array($key->date, $reseved_days);
					if(!$datecheck)
					{
						return json_encode($result);
					}
				}
				else
				{
					$result['status'] = "Not available";
					return json_encode($result);
				}
			}
			else if($status == "Available")
			{
				$special_nights_price += $key->session_currency_price;
				$special_nights_count += 1;
				$special_night_status  = "Available";
				$i++;
			}
			if(date('N', strtotime($key->date)) == 5)
			{
				$friday_count = 1 + $friday;
				$friday++;
			}
			if(date('N', strtotime($key->date)) == 6)
			{
				$satday_count = 1 + $satday;
				$satday++;
			}
		}

		$month_days                = floor($total_nights / 28);
		$month_days_remaining      = number_format($total_nights % 28);
		$week_days                 = floor($month_days_remaining / 7);
		$week_days_remaining       = number_format($month_days_remaining % 7);

		if($price_details->weekend_price !=0)
		{
			$weekend_days = json_decode($price_details->weekend_days);
			$weekend_count = 0;
			for ($i = strtotime($checkin); $i < strtotime($checkout); $i = strtotime('+1 day', $i))
			{
				if(in_array(date('N', $i), $weekend_days)) {
					$weekend_count++;
				}
			}

			$week_end_days            = $weekend_count;
			$week_end_remainig_nights = $total_nights - $week_end_days;
			$week_end_price           = round($price_details->weekend_price * $week_end_days);
		}

		if($week_end_days)
		{
			$night_diff = $week_end_remainig_nights ;
		}
		else
		{
			$night_diff = $total_nights;
		}

		$week_price  = round($night_diff  * $price);
		$total_week_price =  round($week_price  + $week_end_price);
		$result['total_night_price'] = $total_week_price;


		if($reservation_id){
			$result['service_fee'] = $price_details->service;
			$result['host_fee'] = $price_details->host_fee;
		}

		if($guest_count > $allowed_guests)
		{
			$additional_guest_count     = $guest_count - $allowed_guests;
			$result['additional_guest'] = $additional_guest_count * $price_details->extra_guest;
			// dd($price_details->extra_guest);
		}
		$result['rooms_price']  = round($result['total_night_price'] / $total_nights, 2);
		$result['total_nights'] = $total_nights;
		$total_additional_charges = 0;
		foreach ($price_details->additional_charges as $charges) {
			array_push($result['additional_charges'], (object) array(
				'name' => $charges->name,
				'amount' => $charges->amount
			));
			$total_additional_charges += $charges->amount;
		}
		$result['total_additional_charges'] = $total_additional_charges;
		$result['subtotal']     = $result['total_night_price'] + $result['additional_guest'] + $result['total_additional_charges'];
		$total_tax = 0;
		foreach ($price_details->taxes as $tax) {
			$taxable_amount = $result['subtotal'] - $result['total_additional_charges'];
			if($tax->calculation_by == 'gross') {
				$taxable_amount = $result['subtotal'];
			}
			$tax_amount = $taxable_amount * ($tax->roi / 100);
			array_push($result['taxes'], (object) array(
				'name' => $tax->name,
				'amount' => $tax_amount
			));
			$total_tax += $tax_amount;
		}
		$result['total_tax'] = $total_tax;
		$result['total']        = $result['subtotal'] + $result['total_tax'];
		if(SpecialOffer::find($special_offer_id) && SpecialOffer::find($special_offer_id)->type == "special_offer")
		{
			$result['special_offer'] = "yes";
		} else {
			$result['special_offer'] = '';
		}
		if(SpecialOffer::find($special_offer_id))
		{
			if(SpecialOffer::find($special_offer_id)->type == "special_offer"){
				$result['total_night_price'] = SpecialOffer::find($special_offer_id)->price;
				$result['total'] = SpecialOffer::find($special_offer_id)->price;
				$result['subtotal'] = SpecialOffer::find($special_offer_id)->price;
				$result['rooms_prices'] = SpecialOffer::find($special_offer_id)->price/$total_nights;
			}
		}
		$result['payout']       = $result['total'];
		if($reservation_id == ''){
			$result['currency']         = $price_details->code;
		}else{
			$result['currency']         = $price_details->currency->code;
		}
		return json_encode($result);
	}
	/**
	* Get days between two dates
	*
	* @param date $sStartDate  Start Date
	* @param date $sEndDate    End Date
	* @return array $days      Between two dates
	*/
	public function get_days($sStartDate, $sEndDate)
	{
		$aDays[]      = $sStartDate;
		$sCurrentDate = $sStartDate;

		while($sCurrentDate < $sEndDate)
		{
			$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
			$aDays[]      = $sCurrentDate;
		}

		return $aDays;
	}
	/**
	* Currency Convert
	*
	* @param int $from   Currency Code From
	* @param int $to     Currency Code To
	* @param int $price  Price Amount
	* @return int Converted amount
	*/
	public function currency_convert($from = '', $to = '', $price)
	{
		if($from == '')
		{
			if(Session::get('currency'))
			$from = Session::get('currency');
			else
			$from = Currency::where('default_currency', 1)->first()->code;
		}
		if($to == '')
		{
			if(Session::get('currency'))
			$to = Session::get('currency');
			else
			$to = Currency::where('default_currency', 1)->first()->code;
		}
		$rate = Currency::whereCode($from)->first()->rate;
		$usd_amount = $price / $rate;
		$session_rate = Currency::whereCode($to)->first()->rate;
		return round($usd_amount * $session_rate);
	}
	/**
	* Currency Rate
	*
	* @param int $from   Currency Code From
	* @param int $to     Currency Code To
	* @return int Converted Currency Rate
	*/
	public function currency_rate($from, $to)
	{
		$from_rate = Currency::whereCode($from)->first()->rate;
		$to_rate = Currency::whereCode($to)->first()->rate;

		return round($from_rate / $to_rate);
	}
	/**
	* Date Convert
	*
	* @param date $date   Given Date
	* @return date Converted new date format
	*/
	public function date_convert($date)
	{
		return date('Y-m-d', strtotime($date));
	}
	/**
	* Penalty Amount Check
	*
	* @param total $amount   Given amount
	* @return check if any penalty for this host then renturn remainning amount
	*/

	public function check_host_penalty($penalty, $reservation_amount,$reservation_currency_code)
	{
		$penalty_id1 = '';
		$penalty_id2 = '';
		$penalty_amt1 = '';
		$penalty_amt2 = '';
		if($penalty->count() > 0 )
		{
			$host_amount = $reservation_amount;
			foreach ($penalty as $pen) {
				$host_amount = $this->currency_convert($reservation_currency_code,$pen->currency_code,$host_amount);
				$remaining_amount = $pen->remain_amount;
				if($host_amount > $remaining_amount)
				{
					$host_amount = $host_amount - $remaining_amount ;
					$penalty = HostPenalty::find($pen->id);
					$penalty->remain_amount    = 0;
					$penalty->status           = "Completed";
					$penalty->save();
					$penalty_id1 .= $pen->id.',';
					$penalty_amt1 .= $remaining_amount.',';
				}
				else
				{
					$amount_reamining = $remaining_amount - $host_amount;
					$penalty = HostPenalty::find($pen->id);
					$penalty->remain_amount  = $amount_reamining;
					$penalty->save();
					$penalty_id2 .= $pen->id.',';
					$penalty_amt2 .= $host_amount.',';
					$host_amount = 0;
				}
				$host_amount = $this->currency_convert($pen->currency_code,$reservation_currency_code,$host_amount);
			}

			$pty_id         = $penalty_id1.$penalty_id2;

			$penalty_id     = rtrim($pty_id, ',');

			$pty_amt        = $penalty_amt1.$penalty_amt2;

			$penalty_amount = rtrim($pty_amt, ',');

		}
		else
		{
			$host_amount = $reservation_amount;

			$penalty_id     = 0;

			$penalty_amount = 0;
		}

		$result['host_amount']     = $host_amount;
		$result['penalty_id']      = $penalty_id;
		$result['penalty_amount']  = $penalty_amount;

		return $result;

	}

	public function revert_travel_credit($reservation_id) {

		$applied_referrals = AppliedTravelCredit::whereReservationId($reservation_id)->get();

		foreach($applied_referrals as $row) {
			$referral = Referrals::find($row->referral_id);

			if($row->type == 'main')
			$referral->credited_amount = $referral->credited_amount + $this->currency_convert($row->currency_code, $referral->currency_code, $row->original_amount);
			else
			$referral->friend_credited_amount = $referral->friend_credited_amount + $this->currency_convert($row->currency_code, $referral->currency_code, $row->original_amount);

			$referral->save();

			$applied_referrals = AppliedTravelCredit::find($row->id)->delete();
		}

	}
}
