<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('re/{id}/{df}',  'EmailController@booking_response_remainder');
Route::get('/phpinfo', function(){
    phpinfo();
});
Route::get('/update-city-id', 'Api\ApiController@updateCityId');
Route::get('/update-statewise-id', 'Api\ApiController@updateStatewiseId');
Route::match(['get', 'post'], 'api_payments/book/{id?}', 'PaymentController@index');

Route::get('p', 'EmailController@booking');

Route::get('reservation_email/{reservation_id}', ['uses' => 'EmailController@reservation_expired_admin']);

Route::post('api_payments/pre_accept', 'PaymentController@pre_accept');

Route::post('api_payments/create_booking', 'PaymentController@create_booking');

Route::get('api_payments/success', 'PaymentController@success');

Route::get('api_payments/cancel', 'PaymentController@cancel');

Route::group(['middleware' => ['install','locale']], function () {
  Route::get('/', 'HomeController@index');
});

Route::get('phpinfo', 'HomeController@phpinfo');

Route::get('pcss', 'PcssController@index');

Route::get('googleAuthenticate', 'UserController@googleAuthenticate');
//linkedin login
Route::get('linkedin', function () {
  return Redirect::to('login');
});
Route::get('auth/linkedin', 'UserController@redirectToLinkedin');

Route::get('linkedinLoginVerification', 'UserController@linkedinLoginVerification');

Route::get('linkedinConnect', 'UserController@linkedinConnect');

// Before Login Routes

Route::group(['before' => 'no_auth', 'middleware' => 'locale'], function(){

  Route::get('login', 'HomeController@login');

  Route::get('auth/login', function()
  {
    return Redirect::to('login');
  });

  Route::get('signup_login', 'HomeController@signup_login');

  Route::post('create', 'UserController@create');

  Route::post('authenticate', 'UserController@authenticate');

  Route::get('facebookAuthenticate', 'UserController@facebookAuthenticate');

  Route::get('googleLogin', 'UserController@googleLogin');

  Route::match(array('GET', 'POST'), 'forgot_password', 'UserController@forgot_password');

  Route::get('users/set_password/{secret?}', 'UserController@set_password');

  Route::post('users/set_password', 'UserController@set_password');

  Route::get('c/{username}', 'ReferralsController@invite_referral');

  Route::get('user_disabled', 'UserController@user_disabled');

  Route::get('users/signup_email', 'UserController@signup_email');

  Route::post('users/finish_signup_email', 'UserController@finish_signup_email');

});

Route::group(['middleware' => 'locale'], function () {


  Route::get('contact', 'HomeController@contact');
  //Route::get('home_two', 'HomeController@home_two');
  Route::match(['get', 'post'], 'contact_create', 'HomeController@contact_create');

  Route::post('set_session', 'HomeController@set_session');

  Route::get('s', 'SearchController@index');

  Route::post('search/prop_search/', 'SearchController@prop_search');

  Route::match(['get', 'post'], 'searchResult', 'SearchController@searchResult');

  Route::match(['get', 'post'], 'initial-search', 'SearchController@initial_search');

  Route::post('rooms_photos', 'SearchController@rooms_photos');

  Route::get('s/{id}', 'SearchController@index')->where('id', '[0-9]+');

  Route::get('currency_cron', 'CronController@currency');

  Route::get('cron/ical_sync', 'CronController@ical_sync');

  Route::get('cron/expire', 'CronController@expire');

  Route::get('cron/travel_credit', 'CronController@travel_credit');

  Route::get('cron/review_remainder', 'CronController@review_remainder');
  Route::get('cron/host_remainder_pending_reservaions', 'CronController@host_remainder_pending_reservaions');

  Route::get('users/show/{id}', 'UserController@show')->where('id', '[0-9]+');

  Route::get('home/cancellation_policies', 'HomeController@cancellation_policies');

  Route::get('help', 'HomeController@help');

  Route::get('help/topic/{id}/{category}', 'HomeController@help');

  Route::get('help/article/{id}/{question}', 'HomeController@help');
  Route::get('ajax_help_search', 'HomeController@ajax_help_search');

  Route::get('wishlist_list', 'WishlistController@wishlist_list');
  Route::get('wishlists/{id}', 'WishlistController@wishlist_details')->where('id', '[0-9]+');
  Route::get('users/{id}/wishlists', 'WishlistController@my_wishlists');

  Route::get('invite', 'ReferralsController@invite');

  Route::get('wishlists/popular', 'WishlistController@popular');
  Route::get('wishlists/picks', 'WishlistController@picks');

  Route::get('calendar/ical/{id}', 'CalendarController@ical_export');

});

// After Login Routes

Route::group(['before' => 'guest', 'middleware' => 'locale'], function () {

  Route::get('dashboard', 'UserController@dashboard');

  Route::get('users/edit', 'UserController@edit');

  Route::match(['get', 'post'], 'users/get_users_phone_numbers', 'UserController@get_users_phone_numbers');

  Route::post('users/update_users_phone_number', 'UserController@update_users_phone_number');

  // Route::get('welcom_email/{id}/{type}', ['uses' => 'EmailController@wrote_review']);

  Route::post('users/remove_users_phone_number', 'UserController@remove_users_phone_number');

  Route::post('users/verify_users_phone_number', 'UserController@verify_users_phone_number');

  Route::get('users/edit/media', 'UserController@media');

  Route::get('users/edit_verification', 'UserController@verification');

  Route::get('facebookConnect', 'UserController@facebookConnect');

  Route::get('facebookDisconnect', 'UserController@facebookDisconnect');

  Route::get('googleLoginVerification', 'UserController@googleLoginVerification');

  Route::get('googleConnect/{id}', 'UserController@googleConnect')->where('id', '[0-9]+');

  Route::get('googleDisconnect', 'UserController@googleDisconnect');


  Route::get('linkedinDisconnect', 'UserController@linkedinDisconnect');

  Route::post('users/image_upload', 'UserController@image_upload');

  Route::get('users/reviews', 'UserController@reviews');

  Route::match(['get', 'post'], 'reviews/edit/{id}', 'UserController@reviews_edit')->where('id', '[0-9]+');

  Route::post('users/update/{id}', 'UserController@update')->where('id', '[0-9]+');

  Route::get('users/confirm_email/{code?}', 'UserController@confirm_email');

  Route::get('users/request_new_confirm_email', 'UserController@request_new_confirm_email');

  Route::get('users/security', 'UserController@security');

  Route::post('wishlist_create', 'WishlistController@create');
  Route::post('create_new_wishlist', 'WishlistController@create_new_wishlist');
  Route::post('edit_wishlist/{id}', 'WishlistController@edit_wishlist')->where('id', '[0-9]+');
  Route::get('delete_wishlist/{id}', 'WishlistController@delete_wishlist')->where('id', '[0-9]+');
  Route::post('remove_saved_wishlist/{id}', 'WishlistController@remove_saved_wishlist')->where('id', '[0-9]+');
  Route::post('add_note_wishlist/{id}', 'WishlistController@add_note_wishlist')->where('id', '[0-9]+');
  Route::post('save_wishlist', 'WishlistController@save_wishlist');
  Route::get('wishlists/my', 'WishlistController@my_wishlists');
  Route::post('share_email/{id}', 'WishlistController@share_email')->where('id', '[0-9]+');

  Route::match(['get', 'post'], 'users/payout_preferences/{id}', 'UserController@payout_preferences')->where('id', '[0-9]+');
  Route::get('users/payout_delete/{id}', 'UserController@payout_delete')->where('id', '[0-9]+');
  Route::get('users/payout_default/{id}', 'UserController@payout_default')->where('id', '[0-9]+');

  Route::get('users/transaction_history', 'UserController@transaction_history');
  Route::post('users/result_transaction_history', 'UserController@result_transaction_history');
  Route::get('transaction_history/csv/{id}', 'UserController@transaction_history_csv')->where('id', '[0-9]+');

  Route::post('change_password', 'UserController@change_password');

  Route::get('account', function() {
    return Redirect::to('users/payout_preferences/'.Auth::user()->user()->id);
  });

  Route::get('rooms', 'RoomsController@index');

  Route::get('rooms/new', 'RoomsController@new_room');

  Route::post('rooms/create', 'RoomsController@create');

  Route::get('rooms/create', function(){
    return redirect('rooms/new');
  });

  Route::group(['middleware' => 'manage_listing_auth'] , function(){

    Route::post('manage-listing/{id}/update_rooms', 'RoomsController@update_rooms')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/update_amenities', 'RoomsController@update_amenities')->where('id', '[0-9]+');

    Route::post('add_photos/{id}', 'RoomsController@add_photos')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/featured_image', 'RoomsController@featured_image')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/delete_photo', 'RoomsController@delete_photo')->where('id', '[0-9]+');

    Route::get('manage-listing/{id}/photos_list', 'RoomsController@photos_list')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/photo_highlights', 'RoomsController@photo_highlights')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/update_price', 'RoomsController@update_price')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/currency_check', 'RoomsController@currency_check')->where('id', '[0-9]+');

    Route::get('manage-listing/{id}/currency_check', 'RoomsController@currency_check')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/update_description', 'RoomsController@update_description')->where('id', '[0-9]+');

    Route::get('manage-listing/{id}/rooms_steps_status', 'RoomsController@rooms_steps_status');

    Route::get('manage-listing/{id}/rooms_data', 'RoomsController@rooms_data')->where('id', '[0-9]+');

    Route::post('manage-listing/{id}/calendar_edit', 'RoomsController@calendar_edit')->where('id', '[0-9]+');

  });

  Route::post('calendar/import/{id}', 'CalendarController@ical_import')->where('id', '[0-9]+');

  Route::get('calendar/sync/{id}', 'CalendarController@ical_sync')->where('id', '[0-9]+');
  Route::post('manage-listing/{id}/remove_video', 'RoomsController@remove_video')->where('id', '[0-9]+');
  
  Route::get('manage-listing/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::post('ajax-manage-listing/{id}/{page}', 'RoomsController@ajax_manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::post('ajax-header/{id}/{page}', 'RoomsController@ajax_header')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  //get method to redirect manage listing
  Route::get('ajax-manage-listing/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::get('ajax-header/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::get('enter_address/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::get('location_not_found/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::get('verify_location/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);

  Route::get('finish_address/{id}/{page}', 'RoomsController@manage_listing')
  ->where(['id' => '[0-9]+','page' => 'basics|description|location|amenities|photos|video|pricing|calendar|details|guidebook|terms|booking|priceandavail']);
  //end

  Route::post('enter_address/{id}/{page}', 'RoomsController@enter_address');

  Route::post('location_not_found/{id}/{page}', 'RoomsController@location_not_found');

  Route::post('verify_location/{id}/{page}', 'RoomsController@verify_location');

  Route::post('finish_address/{id}/{page}', 'RoomsController@finish_address');

  Route::get('inbox', 'InboxController@index');

  Route::match(['get', 'post'], 'payments/book/{id?}', 'PaymentController@index')->where('id', '[0-9]+');

  Route::post('payments/apply_coupon', 'PaymentController@apply_coupon');
  Route::post('payments/remove_coupon', 'PaymentController@remove_coupon');

  //Route::get('payments/create_booking', 'PaymentController@payment_303');
  Route::match(['get', 'post'], 'payments/create_booking', 'PaymentController@create_booking');
  Route::match(['get', 'post'], 'payments/stripe_booking', 'PaymentController@stripe_booking');

  Route::post('payments/pre_accept', 'PaymentController@pre_accept');

  Route::get('payments/success', 'PaymentController@success');
  Route::get('payments/cancel', 'PaymentController@cancel');

  Route::post('users/ask_question/{id}', 'RoomsController@contact_request')->where('id', '[0-9]+');

  // Message
  Route::post('inbox/archive', 'InboxController@archive');
  Route::post('inbox/star', 'InboxController@star');
  Route::post('inbox/message_by_type', 'InboxController@message_by_type');
  Route::post('inbox/all_message', 'InboxController@all_message');
  Route::get('z/q/{id}', 'InboxController@guest_conversation')->where('id', '[0-9]+');
  Route::get('messaging/qt_with/{id}', 'InboxController@host_conversation')->where('id', '[0-9]+');
  Route::post('messaging/qt_reply/{id}', 'InboxController@reply');
  Route::get('messaging/remove_special_offer/{id}', 'InboxController@remove_special_offer')->where('id', '[0-9]+');
  Route::post('inbox/calendar', 'InboxController@calendar');

  Route::post('inbox/message_count', 'InboxController@message_count');

  // Reservation
  Route::get('reservation/{id}', 'ReservationController@index')->where('id', '[0-9]+');
  Route::post('reservation/accept/{id}', 'ReservationController@accept')->where('id', '[0-9]+');
  Route::post('reservation/decline/{id}', 'ReservationController@decline')->where('id', '[0-9]+');
  Route::get('reservation/expire/{id}', 'ReservationController@expire')->where('id', '[0-9]+');
  Route::get('my_reservations', 'ReservationController@my_reservations');
  Route::get('reservation/itinerary', 'ReservationController@print_confirmation');
  Route::get('reservation/requested', 'ReservationController@requested');
  Route::post('reservation/itinerary_friends', 'ReservationController@itinerary_friends');

  // Change and cancel
  Route::match(['get', 'post'],'reservation/change', 'ReservationController@host_cancel_change');
  Route::match(['get', 'post'],'alterations/{code}', 'ReservationController@cancel_alterations');
  Route::match(['get', 'post'],'reservation/reservation_alterations', 'ReservationController@reservation_alterations');

  // Cancel Reservation
  Route::match(['get', 'post'],'trips/guest_cancel_pending_reservation', 'TripsController@guest_cancel_pending_reservation');
  Route::match(['get', 'post'],'trips/guest_cancel_reservation', 'TripsController@guest_cancel_reservation');
  Route::match(['get', 'post'],'reservation/host_cancel_reservation', 'ReservationController@host_cancel_reservation');
  Route::match(['get', 'post'], 'checking/{id}', 'TripsController@get_status')->where('id', '[0-9]+');

  Route::match(['get', 'post'], 'status_update', 'TripsController@status_update');

  Route::match(['get', 'post'],'reservation/cencel_request_send', 'ReservationController@cencel_request_send');
  // Trips
  Route::get('trips/current', 'TripsController@current');
  Route::get('trips/previous', 'TripsController@previous');
  Route::get('reservation/receipt', 'TripsController@receipt');

  Route::post('invite/share_email', 'ReferralsController@share_email');

});

Route::group(['middleware' => 'locale'], function () {

  // Rooms details
  Route::get('rooms/{id}', 'RoomsController@rooms_detail')->where('id', '[0-9]+');
  Route::get('rooms/{id}/slider', 'RoomsController@rooms_slider')->where('id', '[0-9]+');
  Route::post('rooms/rooms_calendar', 'RoomsController@rooms_calendar');
  Route::post('rooms/rooms_calendar_alter', 'RoomsController@rooms_calendar_alter');
  Route::post('rooms/price_calculation', 'RoomsController@price_calculation');
  Route::post('rooms/check_availability', 'RoomsController@check_availability');
  Route::post('rooms/current_date_check', 'RoomsController@current_date_check');
  Route::post('rooms/checkin_date_check', 'RoomsController@checkin_date_check');

});

Route::get('logout', function()
{
  Auth::user()->logout();
  return Redirect::to('login');
});

Route::filter('guest', function($request) {

  if (Auth::user()->guest()) {
    $request_url = Request::url();
    if(strpos($request_url, '/payments/book/')){
      $id = @end(explode('/', $request_url));
      $i = 0;
      $s_key = ''.$id.$i.'';
      if(Session::has('payment')) {
        $session_key = array_keys(Session::get('payment'));
        $count= count($session_key);
        $end = end($session_key);

        if(Session::has('payment.'.$s_key)){

          $last_char =  mb_substr($end, -1) + 1;
          $s_key = $id.$last_char;
        }
        else
        {
          if(is_array($session_key))
          {
            $last_char =  mb_substr($end, -1) + 1;
            $s_key = $id.$last_char;
          }
          else
          {
            $s_key = $s_key;
          }

        }

      }


      if($_POST){
        $payment = array(
          'payment_room_id' => $id,
          'payment_checkin' => $_POST['checkin'],
          'payment_checkout' => $_POST['checkout'],
          'payment_number_of_guests' => $_POST['number_of_guests'],
          'payment_booking_type' => $_POST['booking_type'],
          'payment_reservation_room_type' => $_POST['room_types'],
          'payment_cancellation' => $_POST['cancellation']
        );
        Session::put('payment.'.$s_key,$payment);
        Session::put('s_key',$s_key);
      }
      if($_GET){
        if($_GET['s_key']=='')
        {
          $payment = array(
            'payment_room_id' => $id,
            'payment_checkin' => $_GET['checkin'],
            'payment_checkout' => $_GET['checkout'],
            'payment_number_of_guests' => $_GET['number_of_guests'],
            'payment_booking_type' => $_GET['booking_type'],
            'payment_reservation_room_type' => $_GET['room_types'],
            'payment_cancellation' => $_GET['cancellation'],
          );

          Session::put('payment.'.$s_key,$payment);
          Session::put('s_key',$s_key);
        }
      }
    }


    //check this url is ajax
    $redirect_url='manage-listing/'.$request->id.'/basics';
    if (Request::ajax())
    {
      Session::put('ajax_redirect_url', $redirect_url);
      return response('Unauthorized', 300);
    }
    else
    {
      return Redirect::guest('login');
    }
  }
  if(Auth::user()->user()->status == 'Inactive')
  {
    $data['title'] = 'Disabled';
    return View::make('users.disabled');
  }
});


// Filter for check the user is logged in or not before goto login page
Route::filter('no_auth', function(){
  if(Auth::user()->check()){
    return Redirect::to('dashboard');
  }
});

// Admin Panel Routes
Route::group(['prefix' => 'admin', 'before' => 'admin_guest'], function() {

  Route::get('/', function()
  {
    return Redirect::to('admin/dashboard');
  });

  Route::get('dashboard', 'Admin\AdminController@index');

  Route::get('logout', 'Admin\AdminController@logout');

  Route::get('users', 'Admin\UsersController@index');

  Route::get('reservations', 'Admin\ReservationsController@index');
  Route::get('host_penalty', 'Admin\HostPenaltyController@index');

  Route::match(['GET', 'POST'], 'reports', 'Admin\ReportsController@index');

  Route::get('reports/export/{from}/{to}/{category}', 'Admin\ReportsController@export');

  Route::get('reservation/detail/{id}', 'Admin\ReservationsController@detail')->where('id', '[0-9]+');

  Route::get('reservation/conversation/{id}', 'Admin\ReservationsController@conversation')->where('id', '[0-9]+');

  Route::get('reservation/need_payout_info/{id}/{type}', 'Admin\ReservationsController@need_payout_info');

  Route::post('reservation/payout', 'Admin\ReservationsController@payout');

  Route::match(array('GET', 'POST'), 'add_user', 'Admin\UsersController@add');

  Route::match(array('GET', 'POST'), 'edit_user/{id}', 'Admin\UsersController@update')->where('id', '[0-9]+');

  Route::get('delete_user/{id}', 'Admin\UsersController@delete')->where('id', '[0-9]+');

  Route::get('rooms', 'Admin\RoomsController@index');

  Route::match(array('GET', 'POST'), 'add_room', 'Admin\RoomsController@add');
  Route::post('admin_update_rooms', 'Admin\RoomsController@update_video');

  Route::match(array('GET', 'POST'), 'edit_room/{id}', 'Admin\RoomsController@update')->where('id', '[0-9]+');

  Route::get('delete_room/{id}', 'Admin\RoomsController@delete')->where('id', '[0-9]+');

  Route::get('popular_room/{id}', 'Admin\RoomsController@popular')->where('id', '[0-9]+')->where('id', '[0-9]+');
  Route::get('recommended_room/{id}', 'Admin\RoomsController@recommended')->where('id', '[0-9]+')->where('id', '[0-9]+');

  Route::post('ajax_calendar/{id}', 'Admin\RoomsController@ajax_calendar')->where('id', '[0-9]+');

  Route::post('delete_photo', 'Admin\RoomsController@delete_photo');

  Route::post('admin_pricelist', 'Admin\RoomsController@update_price');

  Route::post('featured_image', 'Admin\RoomsController@featured_image');

  Route::post('photo_highlights', 'Admin\RoomsController@photo_highlights');

  Route::get('rooms/users_list', 'Admin\RoomsController@users_list');

  Route::get('delete_room/{id}', 'Admin\RoomsController@delete')->where('id', '[0-9]+');

  // Manage Admin Permission Routes

  Route::group(['before' => 'manage_admin'], function () {

    Route::get('admin_users', 'Admin\AdminusersController@index');

    Route::match(array('GET', 'POST'), 'add_admin_user', 'Admin\AdminusersController@add');

    Route::match(array('GET', 'POST'), 'edit_admin_user/{id}', 'Admin\AdminusersController@update')->where('id', '[0-9]+');

    Route::get('delete_admin_user/{id}', 'Admin\AdminusersController@delete')->where('id', '[0-9]+');

    Route::get('roles', 'Admin\RolesController@index');

    Route::match(array('GET', 'POST'), 'add_role', 'Admin\RolesController@add');

    Route::match(array('GET', 'POST'), 'edit_role/{id}', 'Admin\RolesController@update')->where('id', '[0-9]+');

    Route::get('delete_role/{id}', 'Admin\RolesController@delete')->where('id', '[0-9]+');

    Route::get('permissions', 'Admin\PermissionsController@index');

    Route::match(array('GET', 'POST'), 'add_permission', 'Admin\PermissionsController@add');

    Route::match(array('GET', 'POST'), 'edit_permission/{id}', 'Admin\PermissionsController@update')->where('id', '[0-9]+');

    Route::get('delete_permission/{id}', 'Admin\PermissionsController@delete')->where('id', '[0-9]+');

  });

  // Manage Amenities Routes

  Route::group(['before' => 'manage_amenities'], function () {

    Route::get('amenities', 'Admin\AmenitiesController@index');

    Route::match(array('GET', 'POST'), 'add_amenity', 'Admin\AmenitiesController@add');

    Route::match(array('GET', 'POST'), 'edit_amenity/{id}', 'Admin\AmenitiesController@update')->where('id', '[0-9]+');

    Route::get('delete_amenity/{id}', 'Admin\AmenitiesController@delete')->where('id', '[0-9]+');

    Route::get('amenities_type', 'Admin\AmenitiesTypeController@index');

    Route::match(array('GET', 'POST'), 'add_amenities_type', 'Admin\AmenitiesTypeController@add');

    Route::match(array('GET', 'POST'), 'edit_amenities_type/{id}', 'Admin\AmenitiesTypeController@update')->where('id', '[0-9]+');

    Route::get('delete_amenities_type/{id}', 'Admin\AmenitiesTypeController@delete')->where('id', '[0-9]+');

  });

  // Manage Property Type Routes

  Route::group(['before' => 'manage_property_type'], function () {

    Route::get('property_type', 'Admin\PropertyTypeController@index');

    Route::match(array('GET', 'POST'), 'add_property_type', 'Admin\PropertyTypeController@add');

    Route::match(array('GET', 'POST'), 'edit_property_type/{id}', 'Admin\PropertyTypeController@update')->where('id', '[0-9]+');

    Route::get('delete_property_type/{id}', 'Admin\PropertyTypeController@delete')->where('id', '[0-9]+');

  });

  // Manage Home Page Cities Routes

  Route::get('referrals', 'Admin\ReferralsController@index');

  Route::get('referral_details/{id}', 'Admin\ReferralsController@details')->where('id', '[0-9]+');


  Route::group(['before' => 'manage_home_cities'], function () {

    Route::get('home_cities', 'Admin\HomeCitiesController@index');

    Route::match(array('GET', 'POST'), 'add_home_city', 'Admin\HomeCitiesController@add');

    Route::match(array('GET', 'POST'), 'edit_home_city/{id}', 'Admin\HomeCitiesController@update')->where('id', '[0-9]+');

    Route::get('delete_home_city/{id}', 'Admin\HomeCitiesController@delete')->where('id', '[0-9]+');

  });

  // Manage Home Page Slider Routes

  Route::group(['before' => 'manage_home_page_sliders'], function () {

    Route::get('slider', 'Admin\SliderController@index');

    Route::match(array('GET', 'POST'), 'add_slider', 'Admin\SliderController@add');

    Route::match(array('GET', 'POST'), 'edit_slider/{id}', 'Admin\SliderController@update')->where('id', '[0-9]+');

    Route::get('delete_slider/{id}', 'Admin\SliderController@delete')->where('id', '[0-9]+');

  });
  // Manage Home Page Bottom Slider Routes

  Route::group(['before' => 'manage_home_page_bottom_sliders'], function () {

    Route::get('bottom_slider', 'Admin\BottomSliderController@index');

    Route::match(array('GET', 'POST'), 'add_bottom_slider', 'Admin\BottomSliderController@add');

    Route::match(array('GET', 'POST'), 'edit_bottom_slider/{id}', 'Admin\BottomSliderController@update')->where('id', '[0-9]+');

    Route::get('delete_bottom_slider/{id}', 'Admin\BottomSliderController@delete')->where('id', '[0-9]+');

  });

  // Manage Room Type Routes

  Route::group(['before' => 'manage_room_type'], function () {

    Route::get('room_type', 'Admin\RoomTypeController@index');

    Route::match(array('GET', 'POST'), 'add_room_type', 'Admin\RoomTypeController@add');

    Route::match(array('GET', 'POST'), 'edit_room_type/{id}', 'Admin\RoomTypeController@update')->where('id', '[0-9]+');

    Route::match(array('GET', 'POST'), 'status_check/{id}', 'Admin\RoomTypeController@chck_status')->where('id', '[0-9]+');
    Route::match(array('GET', 'POST'), 'bed_status_check/{id}', 'Admin\BedTypeController@chck_status')->where('id', '[0-9]+');

    Route::get('delete_room_type/{id}', 'Admin\RoomTypeController@delete')->where('id', '[0-9]+');

  });

  // Manage Our Community Banners Routes

  Route::group(['before' => 'manage_our_community_banners'], function () {

    Route::get('our_community_banners', 'Admin\OurCommunityBannersController@index');

    Route::match(array('GET', 'POST'), 'add_our_community_banners', 'Admin\OurCommunityBannersController@add');

    Route::match(array('GET', 'POST'), 'edit_our_community_banners/{id}', 'Admin\OurCommunityBannersController@update')->where('id', '[0-9]+');

    Route::get('delete_our_community_banners/{id}', 'Admin\OurCommunityBannersController@delete')->where('id', '[0-9]+');

  });

  // Manage Host Banners Routes

  Route::group(['before' => 'manage_host_banners'], function () {

    Route::get('host_banners', 'Admin\HostBannersController@index');

    Route::match(array('GET', 'POST'), 'add_host_banners', 'Admin\HostBannersController@add');

    Route::match(array('GET', 'POST'), 'edit_host_banners/{id}', 'Admin\HostBannersController@update')->where('id', '[0-9]+');

    Route::get('delete_host_banners/{id}', 'Admin\HostBannersController@delete')->where('id', '[0-9]+');

  });

  // Manage Help Routes

  Route::group(['before' => 'manage_help'], function () {

    Route::get('help_category', 'Admin\HelpCategoryController@index');

    Route::match(array('GET', 'POST'), 'add_help_category', 'Admin\HelpCategoryController@add');

    Route::match(array('GET', 'POST'), 'edit_help_category/{id}', 'Admin\HelpCategoryController@update')->where('id', '[0-9]+');

    Route::get('delete_help_category/{id}', 'Admin\HelpCategoryController@delete')->where('id', '[0-9]+');

    Route::get('help_subcategory', 'Admin\HelpSubCategoryController@index');

    Route::match(array('GET', 'POST'), 'add_help_subcategory', 'Admin\HelpSubCategoryController@add');

    Route::match(array('GET', 'POST'), 'edit_help_subcategory/{id}', 'Admin\HelpSubCategoryController@update')->where('id', '[0-9]+');

    Route::get('delete_help_subcategory/{id}', 'Admin\HelpSubCategoryController@delete')->where('id', '[0-9]+');

    Route::get('help', 'Admin\HelpController@index');

    Route::match(array('GET', 'POST'), 'add_help', 'Admin\HelpController@add');

    Route::match(array('GET', 'POST'), 'edit_help/{id}', 'Admin\HelpController@update')->where('id', '[0-9]+');

    Route::get('delete_help/{id}', 'Admin\HelpController@delete')->where('id', '[0-9]+');

    Route::post('ajax_help_subcategory/{id}', 'Admin\HelpController@ajax_help_subcategory')->where('id', '[0-9]+');

  });

  // Manage Bed Type Routes

  Route::group(['before' => 'manage_bed_type'], function () {

    Route::get('bed_type', 'Admin\BedTypeController@index');

    Route::match(array('GET', 'POST'), 'add_bed_type', 'Admin\BedTypeController@add');

    Route::match(array('GET', 'POST'), 'edit_bed_type/{id}', 'Admin\BedTypeController@update')->where('id', '[0-9]+');

    Route::get('delete_bed_type/{id}', 'Admin\BedTypeController@delete')->where('id', '[0-9]+');

  });

  // Manage Pages Routes

  Route::group(['before' => 'manage_pages'], function () {

    Route::get('pages', 'Admin\PagesController@index');

    Route::match(array('GET', 'POST'), 'add_page', 'Admin\PagesController@add');

    Route::match(array('GET', 'POST'), 'edit_page/{id}', 'Admin\PagesController@update')->where('id', '[0-9]+');

    Route::get('delete_page/{id}', 'Admin\PagesController@delete')->where('id', '[0-9]+');

  });

  // Manage Currency Routes

  Route::group(['before' => 'manage_currency'], function () {

    Route::get('currency', 'Admin\CurrencyController@index');

    Route::match(array('GET', 'POST'), 'add_currency', 'Admin\CurrencyController@add');

    Route::match(array('GET', 'POST'), 'edit_currency/{id}', 'Admin\CurrencyController@update')->where('id', '[0-9]+');

    Route::get('delete_currency/{id}', 'Admin\CurrencyController@delete')->where('id', '[0-9]+');

  });

  // Manage Coupon Code Routes

  Route::group(['before' => 'manage_coupon_code'], function () {

    Route::get('coupon_code', 'Admin\CouponCodeController@index');

    Route::match(array('GET', 'POST'), 'add_coupon_code', 'Admin\CouponCodeController@add');

    Route::match(array('GET', 'POST'), 'edit_coupon_code/{id}', 'Admin\CouponCodeController@update')->where('id', '[0-9]+');

    Route::get('delete_coupon_code/{id}', 'Admin\CouponCodeController@delete');

  });
  // Manage Language Routes

  Route::group(['before' => 'manage_language'], function () {

    Route::get('language', 'Admin\LanguageController@index');

    Route::match(array('GET', 'POST'), 'add_language', 'Admin\LanguageController@add');

    Route::match(array('GET', 'POST'), 'edit_language/{id}', 'Admin\LanguageController@update')->where('id', '[0-9]+');

    Route::get('delete_language/{id}', 'Admin\LanguageController@delete')->where('id', '[0-9]+');

  });

  // Manage Country Routes

  Route::group(['before' => 'manage_country'], function () {

    Route::get('country', 'Admin\CountryController@index');

    Route::match(array('GET', 'POST'), 'add_country', 'Admin\CountryController@add');

    Route::match(array('GET', 'POST'), 'edit_country/{id}', 'Admin\CountryController@update')->where('id', '[0-9]+');

    Route::get('delete_country/{id}', 'Admin\CountryController@delete')->where('id', '[0-9]+');

  });

  Route::match(array('GET', 'POST'), 'api_credentials', 'Admin\ApiCredentialsController@index');

  Route::match(array('GET', 'POST'), 'payment_gateway', 'Admin\PaymentGatewayController@index');

  Route::match(array('GET', 'POST'), 'email_settings', 'Admin\EmailController@index');

  Route::match(array('GET', 'POST'), 'send_email', 'Admin\EmailController@send_email');

  Route::match(array('GET', 'POST'), 'site_settings', 'Admin\SiteSettingsController@index');

  Route::match(array('GET', 'POST'), 'theme_settings', 'Admin\ThemeSettingsController@index');

  Route::match(array('GET', 'POST'), 'referral_settings', 'Admin\ReferralSettingsController@index');

  Route::match(array('GET', 'POST'), 'fees', 'Admin\FeesController@index');

  Route::match(array('GET', 'POST'), 'fees/host_penalty_fees', 'Admin\FeesController@host_penalty_fees');

  Route::match(array('GET', 'POST'), 'metas', 'Admin\MetasController@index');

  Route::match(array('GET', 'POST'), 'edit_meta/{id}', 'Admin\MetasController@update')->where('id', '[0-9]+');

  Route::match(array('GET', 'POST'), 'reviews', 'Admin\ReviewsController@index');

  Route::match(array('GET', 'POST'), 'wishlists', 'Admin\WishlistController@index');
  Route::match(array('GET', 'POST'), 'pick_wishlist/{id}', 'Admin\WishlistController@pick')->where('id', '[0-9]+');

  Route::match(array('GET', 'POST'), 'edit_review/{id}', 'Admin\ReviewsController@update')->where('id', '[0-9]+');

  Route::match(array('GET', 'POST'), 'join_us', 'Admin\JoinUsController@index');

  // Admin Panel User Route Permissions

  Entrust::routeNeedsPermission('admin/users', 'users');

  Entrust::routeNeedsPermission('admin/edit_user*', 'edit_user');

  Entrust::routeNeedsPermission('admin/add_user', 'add_user');

  Entrust::routeNeedsPermission('admin/delete_user*', 'delete_user');

  Entrust::routeNeedsPermission('admin/rooms', 'rooms');

  Entrust::routeNeedsPermission('admin/edit_room*', 'edit_room');

  Entrust::routeNeedsPermission('admin/add_room', 'add_room');

  Entrust::routeNeedsPermission('admin/delete_room*', 'delete_room');

  Entrust::routeNeedsPermission('admin/reservations', 'reservations');
  Entrust::routeNeedsPermission('admin/host_penalty', 'reservations');

  Entrust::routeNeedsPermission('admin/reports', 'reports');

  Entrust::routeNeedsPermission('admin/api_credentials', 'api_credentials');

  Entrust::routeNeedsPermission('admin/payment_gateway', 'payment_gateway');

  Entrust::routeNeedsPermission('admin/email_settings', 'email_settings');

  Entrust::routeNeedsPermission('admin/send_email', 'send_email');

  Entrust::routeNeedsPermission('admin/site_settings', 'site_settings');

  Entrust::routeNeedsPermission('admin/theme_settings', 'site_settings');

  Entrust::routeNeedsPermission('admin/fees', 'manage_fees');

  Entrust::routeNeedsPermission('admin/referral_settings', 'manage_referral_settings');

  Entrust::routeNeedsPermission('admin/metas', 'manage_metas');

  Entrust::routeNeedsPermission('admin/edit_meta*', 'manage_metas');

  Entrust::routeNeedsPermission('admin/join_us', 'join_us');

  Entrust::routeNeedsPermission('admin/reviews', 'manage_reviews');

  Entrust::routeNeedsPermission('admin/edit_review/{id}', 'manage_reviews');

  Entrust::routeNeedsPermission('admin/wishlists', 'manage_wishlists');

});

Route::group(['prefix' => 'admin', 'before' => 'admin_no_auth'], function () {

  Route::get('login', 'Admin\AdminController@login');

});

Route::post('admin/authenticate', 'Admin\AdminController@authenticate');

Route::get('admin/create', 'Admin\AdminController@create');

Route::filter('admin_guest', function() {
  if (Auth::admin()->guest()) {
    return Redirect::to('admin/login');
  }
});

// Filter for check the admin user is logged in or not before goto login page
Route::filter('admin_no_auth', function(){
  if(Auth::admin()->check()){
    return Redirect::to('admin/dashboard');
  }
});

Route::get('in_secure',function(){
  return view('errors.in_secure');
});

Route::filter('manage_admin', function(){
  if(!Entrust::can('manage_admin'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_amenities', function(){
  if(!Entrust::can('manage_amenities'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_property_type', function(){
  if(!Entrust::can('manage_property_type'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_home_page_sliders', function(){
  if(!Entrust::can('manage_home_page_sliders'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_home_page_bottom_sliders', function(){
  if(!Entrust::can('manage_home_page_bottom_sliders'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_room_type', function(){
  if(!Entrust::can('manage_room_type'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_help', function(){
  if(!Entrust::can('manage_help'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_bed_type', function(){
  if(!Entrust::can('manage_bed_type'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_currency', function(){
  if(!Entrust::can('manage_currency'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_coupon_code', function(){
  if(!Entrust::can('manage_coupon_code'))
  {
    return Redirect::to('admin/login');
  }
});


Route::filter('manage_language', function(){
  if(!Entrust::can('manage_language'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_country', function(){
  if(!Entrust::can('manage_country'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_our_community_banners', function(){
  if(!Entrust::can('manage_our_community_banners'))
  {
    return Redirect::to('admin/login');
  }
});

Route::filter('manage_host_banners', function(){
  if(!Entrust::can('manage_host_banners'))
  {
    return Redirect::to('admin/login');
  }
});
Route::filter('manage_home_page_bottom_sliders', function(){
  if(!Entrust::can('manage_home_page_bottom_sliders'))
  {
    return Redirect::to('admin/login');
  }
});
Route::filter('site_settings', function(){
  if(!Entrust::can('site_settings'))
  {
    return Redirect::to('admin/login');
  }
});
Route::filter('reservations', function(){
  if(!Entrust::can('reservations'))
  {
    return Redirect::to('admin/login');
  }
});

Route::get('{name}', 'HomeController@static_pages');

// API WebService

/* Route::group(['prefix' => 'api'], function () {

Route::post('register', 'Api\TokenAuthController@register');
Route::post('authenticate', 'Api\TokenAuthController@authenticate');
Route::post('token', 'Api\TokenAuthController@token');

Route::group(['middleware' => 'jwt.auth'], function () {
Route::post('authenticate/user', 'Api\TokenAuthController@getAuthenticatedUser');
Route::post('user_details', 'Api\UserController@user_details');
Route::post('home_cities', 'Api\UserController@home_cities');
});
});
*/
Route::group(['prefix' => 'api'], function () {

  Route::get('register', 'Api\TokenAuthController@register');

  Route::get('authenticate', 'Api\TokenAuthController@authenticate');

  Route::get('token', 'Api\TokenAuthController@token');

  Route::get('signup', 'Api\TokenAuthController@signup');

  Route::get('login', 'Api\TokenAuthController@login');

  Route::get('emailvalidation', 'Api\TokenAuthController@emailvalidation');

  Route::get('forgotpassword', 'Api\TokenAuthController@forgotpassword');


  Route::group(['middleware' => ['jwt.auth','disable_user']], function () {


    Route::get('authenticate/user', 'Api\TokenAuthController@getAuthenticatedUser');

    Route::get('user_details', 'Api\UserController@user_details');

    Route::get('home_cities', 'Api\UserController@home_cities');

    Route::get('payout_details', 'Api\UserController@payout_details');

    Route::get('payout_changes', 'Api\UserController@payout_changes');

    Route::get('explore', 'Api\SearchController@explore_details');

    Route::get('rooms', 'Api\RoomsController@rooms_detail');

    Route::get('review_detail', 'Api\RoomsController@review_detail');

    Route::get('maps', 'Api\RoomsController@maps');

    Route::get('calendar_availability', 'Api\RoomsController@calendar_availability');

    Route::get('country_list', 'Api\HomeController@country_list');

    Route::get('pre_payment', 'Api\PaymentController@pre_payment');

    Route::get('after_payment', 'Api\PaymentController@after_payment');

    Route::get('payment_methods', 'Api\PaymentController@payment_methods');

    Route::get('apply_coupon', 'Api\PaymentController@apply_coupon');

    Route::get('house_rules', 'Api\RoomsController@house_rules');

    Route::get('add_rooms_price', 'Api\RoomsController@add_rooms_price');

    Route::get('update_Long_term_prices', 'Api\RoomsController@update_Long_term_prices');

    Route::get('update_room_currency', 'Api\RoomsController@update_room_currency');

    Route::get('update_location', 'Api\RoomsController@update_location');

    Route::get('disable_listing ', 'Api\RoomsController@disable_listing');

    Route::get('calendar_availability_status', 'Api\RoomsController@calendar_availability_status');
    Route::get('logout', 'Api\UserController@logout');

    Route::get('currency_change', 'Api\PaymentController@currency_change');

    Route::get('currency_list', 'Api\HomeController@currency_list');

    Route::get('amenities_list', 'Api\HomeController@amenities_list');

    Route::get('update_house_rules', 'Api\HomeController@update_house_rules');

    Route::get('update_description', 'Api\HomeController@update_description');

    Route::get('update_title_description', 'Api\HomeController@update_title_description');

    Route::get('update_amenities', 'Api\HomeController@update_amenities');

    Route::get('add_whishlist', 'Api\HomeController@add_whishlist');

    Route::get('update_calendar', 'Api\HomeController@update_calendar');

    Route::get('home_pending_request', 'Api\HomeController@home_pending_request');

    Route::get('rooms_list_calendar', 'Api\HomeController@rooms_list_calendar');

    Route::get('new_add_room', 'Api\RoomsController@new_add_room');

    Route::get('listing', 'Api\RoomsController@listing_rooms');

    Route::get('listing_rooms_beds', 'Api\RoomsController@listing_rooms_beds');

    Route::get('send_message', 'Api\MessagesController@send_message');

    Route::get('inbox_reservation', 'Api\ReservationController@inbox_reservation');

    Route::get('view_profile','Api\UserController@view_profile');

    Route::get('edit_profile','Api\UserController@edit_profile');

    Route::get('conversation_list', 'Api\ReservationController@conversation_list');

    // Route::match(array('GET', 'POST'), 'upload_profile_image','Api\UserController@upload_profile_image');

    Route::get('trips_type','Api\TripsController@trips_type');

    Route::get('trips_details','Api\TripsController@trips_details');

    Route::get('add_wishlists','Api\WishlistsController@add_wishlists');

    Route::get('get_whishlist', 'Api\WishlistsController@get_whishlist');

    Route::get('get_particular_wishlist','Api\WishlistsController@get_particular_wishlist');

    Route::get('delete_wishlist','Api\WishlistsController@delete_wishlist');

    Route::get('edit_wishlist','Api\WishlistsController@edit_wishlist');

    Route::get('add_payout_perference','Api\PaymentController@add_payout_perference');

    Route::get('book_now','Api\PaymentController@book_now');

    Route::get('pay_now','Api\PaymentController@pay_now');

    Route::get('payment_success','Api\PaymentController@payment_success');

    Route::get('user_profile_details','Api\UserController@user_profile_details');

    Route::get('guest_cancel_pending_reservation','Api\TripsController@guest_cancel_pending_reservation');

    Route::get('guest_cancel_reservation','Api\TripsController@guest_cancel_reservation');

    Route::get('reservation_list','Api\ReservationController@reservation_list');

    Route::get('update_booking_type', 'Api\RoomsController@update_booking_type');

    Route::get('contact_request', 'Api\RoomsController@contact_request');

    Route::get('update_policy', 'Api\RoomsController@update_policy');

    Route::get('remove_uploaded_image', 'Api\RoomsController@remove_uploaded_image');

    Route::get('host_cancel_reservation','Api\ReservationController@host_cancel_reservation');

    Route::get('pre_approve','Api\ReservationController@pre_approve');

    Route::get('pre_accept','Api\PaymentController@pre_accept');

    Route::get('accept','Api\PaymentController@accept');

    Route::get('decline','Api\PaymentController@decline');

    Route::get('new_update_calendar', 'Api\HomeController@new_update_calendar');

    //get room and property type
    Route::get('room_property_type','Api\RoomsController@room_property_type');

    //Route::get('calendar_availabilitys', 'Api\RoomsController@calendar_availabilitys');
    // Route::get('SignUp_details', 'Api\UserController@signup_details');
    //  Route::get('Login_details', 'Api\UserController@SignUp_details');
  });
  Route::match(array('GET', 'POST'), 'upload_profile_image','Api\UserController@upload_profile_image');

  Route::match(array('GET', 'POST'), 'room_image_upload','Api\RoomsController@room_image_upload');


});
