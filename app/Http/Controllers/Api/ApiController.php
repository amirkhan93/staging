<?php

/**
 * ApiController Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    ApiController
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\RoomsAddress;
use App\Http\Controllers\Controller;
use App\Http\Helper\PaymentHelper;
use App\Http\Start\Helpers;
use App\City;
use App\State;
use Validator;
use DB;
use Auth;
use DateTime;
use Session;
use JWTAuth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ApiController extends Controller
{
  public function __construct()
  {
   
  }
    
  public function updateCityId(Request $request)
  {
    $room_details = RoomsAddress::whereNotNull('city')->where('city', 'REGEXP', '[a-z]+')->select('room_id', 'city')->get();
    $updated_count = 0;
    $notupdated_array = [];
    foreach($room_details as $detail)
    {
      $city_id = City::where('name', $detail->city)->first();
      if(isset($city_id->id))
      {
        $state_id = State::where('id', $city_id->state_id)->first();
        $rooms = RoomsAddress::findOrfail($detail->room_id);
        $rooms->city = $city_id->id; 
        $rooms->state = $state_id->id; 
        $rooms->country = $state_id->country_id;
        $rooms->update();
        $updated_count++;
      }else
      {
        Log::info('Not Updated Room details: '.$detail->room_id);
        $notupdated_array[] = ['room_id' => $detail->room_id];
      } 
    }
    return response()->json([ 'success_message' => 'update records',
                              'number of records updated citywise' => $updated_count,
                              'not updated' => count($notupdated_array)
                            ]);
  }

  public function updateStatewiseId()
  {
    $room_details = RoomsAddress::whereNotNull('state')->where('state', 'REGEXP', '[a-z]+')->select('room_id', 'state')->get();
    $updated_count = 0;
    $notupdated_array = [];
    foreach($room_details as $detail)
    {
      $state_id = State::where('name', $detail->state)->first();
      if(isset($state_id->id))
      {
        $rooms = RoomsAddress::findOrfail($detail->room_id);
        $rooms->city = null; 
        $rooms->state = $state_id->id; 
        $rooms->country = $state_id->country_id;
        $rooms->update();
        $updated_count++;
      }else
      {
        Log::info('Not Updated Room details: '.$detail->room_id);
        $notupdated_array[] = ['room_id' => $detail->room_id];
      } 
    }
    return response()->json([ 'success_message' => 'update records',
                              'number of records updated statewise' => $updated_count,
                              'not updated' => $notupdated_array
                            ]);
  }

}