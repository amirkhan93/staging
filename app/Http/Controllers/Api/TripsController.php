<?php

/**
 * Trips Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Trips
 * @author      Trioangle Product Team
 * @version     1.5.1.1.1
 * @link        http://trioangle.com
 */


namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Start\Helpers;
use App\Models\Reservation; 
use App\Models\Currency; 
use App\Models\ProfilePicture;
use App\Models\Messages;
use App\Models\Rooms;
use App\Models\Payouts;
use App\Models\Fees;
use App\Models\HostPenalty;
use App\Models\Calendar;
use Validator;
use DB;
use Auth;
use DateTime;
use App\Http\Helper\PaymentHelper;

use JWTAuth;



class TripsController extends Controller
{
     /**
     * Load Current Trips page.
     *
     * @return view Current Trips File
     */
    protected $helper; // Global variable for Helpers instance
    
    protected $payment_helper; // Global variable for PaymentHelper instance

       public function __construct(PaymentHelper $payment)
    {
        $this->payment_helper = $payment;
        $this->helper = new Helpers;
    }

  /**
     *Display trips type
     *
     * @param  Get method inputs
     * @return Response in Json
     */  
 public function trips_type(Request $request)
 {   
    
    $user=JWTAuth::parseToken()->authenticate();
    //get preaccepted reservation list
    $preaccepted= DB::table('reservation')->where('status','=','Pre-Accepted')

                 ->where('date_check','!=','No')->get();
         
      foreach ($preaccepted as $chck_pre) 
      {
        $room_id   = $chck_pre->room_id;
        
        $checkin   = $chck_pre->checkin;
        
        $checkout  = $chck_pre->checkout;
        
        $date_from = strtotime($checkin);
        
        $date_to   = strtotime($checkout); 
        
        $date_ar   = array();

        for ($i=$date_from; $i<=$date_to - 1; $i+=86400) 
         {  
            $date_ar[]= date("Y-m-d", $i).'<br />';  
         }  
        $check=array();

        for ($i=0; $i < count($date_ar) ; $i++) 
        { 
            //change reserved date status in calender
            $check[]=DB::table('calendar')->where([ 'room_id' => $room_id, 'date' => $date_ar[$i], 'status' => 'Not available' ])->get();

        }

        if(count(array_filter($check)) != 0 ) 
        {
            $this->status_update($room_id,$checkin,$checkout);
            echo "Already Booked";
        }

        }
        //get pending trips detials
        $data['pending_trips'] = Reservation::with('users','rooms')->where('status','!=','contact')
                ->where(function($query){
                    $query->where('status','Pending')->orwhere('status','Pre-Accepted'); 
                })
                ->where('user_id',$user->id)->get();
         //get current trips details
        $data['current_trips'] = Reservation::with('users','rooms')->where(function($query) {
                $query->where('checkin','>=',date('Y-m-d'))->where('checkout','<=',date('Y-m-d'));
            })->orWhere(function($query) {
                $query->where('checkin','<=',date('Y-m-d'))->where('checkout','>=',date('Y-m-d'));
            })->where('status','!=','Pending')->where('status','!=','Pre-Accepted')->where('status','!=','contact')->where('user_id',$user->id)->get();
         //get uploming trips details
        $data['upcoming_trips'] = Reservation::with('users','rooms')->where('checkin','>',date('Y-m-d'))->where('status','!=','contact')->where('status','!=','Pre-Accepted')->where('status','!=','')->where('status','!=','Pending')->where('user_id',$user->id)->get();
          //previous trips details
         $data['previous_trips'] = Reservation::with('users','rooms')->where('checkout','<',date('Y-m-d'))->where('user_id',$user->id)->get();
         
         //check trips count greater then 0
        if(count($data['pending_trips']) > 0)
        {

          $trips[]='Pending Trips';
          $trips_count[]=count($data['pending_trips']);

        }
        if(count($data['current_trips']) > 0)
        {

          $trips[]='Current Trips';
          $trips_count[]=count($data['current_trips']);

        }
         if(count($data['upcoming_trips']) > 0)
        {

          $trips[]='Upcoming Trips';
          $trips_count[]=count($data['upcoming_trips']);

        }
         if(count($data['previous_trips']) > 0)
        {

          $trips[]='Previous Trips';
          $trips_count[]=count($data['previous_trips']);

        }
        if(@$trips==null)
        {
            return response()->json([

                                  'success_message' => 'No Trips Types Found',

                                  'status_code'     => '0'

                                  ]);
        }

        return response()->json([
                                  'success_message' => 'Trips Types Listed Sucessfully',

                                  'status_code'     => '1',

                                  'Trips_type'      => $trips,

                                  'Trips_count'     => $trips_count

                                ]);
 }
  /**
     *Display trips details
     *
     * @param  Get method inputs
     * @return Response in Json
     */
  public function trips_details(Request $request)
  {  

    $trips_array=array('pending_trips','current_trips','upcoming_trips','previous_trips');
     //check request trips type is valid or not
    if(!in_array($request->trips_type, $trips_array))
     {
         return response()->json([

                                  'success_message' => 'Invalid Trips Type',

                                  'status_code'     => '0'

                                ]);

     }

      $user=JWTAuth::parseToken()->authenticate(); 
      //get pre_accepted reservation details
      $preaccepted= DB::table('reservation')->where('status','=','Pre-Accepted')->where('date_check','!=','No')->get();

        foreach ($preaccepted as $chck_pre) 
        {

        $room_id   = $chck_pre->room_id;

        $checkin   = $chck_pre->checkin;

        $checkout  = $chck_pre->checkout;

        $date_from = strtotime($checkin);

        $date_to   = strtotime($checkout); 

        $date_ar   = array();
         //check preacceted time is expire or not
        for ($i=$date_from; $i<=$date_to - 1; $i+=86400)
         {  
            $date_ar[]= date("Y-m-d", $i).'<br />';  
         }  

        $check=array();

        for ($i=0; $i < count($date_ar) ; $i++)
         {  //change reserved date status in calender
            $check[]=DB::table('calendar')->where([ 'room_id' => $room_id, 'date' => $date_ar[$i], 'status' => 'Not available' ])->get();
        }
        if(count(array_filter($check)) != 0 ) 
        {
            $this->status_update($room_id,$checkin,$checkout);
            // echo "Already Booked";
        }

        }
        //get pending trips details
        $data['pending_trips'] = Reservation::with(['users','rooms'=>function($query){
                                $query->with('rooms_address');
                            },'host_users'])->where('status','!=','contact')
                ->where(function($query){
                    $query->where('status','Pending')->orwhere('status','Pre-Accepted'); 
                })
                ->where('user_id',$user->id)->orderBy('id','DESC')->get();
         //get currenct trips details
        $data['current_trips'] = Reservation::with(['users','rooms'=>function($query){
                                $query->with('rooms_address');
                            },'host_users'])->where(function($query) {
                $query->where('checkin','>=',date('Y-m-d'))->where('checkout','<=',date('Y-m-d'));
            })->orWhere(function($query) {
                $query->where('checkin','<=',date('Y-m-d'))->where('checkout','>=',date('Y-m-d'));
            })->where('status','!=','Pending')->where('status','!=','Pre-Accepted')->where('status','!=','contact')->where('user_id',$user->id)->orderBy('id','DESC')->get();
         //get upcoming trips details
        $data['upcoming_trips']= Reservation::with(['users','rooms'=>function($query){
                                $query->with('rooms_address');
                            },'host_users'])->where('checkin','>',date('Y-m-d'))->where('status','!=','contact')->where('status','!=','Pre-Accepted')->where('status','!=','')->where('status','!=','Pending')->where('user_id',$user->id)->orderBy('id','DESC')->get();
          //get previous trips details
         $data['previous_trips'] = Reservation::with(['users','rooms'=>function($query){
                                $query->with('rooms_address');
                            },'host_users'])->where('checkout','<',date('Y-m-d'))->where('user_id',$user->id)->orderBy('id','DESC')->get();

         $result=$data[$request->trips_type];
          //get currecy details
         $currency_details= Currency::where('code',$user->currency_code)->first()->toArray();    
         foreach($result as $result_data)
         {      //check current trips is pending trips    
              if($request->trips_type=='pending_trips')
              {  
                 
                $room_location[]=$result_data->rooms->rooms_address->country_name;

                 $date = date('Y-m-d'); 
                  //check pending checkin date greater then current date
                if($result_data->checkin<$date)
                {   
                  if($result_data->status=='Pre-Accepted')
                   {
                    @$booking_status='Already Booked';
                   }
                }
                else
                {  
                   if($result_data->status=='Pre-Accepted')
                   {
                       //get booking status available or not
                      @$booking_status= $this->get_status(
                                                $result_data->room_id,
                                                $result_data->checkin,
                                                $result_data->checkout
                                                );

                   }
                   else
                   {
                     @$booking_status='';
                   }
             
                }
              }
              // check trips type
              if($request->trips_type=='current_trips'|| 
                 $request->trips_type=='upcoming_trips'||
                 $request->trips_type=='previous_trips')
              { 
                  $room_location=array();
                //get room address with remove null value
                if($result_data->rooms->rooms_address->address_line_1!='')
                  {

                     $room_location[]=$result_data->rooms->rooms_address->address_line_1;

                  }
                  if($result_data->rooms->rooms_address->address_line_2!='')
                  {

                     $room_location[]=$result_data->rooms->rooms_address->address_line_2;

                  }
                  if($result_data->rooms->rooms_address->city!='')
                  {

                     $room_location[]=$result_data->rooms->rooms_address->city;

                  }
                   if($result_data->rooms->rooms_address->state!='')
                  {

                     $room_location[]=$result_data->rooms->rooms_address->state;

                  }
                  if($result_data->rooms->rooms_address->country!='')
                  {

                     $room_location[]=$result_data->rooms->rooms_address->country;

                  }


              }

                $update_date = new DateTime($result_data->updated_at->toDateTimeString());

                $update_date=date('D, F d, Y'); 
                 
                      
                $response_data[]=array(

                  'reservation_id'      => $result_data->id,

                  'room_id'             => $result_data->room_id,

                  'user_name'           => $result_data->users->full_name,

                  'user_thumb_image'    => @ProfilePicture::where(

                                           'user_id',$result_data->user_id)

                                           ->first()->header_src,

                  'trip_status'         => $result_data->status,

                  'booking_status'      => @$booking_status!=null

                                          ?$booking_status :'',

                  'trip_date'           => $result_data->dates,

                  'check_in'            => $result_data->checkin_md,

                  'check_out'           => date('M d, Y', strtotime($result_data
                                          ->checkout)),

                  'room_name'           => $result_data->rooms->name,

                  'room_location'       => implode(',', $room_location),

                  'room_type'           => $result_data->rooms->room_type_name,

                  'total_nights'        => $result_data->nights,

                  'guest_count'         => @$result_data->number_of_guests!=null

                                            ?$result_data->number_of_guests:'0',

                  'host_user_name'      => $result_data->host_users->full_name,

                  'host_thumb_image'    => ProfilePicture::where(

                                        'user_id',$result_data->host_id)->first()

                                           ->header_src,

                  'room_image'          => url().'/images/'.$result_data->rooms

                                           ->photo_name,

                  'total_cost'          => $result_data->check_total,

                  'total_trips_count'   => count($data[$request->trips_type]),

                  'host_user_id'        => $result_data->host_id,

                  'per_night_price'     => @(string)$result_data->per_night,

                  'service_fee'         => @(string)$result_data->service,


                  'security_deposit'    => @(string)$result_data->security,


                  'cleaning_fee'        => @(string)$result_data->cleaning,

                  'additional_guest_fee'=> @(string)$result_data->additional_guest,

                  'payment_recieved_date'=> $result_data->transaction_id !=='' 
                                            ? $update_date :'',

                  'can_view_receipt'    => $result_data->status=='Accepted'

                                           ?'Yes':'No',

                  'currency_symbol'    => $currency_details['original_symbol']);
         }            
           if(@$response_data==null)
           {
              return response()->json([
                                  
                                  'success_message' => 'No Data Found',

                                  'status_code'     => '0'

                                  ]);
           }

             $sucess=array(

                        'success_message' => 'Trips Details Listed Sucessfully',

                        'status_code'     => '1'
                        
                        );
              
            return json_encode(
                              array_merge($sucess,[$request->trips_type=>$response_data]),JSON_UNESCAPED_SLASHES);

  }

 /**
     * Get days between two dates
     *
     * @param date $sStartDate  Start Date
     * @param date $sEndDate    End Date
     * @return array $days      Between two dates
     */
  public function get_days($sStartDate, $sEndDate)
  {    
        $sStartDate   = $this->payment_helper->date_convert($sStartDate);
        $sEndDate     = $this->payment_helper->date_convert($sEndDate);
        $aDays[]      = $sStartDate;
        $sCurrentDate = $sStartDate;  
       
        while($sCurrentDate < $sEndDate)
        {
            $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
            $aDays[]      = $sCurrentDate;  
        }
      
        return $aDays;  
  }
  /**
     * Check room already booked or not
     *
     * @param room_id  
     * @param start_date  
     * @param end_date  
     * @return array $days      Between two dates
     */
  public function check_already_booked($room_id, $start_date,$end_date)
  {    

        //check room staus already booked or not
       $data= $this->payment_helper->price_calculation($room_id,$start_date,$end_date,'','','');

      $data = json_decode($data, TRUE);

       $result=@$data['status'];

        if((isset($data['status'])) && ($result=='Not available'))
        {
        
          return 'Aready_booked';
        } 
        else
        {
                  
         return 'Available';

        }
        
  }
  /**
     * Check room status
     *
     * @param  room_id  
     * @param  start_date  
     * @param  end_date  
     * @return status
     */
  public function get_status($room_id, $start_date,$end_date)
  {
        $room_id   = $room_id;

        $checkin   = $start_date;

        $checkout  = $end_date;

        $date_from = strtotime($checkin);

        $date_to   = strtotime($checkout); 

        $date_ar   = array();

        for($i=$date_from; $i<=$date_to - 1; $i+=86400)
         {  
            $date_ar[]= date("Y-m-d", $i).'<br />';  
         }  

        $check=array();

        for ($i=0; $i < count($date_ar) ; $i++) 
        {   //change reserved date staus in calendar
            $check[]=DB::table('calendar')->where(['room_id' => $room_id, 'date' => $date_ar[$i], 'status' => 'Not available' ])->get();
        }
        if(count(array_filter($check)) == 0 ) 
        {
            return "Available"; //Pre-Accepted
            exit;
        }
        else
        {  
            $this->status_update($room_id,$checkin,$checkout);
            return "Already Booked";
            exit;
        }
  }
  /**
     * Update the reservation status
     *
     * @param room_id  
     * @param checkin  
     * @param checkout
     * @return 
     */
  public function status_update($room_id, $checkin, $checkout)
  {
    $chck_reservation=Reservation::where(['room_id'=>$room_id,'checkin'=>$checkin, 'checkout'=>$checkout])->where('status','!=','Accepted')->get();

    $count_reservation=count($chck_reservation);

     if($count_reservation > '0')
        {
            foreach ($chck_reservation as $result) 
            {
                Reservation::where('id',$result->id)->update(['date_check' => 'No']);
            }
        }
    }
     /**
     * Pending Reservation Cancel by Guest
     *
     * @param Get method inputs
     * @return Response in Json
     */
  public function guest_cancel_pending_reservation(Request $request)
  {
    
     $rules     = array( 'reservation_id' =>   'required|exists:reservation,id');

     $niceNames = array('reservation_id'  =>   'Reservation Id'); 

     $messages  = array('required'        =>   ':attribute is required.');

     $validator = Validator::make($request->all(), $rules, $messages);

     $validator->setAttributeNames($niceNames); 


      if ($validator->fails()) 
      {
        $error=$validator->messages()->toArray();

          foreach($error as $er)
          {
            $error_msg[]=array($er);

          } 
  
          return response()->json([

                'success_message'=>$error_msg['0']['0']['0'],

                'status_code'=>'0'] );
      }
      else
      {

          $user = JWTAuth::parseToken()->authenticate();

          $reservation_details = Reservation::find($request->reservation_id);
           //check valid user or not
          if($user->id!=$reservation_details->user_id)
          {

            return response()->json([

                                'success_message'=>'Permission Denied ',

                                'status_code'=>'0'] );

          }

          if($reservation_details->status=='Cancelled' || 

             $reservation_details->status=='Declined' || 

             $reservation_details->status=='Expired')

          {
                 return response()->json([

                                'success_message'=>'Not Available',

                                'status_code'=>'0'] );
          }

          $messages                 = new Messages;

          $messages->room_id        = $reservation_details->room_id;

          $messages->reservation_id = $reservation_details->id;

          $messages->user_to        = $reservation_details->host_id;

          $messages->user_from      = $user->id;

          $messages->message        = $this->helper->phone_email_remove($request->cancel_message);
          $messages->message_type   = 10;

          $messages->save();

           //cancel reservation by guest
          $cancel = Reservation::find($request->reservation_id);

          $cancel->cancelled_by     = "Guest";

          $cancel->cancelled_reason = $request->cancel_reason;

          $cancel->cancelled_at     = date('Y-m-d H:m:s');

          $cancel->status           = "Cancelled";

          $cancel->updated_at       = date('Y-m-d H:m:s');

          $cancel->save();
       

          return response()->json([

                                'success_message'=>'Reservation Successfully Cancelled',

                                'status_code'=>'1'] );
      }
  }
  /**
     * Reservation Cancel by Guest
     *
     * @param Get method inputs
     * @return Response in Json
     */
  public function guest_cancel_reservation(Request $request)
  {  

    $rules     = array( 'reservation_id' =>  'required|exists:reservation,id');

    $niceNames = array('reservation_id'  =>  'Reservation Id'); 

    $messages  = array('required'        =>  ':attribute is required.');

    $validator = Validator::make($request->all(), $rules, $messages);

    $validator->setAttributeNames($niceNames); 


      if ($validator->fails()) 
      {
        $error=$validator->messages()->toArray();

          foreach($error as $er)
          {
            $error_msg[]=array($er);

          } 
  
          return response()->json([

                'success_message' => $error_msg['0']['0']['0'],

                'status_code'     => '0'

                                ]);
      }
      else
      { 

       $user = JWTAuth::parseToken()->authenticate();

       $reservation_details = Reservation::find($request->reservation_id);
        //check valid user or not
       if($user->id!=$reservation_details->user_id)
          {

            return response()->json([

                                'success_message'=>'Permission Denied',

                                'status_code'=>'0'] );

          }
           //Prevent Cancel Reservation
       if($reservation_details->status=='Cancelled')
       {
         return response()->json([

                                'success_message'=>'Not Available',

                                'status_code'=>'0'] );
       }
    
       $rooms_details = Rooms::find($reservation_details->room_id);

       if($reservation_details->status == "Accepted")
       {
        
        // penalty refund process

       $penalty_revert = Payouts::where('user_id', $reservation_details->host_id)

                        ->where('reservation_id',$request->reservation_id)->get();
    
    
       if($penalty_revert[0]->penalty_id != 0 && $penalty_revert[0]->penalty_id != '')
       {

       $penalty_id = explode(",",$penalty_revert[0]->penalty_id);

       $penalty_amt = explode(",",$penalty_revert[0]->penalty_amount);

        $i =0;

        foreach ($penalty_id as $row) 
        {


          $old_amt = HostPenalty::where('id',$row)->get();

          $upated_amt = $old_amt[0]->remain_amount + $penalty_amt[$i];

          HostPenalty::where('id',$row)->update(['remain_amount' => $upated_amt,'status' => 'Pending' ]); 
      
          $i++;

        }
       }
       
      // penalty refund process end


       $datetime1 = new DateTime(); 
       $datetime2 = new DateTime($reservation_details->checkin); 
       $interval_diff = $datetime1->diff($datetime2);
        
       $interval = $interval_diff->days;
      // host fee calculation 

      $host_fee_percentage     = Fees::find(2)->value;

      // host fee calcualtion

      if($datetime1 < $datetime2 )
      { 
            
        // Check cancellation policy for Flexible

        if($reservation_details->cancellation == "Flexible")
        {
           
           /* Check the current date if its lessthan the checkin date */ 
    
            if($interval == 0)
            {
                 
            // first night is non-refundable
              
                $refundable_price = ($reservation_details->subtotal - $reservation_details->per_night);
                //dd($reservation_details->cleaning);
                  $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $refundable_price;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();

            
               // Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get(); 
                
                if($host_fee_percentage > 0)
                {
                    $host_first_night_price = ($reservation_details->per_night - ($reservation_details->per_night * ($host_fee_percentage/100)));
                }
                else
                {
                    $host_first_night_price = $reservation_details->per_night;
                }

                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$host_first_night_price,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end
                

                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();
          

                $payouts                  = Payouts::find($payouts_id[0]->id);

                $payouts->amount          = $host_amount;

                $payouts->penalty_id      = $penalty_id;

                $payouts->penalty_amount  = $penalty_amount;

                $payouts->save();
    

            }
              elseif($interval > 0)
            {
               // if cancel date greaterthan one, the total amount will refund to guest 
               
                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $reservation_details->subtotal;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();

            $payouts_host_amount     = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->delete();
    
            }
        }

    // Check cancellation policy for Moderate

        if($reservation_details->cancellation == "Moderate")
        {
           
/* Check the current date if its lessthan the checkin date */ 

            if($interval < 5)
            {
                 
            // first night is non-refundable
              
                $refundable_price = ($reservation_details->subtotal - $reservation_details->per_night);

            // As per Moderate policy 50% maount will be refunded to host

                $additional_amt = $reservation_details->cleaning + $reservation_details->security + $reservation_details->additional_guest ;

                $refund_amt = $refundable_price - $additional_amt;

                $div_refundable_price = (50 / 100) * $refund_amt;

                 if($host_fee_percentage > 0)
                {
                    $host_first_night_price = (($div_refundable_price + $reservation_details->per_night) - (($div_refundable_price + $reservation_details->per_night) * ($host_fee_percentage/100)));
                }
                else
                {
                    $host_first_night_price = $div_refundable_price + $reservation_details->per_night;
                }

                $guest_refundable_price = $div_refundable_price + $additional_amt;

                $host_refundable_price = $host_first_night_price;

                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $guest_refundable_price;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();

            
// Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get(); 

                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$host_refundable_price,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end


                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();
          

                $payouts                  = Payouts::find($payouts_id[0]->id);

                $payouts->amount          = $host_amount;

                $payouts->penalty_id      = $penalty_id;

                $payouts->penalty_amount  = $penalty_amount;

                $payouts->save();
    

            }
              elseif($interval >= 5)
            {
               // if cancel date greaterthan five, the total amount will refund to guest 
               
                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $reservation_details->subtotal;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();

                $payouts_host_amount     = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->delete();
    
            }
        }

// Check cancellation policy for Strict

        if($reservation_details->cancellation == "Strict")
        {
          
/* Check the current date if its lessthan the checkin date */ 

            if($interval < 7)
            {
                 
            // first night is non-refundable
              
                $refundable_price = $reservation_details->subtotal;

            // If the guest cancels less than 7 days in advance, the nights not spent are not refunded.

                $guest_refundable_price = $reservation_details->cleaning + $reservation_details->security + $reservation_details->additional_guest;

                $host_refundable_price  = $reservation_details->subtotal - $guest_refundable_price;

                if($guest_refundable_price > 0)
                {
                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $guest_refundable_price;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();
                }

            
// Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get(); 
                
                 if($host_fee_percentage > 0)
                {
                    $host_refund_night_price = ($host_refundable_price - ($host_refundable_price * ($host_fee_percentage/100)));
                }
                else
                {
                    $host_refund_night_price = $host_refundable_price;
                }

                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$host_refund_night_price,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end


                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();
          

                $payouts                  = Payouts::find($payouts_id[0]->id);

                $payouts->amount          = $host_amount;

                $payouts->penalty_id      = $penalty_id;

                $payouts->penalty_amount  = $penalty_amount;

                $payouts->save();
    

            }
              elseif($interval >= 7)
            {
               // if cancel date greaterthan seven, the total 50 % amount will refund to guest 

            // As per Moderate policy 50% maount will be refunded to host

                $additional_amt = $reservation_details->cleaning + $reservation_details->security + $reservation_details->additional_guest ;

                $refund_amt = $reservation_details->subtotal - $additional_amt;

                $div_refundable_price = (50 / 100) * $refund_amt;

                $guest_refund_amount = $div_refundable_price + $additional_amt;

                $host_refund_price = $div_refundable_price;

                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $guest_refund_amount;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();

// Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get(); 
                
                 if($host_fee_percentage > 0)
                {
                    $host_refund_night_price = ($host_refund_price - ($host_refund_price * ($host_fee_percentage/100)));
                }
                else
                {
                    $host_refund_night_price = $host_refund_price;
                }

                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$host_refund_night_price,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end


                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();
          

                $payouts                  = Payouts::find($payouts_id[0]->id);

                $payouts->amount          = $host_amount;

                $payouts->penalty_id      = $penalty_id;

                $payouts->penalty_amount  = $penalty_amount;

                $payouts->save();

    
            }
        }



            $days = $this->get_days($reservation_details->checkin, $reservation_details->checkout);
        
// Update Calendar, delete booked dates

        for($j=0; $j<count($days)-1; $j++)
            {
           
            Calendar::where('room_id', $reservation_details->room_id)->where('date', $days[$j])->where('status', 'Not available')->delete();

            }

// Update Calendar,delete booked dates
            
/* Check the current date  if its lessthan the checkin date end*/  
    }
    else
        {

                      /*--- cancel in checkin date ---*/

                 if($interval == 0)
            {
                 
                // first night is non-refundable
                
                if($reservation_details->cancellation == "Moderate")
                    {

                    
                    $refundable_price_all = ($reservation_details->subtotal - $reservation_details->per_night);

                    // As per Moderate policy 50% maount will be refunded to host

                    $additional_amt = $reservation_details->cleaning + $reservation_details->security + $reservation_details->additional_guest ;

                    $refund_amt = $refundable_price_all - $additional_amt;

                    $div_refundable_price = (50 / 100) * $refund_amt;

                    $refundable_price = $div_refundable_price + $reservation_details->security;


                     if($host_fee_percentage > 0)
                        {
                        
                        $host_first_night_price = (($div_refundable_price + $reservation_details->per_night) - (($div_refundable_price + $reservation_details->per_night) * ($host_fee_percentage/100)));
                        }
                    else
                        {
                    
                    $host_first_night_price = $div_refundable_price + $reservation_details->per_night;
                        }


                    $host_refund = $host_first_night_price + $reservation_details->cleaning + $reservation_details->additional_guest;

                    }

                if($reservation_details->cancellation == "Flexible")
                    {

                    $refundable_price = (($reservation_details->subtotal - $reservation_details->per_night) - $reservation_details->cleaning) - $reservation_details->additional_guest;

                    // Host fee deducted 
                    $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get();

                        if($host_fee_percentage > 0)
                            {
                                $host_night_price = ($reservation_details->per_night - ($reservation_details->per_night * ($host_fee_percentage/100)));
                            }
                        else
                            {
                                $host_night_price = $reservation_details->per_night;
                            }
                    // Host fee deducted 

                    $host_refund      = $host_night_price + $reservation_details->cleaning + $reservation_details->additional_guest;


                    }

                if($reservation_details->cancellation == "Strict")
                    {

                    $refundable_price = $reservation_details->security;

                     if($host_fee_percentage > 0)
                            {
                                $host_refund_night_price = (($reservation_details->nights * $reservation_details->per_night) - (($reservation_details->nights * $reservation_details->per_night) * ($host_fee_percentage/100)));
                            }
                       else
                            {
                                $host_refund_night_price = $reservation_details->nights * $reservation_details->per_night;
                            }

                    $host_refund      = $host_refund_night_price + $reservation_details->cleaning + $reservation_details->additional_guest; 

                    }       
                
                if($refundable_price > 0)
                {
                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $refundable_price;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
    
                $payouts->save();
                }


// Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get();
            
                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$host_refund,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end

// update host payout   
                
                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();
          

                $payouts = Payouts::find($payouts_id[0]->id);
                $payouts->amount          =  $host_amount;
                $payouts->penalty_id      = $penalty_id;
                $payouts->penalty_amount  = $penalty_amount;
                $payouts->save();

// update host payout      
        
// Update Calendar, delete booked dates

                $days = $this->get_days($reservation_details->checkin, $reservation_details->checkout);

                for($j=0; $j<count($days)-1; $j++)
                {
                Calendar::where('room_id',$reservation_details->room_id)->where('date', $days[$j])->where('status', 'Not available')->delete();
                }

// Update Calendar, delete booked dates

            }
/*--- cancel in checkin date ---*/
            else
            {
               
/*--Cancel after checkin date ---*/  

// calculate the not stay nights, guest amount 

                $remaining_nights =  $reservation_details->nights - $interval;

                if($reservation_details->cancellation == "Moderate")
                    {

                    $refundable_to_guest = (50 / 100) *($remaining_nights * $reservation_details->per_night);

                    $refundable_price_guest = $refundable_to_guest + $reservation_details->security;

                    if($host_fee_percentage > 0)
                            {
                                $host_refund_night_price = ( ($interval * $reservation_details->per_night) - (($interval * $reservation_details->per_night) * ($host_fee_percentage/100)));
                            }
                       else
                            {
                                $host_refund_night_price = $interval * $reservation_details->per_night;
                            } 

                    $refundable_price_host  = $host_refund_night_price + $reservation_details->cleaning + $reservation_details->additional_guest + $refundable_to_guest;

                    }

                if($reservation_details->cancellation == "Flexible")
                    {

                    $refundable_price_guest = ($remaining_nights * $reservation_details->per_night) + $reservation_details->security;


                       if($host_fee_percentage > 0)
                            {
                                $host_refund_night_price = ( ($interval * $reservation_details->per_night) - (($interval * $reservation_details->per_night) * ($host_fee_percentage/100)));
                            }
                       else
                            {
                                $host_refund_night_price = $interval * $reservation_details->per_night;
                            } 

                    $refundable_price_host  = $host_refund_night_price + $reservation_details->cleaning + $reservation_details->additional_guest;

                    }

                if($reservation_details->cancellation == "Strict")
                    {

                    $refundable_price_guest = $reservation_details->security;

                    if($host_fee_percentage > 0)
                            {
                                $host_refund_night_price = ( ($reservation_details->nights * $reservation_details->per_night) - (($reservation_details->nights * $reservation_details->per_night) * ($host_fee_percentage/100)));
                            }
                       else
                            {
                                $host_refund_night_price = $reservation_details->nights * $reservation_details->per_night;
                            }

                    $refundable_price_host  = $host_refund_night_price + $reservation_details->cleaning + $reservation_details->additional_guest;

                    }   

                if($refundable_price_guest > 0)
                {
                $payouts = new Payouts;
    
                $payouts->reservation_id = $request->reservation_id;
                $payouts->room_id        = $reservation_details->room_id;
                $payouts->user_id        = $reservation_details->user_id;
                $payouts->user_type      = 'guest';
                $payouts->amount         = $refundable_price_guest;
                $payouts->currency_code  = $reservation_details->currency_code;
                $payouts->status         = 'Future';
                $payouts->penalty_amount = 0;
                $payouts->penalty_id     = 0;
    
                $payouts->save();
                }
// calculate the not stay nights, guest amount 
                
// Deduct penalty amount start

                $penalty = HostPenalty::where('user_id',$reservation_details->host_id)->where('remain_amount','!=',0)->get();
            
                $penalty_result = $this->payment_helper->check_host_penalty($penalty,$refundable_price_host,$reservation_details->currency_code);

                $host_amount    = $penalty_result['host_amount'];
                $penalty_id     = $penalty_result['penalty_id'];
                $penalty_amount = $penalty_result['penalty_amount'];

// Deduct penalty amount end


                $payouts_id = Payouts::where('user_id', $reservation_details->host_id)->where('reservation_id', $request->reservation_id)->get();

                $payouts = Payouts::find($payouts_id[0]->id);

                $payouts->amount   = $host_amount;

                $payouts->penalty_id      = $penalty_id;

                $payouts->penalty_amount  = $penalty_amount;

                $payouts->save();


                $cancelled_date = date('Y-m-d H:m:s');
                $days = $this->get_days($cancelled_date ,$reservation_details->checkout);
       
// Update Calendar, delete stayed date
                for($j=0; $j<count($days)-1; $j++)
                  {

                Calendar::where('room_id', $reservation_details->room_id)->where('date', $days[$j])->where('status', 'Not available')->delete();

                  }
// Update Calendar, delete stayed date

/*--Cancel after checkin date ---*/ 
              }
          }
      
                $messages = new Messages;
    
                $messages->room_id        = $reservation_details->room_id;
                $messages->reservation_id = $reservation_details->id;
                $messages->user_to        = $reservation_details->host_id;
                $messages->user_from      = $user->id;
                $messages->message        = $this->helper->phone_email_remove($request->cancel_message);
                $messages->message_type   = 10;
    
                $messages->save();
    
    
                $cancel = Reservation::find($request->reservation_id);
    
                $cancel->cancelled_by = "Guest";
                $cancel->cancelled_reason = $request->cancel_reason;
                $cancel->cancelled_at = date('Y-m-d H:m:s');
                $cancel->status = "Cancelled";
                $cancel->updated_at = date('Y-m-d H:m:s');
                $cancel->save();
       
   
       return response()->json([

                                'success_message'=>'Reservation Successfully Cancelled',

                                'status_code'=>'1'

                              ]);

    }
      else
      {
        
        return response()->json([

                                'success_message'=>'Not Available',

                                'status_code'=>'0'

                                ]);
      } 
        
    }
  }


 
}
    
