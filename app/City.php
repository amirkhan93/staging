<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
      /*
        **
            @Relationship define between City and State
            Relationship type: Belongs to State
        **
    */

    public function state()
    {
        return $this->belongsTo('App\State');
    }

}
