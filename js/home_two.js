$(".burger--sm").click(function() {
  $(".header--sm .nav--sm").css("visibility", "visible"),
  $(".makent-header .header--sm .nav-content--sm").addClass("right-content"),
  $(".arrow-icon").toggleClass("fa-angle-down"),
  $(".arrow-icon").toggleClass("fa-angle-up"),
  $(".arrow-icon1").toggleClass("fa-bars"),
  $(".arrow-icon1").toggleClass("fa-bars-up"),
  $("body").addClass("pos-fix"),
  $("body").addClass("remove-pos-fix pos-fix"),
  $(".makent-header .header--sm .title--sm").toggleClass("hide");
}),
$(".nav-mask--sm").click(function() {
  $(".header--sm .nav--sm").css("visibility", "hidden"),
  $(".makent-header .header--sm .nav-content--sm").removeClass(
    "right-content"
  ),
  $(".arrow-icon").toggleClass("fa-angle-down"),
  $(".arrow-icon").toggleClass("fa-angle-up"),
  $(".arrow-icon1").toggleClass("fa-bars"),
  $(".arrow-icon1").toggleClass("fa-bars-up"),
  $("body").removeClass("remove-pos-fix pos-fix"),
  $(".makent-header .header--sm .title--sm").toggleClass("hide");
}),
$(".foryou").click(function() {
  $(".foryou").toggleClass("current"), $(".homes").toggleClass("current");
}),
$(".homes").click(function() {
  $(".foryou").toggleClass("current"), $(".homes").toggleClass("current");
}),
$("body").click(function() {
  $(
    ".tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login"
  ).removeClass("show"),
  $(".panel-drop-down").addClass("hide-drop-down");
}),
$(".button-sm-search").click(function(e) {
  e.stopPropagation(),
  $("#search-modal--sm").removeClass("hide"),
  $("#search-modal--sm").attr("aria-hidden", "false");
}),
$(".arrow-button").click(function(e) {
  e.stopPropagation(), $(".panel-drop-down").toggleClass("hide-drop-down");
}),
$(document).ready(function() {}),
$(".home-bx-slider .bxslider").bxSlider({
  infiniteLoop: !1,
  hideControlOnEnd: !0,
  minSlides: 1,
  maxSlides: 3,
  slideWidth: 320,
  slideMargin: 20,
  moveSlides: 1,
  onSliderLoad: function() {
    setTimeout(function() {
      $("#lazy_load_slider").removeClass("lazy-load");
    }, 2e3);
  }
}),
(start = moment()),
$(".webcot-lg-datepicker button").daterangepicker({
  startDate: start,
  minDate: start,
  autoApply: !0,
  autoUpdateInput: !1,
  locale: { format: "MMM DD" }
}),
$(".webcot-lg-datepicker button").on("show.daterangepicker", function(e, t) {
  $(this)
  .parent()
  .css("opacity", 0),
  $(this)
  .parent()
  .parent()
  .find(".DateRangePickerDiv")
  .parent()
  .css("cssText", "opacity: 1 !important");
}),
$(".webcot-lg-datepicker button").on("apply.daterangepicker", function(e, t) {
  (startDateInput = $('[name="checkin"]')),
  (endDateInput = $('[name="checkout"]')),
  (startDate = t.startDate),
  (endDate = t.endDate),
  startDateInput.val(startDate.format("DD-MM-YYYY")),
  startDateInput.next().html(startDate.format("MMM DD")),
  endDateInput.val(endDate.format("DD-MM-YYYY")),
  endDateInput.next().html(endDate.format("MMM DD"));
}),
$(".webcot-lg-datepicker button").on("hide.daterangepicker", function(e, t) {
  (t.startDate && t.endDate) ||
  ($(this)
  .parent()
  .css("opacity", 1),
  $(this)
  .parent()
  .parent()
  .find(".DateRangePickerDiv")
  .parent()
  .css("cssText", "opacity: 0 !important"));
});

var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 1000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName("txt-rotate");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-rotate");
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #fff }";
  document.body.appendChild(css);
};

$("#searchbar-form").click(function() {
  $(".clicked_overlay").show();
  $("div#discovery-container").addClass("clicked");
  $("body").addClass("form_clicked");
});
$(".clicked_overlay").click(function() {
  $(".clicked_overlay").hide();
  $("div#discovery-container").removeClass("clicked");
  $("body").removeClass("form_clicked");
  $(".search-box").hide();
});

// $("#header-search-input").keyup(function() {
//   var searchedVal = $(this).val();
//   console.log(searchedVal);
//   if (searchedVal != "") {
//     $(".search-box").show();
//   } else {
//     $(".search-box").hide();
//   }
// });

$(".search-box ul li").click(function() {
  var ThisVal = $(this).attr("data-value");
  $("#header-search-input").val(ThisVal);
  $(".search-box").hide();
  console.log(ThisVal);
});

$("#propertiesGallery").lightSlider({
  item: 1,
  loop: true,
  thumbItem: 9,
  slideMargin: 0,
  enableDrag: true,
  verticalHeight: 405,
  pager: false
});

$(window).scroll(function() {
  var scroll = $(window).scrollTop();

  //>=, not <=
  if (scroll >= 200) {
    //clearHeader, not clearheader - caps H
    $("#header").addClass("darkHeader");
    //$(".top_search_area").addClass("isFixed");
  } else {
    $("#header").removeClass("darkHeader");
    //$(".top_search_area").removeClass("isFixed");
  }
}); //missing );

$(document).ready(function() {
  $(".js-example-basic-single").select2();
  $(".js-example-basic-multiple").select2();
});
// $(".datepicker").datepicker({ format: "dd.mm.yyyy" });

$("#occupancy_btn").click(function() {
  $("#occupancy_box").show();
});
$("#close_occu").click(function() {
  $("#occupancy_box").hide();
});

$("#los_btn").click(function() {
  $("#los_box").show();
});
$("#close_loss").click(function() {
  $("#los_box").hide();
});
$("#last_minute_btn").click(function() {
  $("#last_minute_box").show();
});
$("#close_last_minute").click(function() {
  $("#last_minute_box").hide();
});
$("#l_stay_btn").click(function() {
  $("#l_stay_box").show();
});
$("#close_l_stay").click(function() {
  $("#l_stay_box").hide();
});

// var today = new Date(
//   new Date().getFullYear(),
//   new Date().getMonth(),
//   new Date().getDate()
// );

// $("#dateFrom").daterangepicker(
//   {
//     singleDatePicker: true,
//     autoApply: true,
//     locale: {
//       format: "MMMM D, YYYY",
//       separator: " - "
//     },

//     minDate: today
//   },
//   function(start, end, label) {
//     alert(
//       "A new date selection was made: " +
//         start.format("DD-MM-YYYY") +
//         " to " +
//         end.format("DD-MM-YYYY")
//     );

//     selectedDate = start.format("DD-MM-YYYY");
//   }
// );
// $("#dateTo").daterangepicker(
//   {
//     singleDatePicker: true,
//     autoApply: true,
//     locale: {
//       format: "MMMM D, YYYY",
//       separator: " - "
//     },

//     minDate: today
//   },
//   function(start, end, label) {
//     alert(
//       "A new date selection was made: " +
//         start.format("DD-MM-YYYY") +
//         " to " +
//         end.format("DD-MM-YYYY")
//     );
//   }
// );

app.controller("HomePageController", function($scope, $http, $location, $compile)
{
  $scope.location = "";
  $scope.checkin = "";
  $scope.checkout = "";
  $scope.guests = 0;

  $scope.getLocation = function(e) {
    $scope.location = e.currentTarget.getAttribute('data-location');
    $('.search-box').css({display: 'none'});
    $("#search_google_ul_2").hide();
    $("#search_google_ul_2").empty();
  };

  $scope.getUrlEncoded = function(location) {
    return location.replace(/, /gi, "%2C").replace(/ /gi, "%20");
  };

  $scope.getLocationList = function(location)
  {
    if (location == '')
    {
      $("#search_google_ul li").hide();
      $(".ui-helper-hidden-accessible").hide();
      return false;
    }
    $.ajax({
            type: "post",
            url: $scope.base__url + "/search/prop_search",
            data: {
              search_textbox_index: location
            },
            success: function(data)
            {
              $('.search-box').css({display: 'block'});
              $("#search_google_ul_2").show();
              $("#search_google_ul_2").empty();
              $compile($("#search_google_ul_2").html(data))($scope);
            }
          });
  }

  //$("#google_location_2").keyup(function() {
  //   var google_location = $("#google_location_2").val();
  //   if (google_location == '') {
  //     $("#search_google_ul li").hide();
  //     $(".ui-helper-hidden-accessible").hide();
  //     return false;
  //   }
  //   $.ajax({
  //     type: "post",
  //     url: basePath + "/properties/prop_search",
  //     data: {
  //       search_textbox_index: google_location
  //     },
  //     success: function(data) {
  //       $("#wheretogo2_google_2").val('');
  //       $("#search_google_ul_2").show();
  //       $("#search_google_ul_2").empty();
  //       $("#search_google_ul_2").html(data);
  //     }
  //   });
  //   return false;
  // });

  $scope.seach_properties = function(name)
  {
    // $http.get('/s').then(res => console.log(res));
    var url = $scope.base__url + "/s?location=" + $scope.getUrlEncoded($scope.location) + "&checkin=" + $scope.checkin +
              "&checkout=" + $scope.checkout + "&guests=" + $scope.guests;
    window.location.href = url;
  };
});
