app.controller("SearchController", [
  "$scope",
  "$http",
  "$location",
  function($scope, $http, $location) {
    $scope.base__url = "";
    $scope.newSearch = "Search Result";
    $scope.first_search = "Yes";
    var paramStr = $location.absUrl().split("?");
    var paramArr = paramStr[1].split("&");
    var params = {};
    paramArr.forEach(param => {
      params[param.split("=")[0]] = param.split("=")[1];
    });

    function l() {
      "Yes" == $scope.first_search &&
        (($scope.first_search = "No"),
        $(".button_ipunk").show(),
        $(".button_ipunk-result").hide(),
        $(".map.hide-sm-view").hide(),
        $(".search-results").show(),
        $(".filter-div").hide()),
        $(".map").removeClass("loading");
    }

    function r() {
      $(".search-results").hasClass("loading")
        ? $("#no_results").hide()
        : $("#no_results").show();
    }

    function d(e) {
      e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var t = new RegExp("[\\?&]" + e + "=([^&#]*)"),
        a = t.exec(location.search);
      return null === a ? "" : decodeURIComponent(a[1].replace(/\+/g, " "));
    }

    $scope.initialSearch = function(url) {
        // $http.post(url + "/initial-search", params).then(res => console.log(res));
      $http.get($location.absUrl().replace("/s?", "/initial-search?"))
        .then(res => {
          console.log(res);
          $scope.room_result = res.data;
          $scope.checkin = params.checkin;
          $scope.checkout = params.checkout;
          $scope.totalPages = res.data.last_page;
          $scope.currentPage = res.data.current_page;
          for (var a = [], i = 1; i <= res.data.last_page; i++) a.push(i);
          var o = d("amenities"),
            s = d("property_type");
          if (
            $(window).width() > 760 &&
            (("" != o && "" != s) ||
              $("#more_filter_submit").attr("disabled", "disabled"))
          ) {
            $scope.range = a;
            $(".search-results").removeClass("loading");
            l();
            r();
            // u(res.data);
          }
        });
      //   $http.get(url + "/initial-search").then(res => console.log(res));
    };
  }
]);

$(document).ready(function() {
  $(".b1 .minus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".b1 .plus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    if ($input.val() < 10) {
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    }
  });
  $(".b2 .minus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".b2 .plus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    if ($input.val() < 10) {
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    }
  });
  $(".b3 .minus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
  });
  $(".b3 .plus").click(function() {
    var $input = $(this)
      .parent()
      .find("input");
    if ($input.val() < 10) {
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    }
  });
});
